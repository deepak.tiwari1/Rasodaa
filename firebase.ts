import * as firebase from 'firebase/app';
import '@firebase/auth';

const firebaseConfig: any = {
    apiKey: "AIzaSyCGLWuk9bAEjNVSaErUrUv02WikKFCmLmg",
    authDomain: "rasodaa-351304.firebaseapp.com",
    databaseURL: "https://rasodaa-351304.firebaseio.com",
    projectId: "rasodaa-351304",
    storageBucket: "rasodaa-351304.appspot.com",
    messagingSenderId: "743279779049",
    appId: "1:743279779049:android:3d3275c36b61b0fd842478",
    //measurementId: "G-measurement-id",
};
firebase.initializeApp(firebaseConfig);

export default firebase