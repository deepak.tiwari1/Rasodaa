import { View, Text, ScrollView, Image, StyleSheet, Dimensions } from 'react-native'
import React, { useEffect, useState } from 'react'
import * as Server from '../../Management/Utility/GlobalURLs'
import { useSelector } from 'react-redux'
import { RootState } from '../../Management'
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { BASEURL_IMAGE } from '../../Constant/index';

export default function ImageSilder() {
	const [images, setimages] = useState(['', '', '', '']);
	const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
	const [offers, setoffers] = useState([])
	// useEffect(() => {
	// 	fetch(BASEURL_IMAGE + 'offers?start=0&length=10&search=a').then(res => res.text()).then(data => setoffers(data))
	// 	console.log(offers)
	// }, [])
	return (
		<View style={styles.container}>
			<SwiperFlatList
				autoplay
				autoplayDelay={3}
				autoplayLoop
				index={2}
				showPagination
				data={['https://cdn.grabon.in/gograbon/images/web-images/uploads/1618575517942/food-coupons.jpg', 'https://d4t7t8y8xqo0t.cloudfront.net/resized/1080X400/pages%2F849%2Fimage20190304095958.jpg', 'https://dealicious.me/wp-content/uploads/2021/03/amritsr-1024x390.jpg', 'https://www.dineout.co.in/blog/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-18-at-8.06.23-PM.jpeg']}
				paginationStyleItem={{ width: 8, height: 8, margin: 2, borderRadius: 1 }}
				paginationStyleItemActive={{ width: 20, borderRadius: 10 }}
				paginationStyleItemInactive={{ width: 8, height: 8, margin: 2, borderRadius: 1, backgroundColor: '#fff' }}
				autoplayInvertDirection={true}
				renderItem={({ item }) => (
					<Image style={styles.images}
						source={{ uri: item }} />

				)}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		height: 150
	},
	images: {
		// flex:1,
		height: 150,
		width: Dimensions.get('window').width / 1.1,
		resizeMode: 'cover',
		borderRadius: 20,
		marginHorizontal: 20,

	}
})
