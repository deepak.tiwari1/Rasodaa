import { View } from 'react-native';
import { useSelector } from "react-redux";
import { RootState } from '../../../Management';
import styled from "styled-components";
import React, { useState, useEffect } from 'react';

const CustomText = styled.Text`
  font-size: ${props => props.fz}px;
  color: ${props => props.clr};
  fontFamily: ${props => props.fzz.fontFamily};
  lineHeight: ${props => props.fzz.lineHeight}px;
  letterSpacing: ${props => props.fzz.latterSpacing}px;
  text-align:${props => props.align};

`


type TextProperties = {
  text: string,
  clr: string,
  align: string
}


export function DisplayLarge({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.DisplayFont.Large.fontSize + fontSize}
      clr={clr}
      fzz={font.DisplayFont.Large}
      align={align}
    >{text}
    </CustomText>
  )
}

export function DisplayMedium({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.DisplayFont.Medium.fontSize + fontSize}
      clr={clr}
      fzz={font.DisplayFont.Medium}
      align={align}
    >{text}
    </CustomText>
  )
}

export function DisplaySmall({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.DisplayFont.Small.fontSize + fontSize}
      clr={clr}
      fzz={font.DisplayFont.Small}
      align={align}
    >{text}
    </CustomText>
  )
}

export function HeadlineLarge({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.HeadlineFont.Large.fontSize + fontSize}
      clr={clr}
      fzz={font.HeadlineFont.Large}
      align={align}
    >{text}
    </CustomText>
  )
}

export function HeadlineMedium({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);

  return (
    <CustomText
      fz={font.HeadlineFont.Medium.fontSize + fontSize}
      clr={clr}
      fzz={font.HeadlineFont.Medium}
      align={align}
    >{text}
    </CustomText>
  )
}

export function HeadlineSmall({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.HeadlineFont.Small.fontSize + fontSize}
      clr={clr}
      fzz={font.HeadlineFont.Small}
      align={align}
    >{text}
    </CustomText>
  )
}

export function TitleLarge({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.TitleFont.Large.fontSize + fontSize}
      clr={clr}
      fzz={font.TitleFont.Large}
      align={align}
    >{text}
    </CustomText>
  )
}

export function TitleMedium({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.TitleFont.Medium.fontSize + fontSize}
      clr={clr}
      fzz={font.TitleFont.Medium}
      align={align}
    >{text}
    </CustomText>
  )
}

export function TitleSmall({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.TitleFont.Small.fontSize + fontSize}
      clr={clr}
      fzz={font.TitleFont.Small}
      align={align}
    >{text}
    </CustomText>
  )
}

export function LableLarge({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.LableFont.Large.fontSize + fontSize}
      clr={clr}
      fzz={font.LableFont.Large}
      align={align}
    >{text}
    </CustomText>
  )
}

export function LableMedium({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.LableFont.Medium.fontSize + fontSize}
      clr={clr}
      fzz={font.LableFont.Medium}
      align={align}
    >{text}
    </CustomText>
  )
}

export function LableSmall({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.LableFont.Small.fontSize + fontSize}
      clr={clr}
      fzz={font.LableFont.Small}
      align={align}
    >{text}
    </CustomText>
  )
}

export function BodyLarge({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.BodyFont.Large.fontSize + fontSize}
      clr={clr}
      fzz={font.LableFont.Large}
      align={align}
    >{text}
    </CustomText>
  )
}

export function BodyMedium({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.BodyFont.Medium.fontSize + fontSize}
      clr={clr}
      fzz={font.LableFont.Medium}
      align={align}
    >{text}
    </CustomText>
  )
}


export function BodySmall({ text, clr, align }: TextProperties) {
  const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);
  return (
    <CustomText
      fz={font.BodyFont.Small.fontSize + fontSize}
      clr={clr}
      fzz={font.LableFont.Small}
      align={align}
    >{text}
    </CustomText>
  )
}