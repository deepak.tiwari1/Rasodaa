import { View, Text, StyleSheet, Pressable, Dimensions, TouchableOpacity } from 'react-native';
import React, { useState, useRef, useEffect, useMemo } from 'react'
import * as GS from '../../Management/Utility/GlobalStyles'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../Management'
import { DisplayLarge, DisplaySmall, LableLarge, LableMedium } from '../Utility/TextComponent';
import { UserHomeScreenProps } from '../../Navigation/Screens/UserScreensPropTypes'
import styled from 'styled-components/native'
import { setCustomLocationstatus, setLocation, setLocationGranted } from '../../Management/App/appLocationManager'
import Geolocation from 'react-native-geolocation-service';
import LottieView from 'lottie-react-native';
import { saveAppThemeData_TO_LOCALDB } from '../../Management/App/appApperienceManager'
import { LOCATION_ANIMATION } from '../../assets'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import * as Location from 'expo-location';
import RBSheet from 'react-native-raw-bottom-sheet'
import { BASE_URL } from '../../Constant'
import { getRasodaaAccessToken } from '../../Management/services/AccessTokenManager'

type address = {
  "id": string,
  "customer_id": string,
  "address_title": string,
  "address": string,
  "country": string,
  "state": string,
  "city": string,
  "locality": string,
  "pincode": string,
  "latitude": string,
  "longitude": string,
  "is_default": string,
  "deleted_at": string,
  "created_at": string,
  "updated_at": string,
  "customer_name": string,
  "country_name": string,
  "state_name": string,
  "city_name": string,
  "locality_name": string
}

export default function LocationDisplay(props: UserHomeScreenProps) {
  let location = useSelector((state: RootState) => state.locationaccess);
  const isguest = useSelector((state: RootState) => state.navigationmanager).isGuestMode;
  const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
  const dispatch = useDispatch();
  let [address, setAddress] = useState<address[]>([]);
  const Bsheet = useRef();
  const styles = useMemo(() => createStyles(colors, font), [colors, font])
  const containers = useMemo(() => createContainers(colors), [colors])

  const fetchAddress = async (accessToken: string) => {
    // console.log(accessToken)
    await fetch(BASE_URL + 'customer/addresses', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + accessToken
      }
    }).then(response => response.json()).then(res => {
      if (res.status == 200) {
        // console.log(res.addresses);
        setAddress(res.addresses);
      }
      else {
        alert(res.message);
      }
    })
  }

  useEffect(() => {
    getRasodaaAccessToken(fetchAddress);
  }, [])

  return (
    <View style={styles.header_continer}>
      <View style={[styles.location_container]}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <LottieView
            autoPlay
            style={{ width: 40, height: 40 }}
            source={LOCATION_ANIMATION} />
          <Text
            style={{
              color: colors.OnBackground,
              fontSize: font.HeadlineFont.Small.fontSize,
              fontFamily: font.HeadlineFont.Small.fontFamily,
              paddingTop: 5
            }} >
            {location.location_Name}
          </Text>
          <Pressable style={{ marginLeft: 8 }} onPress={() => { Bsheet.current?.open() }}>
            <SimpleLineIcons name="arrow-down" size={20} color={colors.OnBackground} />
          </Pressable>
        </View>

      </View>
      <View style={{ flexDirection: 'row' }}>
        {location.is_custom_location ? <View>
          <ThemeModeButton
            onPress={() => {
              (async () => {
                let { status } = await Location.requestForegroundPermissionsAsync();
                if (status !== 'granted') {
                  props.navigation.navigate('UserHomeScreen');
                  dispatch(setCustomLocationstatus(true))
                }
                else {
                  let location = await Location.getCurrentPositionAsync({});
                  let locationName = await Location.reverseGeocodeAsync(
                    { latitude: location.coords.latitude, longitude: location.coords.longitude });
                  let payload = {
                    currentLocationName: locationName[0].district,
                    currentLocationAddress: locationName[0].district + ', ' + locationName[0].city + ', ' + locationName[0].region + ' ' + locationName[0].postalCode
                  }

                  dispatch(setLocation(payload));
                  dispatch(setLocationGranted(true));
                  dispatch(setCustomLocationstatus(false))
                }
              })();
            }}
          >
            {location.is_custom_location ? <MaterialIcons name="location-off" size={font.HeadlineFont.Large.fontSize} color={colors.Primary} /> : []}
          </ThemeModeButton>
        </View> : []}
        <View>
          <ThemeModeButton onPress={() => {
            if (colors.modeCode == 'light') {
              dispatch(saveAppThemeData_TO_LOCALDB('dark'));
            }
            else {
              dispatch(saveAppThemeData_TO_LOCALDB('light'));
            }
          }}>
            {colors.modeCode == 'light' ? <Ionicons name="ios-sunny" size={font.HeadlineFont.Large.fontSize} color={colors.Primary} /> : <Ionicons name="ios-moon" size={font.HeadlineFont.Large.fontSize} color={colors.Primary} />}
          </ThemeModeButton>
        </View>

      </View>
      <RBSheet
        ref={Bsheet}
        height={Dimensions.get('window').height / 2}
        animationType='slide'
        onClose={() => { }}
        onOpen={() => { }}
        closeOnDragDown={true}
        dragFromTopOnly={true}
        closeOnPressMask={true}
        closeOnPressBack={true}
        customStyles={{
          container: { backgroundColor: colors.Background }
        }}
        keyboardAvoidingViewEnabled={true}
      >
        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, alignItems: 'center' }}>
            <Text style={{ color: colors.OnBackground, fontSize: 22, fontFamily: font.TitleFont.Large.fontFamily }}>Select Address</Text>
            <TouchableOpacity onPress={() => {
              Bsheet.current?.close()
              props.navigation.navigate('UserAddressManagementScreen')
            }}>
              <Text style={{ color: colors.Primary, fontSize: 14, fontFamily: font.TitleFont.Large.fontFamily }}>Manage</Text>
            </TouchableOpacity>
          </View>
          <View style={containers.view_addressContainer}>
            {address.map((item, index) => {
              return (
                <Pressable key={index} style={containers.single_addressContainer} onPress={() => {
                  alert('API NOT Working')
                }}>
                  <View style={containers.singleaddress_LeftContainer}>
                    {/* <Feather name="home" size={20} color={colors.OnBackground} /> */}
                    <View style={containers.addressdetailsContainer}>
                      <Text style={styles.address_heading}>Home</Text>
                      <Text style={styles.address_subheading}>{item.address} {item.address_title}</Text>
                      <Text style={styles.address_subheading}>{item.city_name} {item.country_name}, {item.pincode}</Text>
                    </View>
                  </View>
                </Pressable>
              )
            })}
          </View>
        </View>
      </RBSheet>
    </View>
  )
}

const ThemeModeButton = styled.TouchableOpacity`
    marginVertical:5px
`

const styles = StyleSheet.create({

})

export const createContainers = (colors) => StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.Background,
    paddingHorizontal: 20
  },
  add_addressContainer: {
    width: '100%',
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  view_addressContainer: {
    paddingVertical: 25,
    marginHorizontal: 5,
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderBottomColor: (colors.modeCode == 'dark' ? 'rgba(243,243,243,.4)' : 'rgba(51,51,51,.4)')
  },
  single_addressContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  singleaddress_LeftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '80%'
  },
  addressdetailsContainer: {
    marginLeft: 15,
  },
  addresssheet_Container: {
    flex: 1,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  adresssheet_addressTypeContainer: {
    width: '100%',
    paddingVertical: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  adresssheet_addressTypeButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 10,
  },
  adresssheet_addressFormContainer: {
    width: '100%',
    paddingHorizontal: 10
  }
})

export const createStyles = (colors, font) => StyleSheet.create({
  header_continer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 8
  },
  location_container: {
    justifyContent: 'center',
    alignItems: 'flex-start',

  },
  headerText: {
    color: colors.OnBackground,
    fontSize: font.HeadlineFont.Small.fontSize,
    fontFamily: font.HeadlineFont.Small.fontFamily,
    alignSelf: 'center',
    marginVertical: 30
  },
  add_addressButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 5,
    backgroundColor: colors.Primary,
    borderRadius: 10,
    height: 40,
    // width: 100
  },
  add_addressButtonText: {
    color: colors.OnPrimary,
    fontSize: font.BodyFont.Large.fontSize,
    fontFamily: font.BodyFont.Large.fontFamily,
  },
  address_heading: {
    fontSize: font.BodyFont.Large.fontSize,
    fontFamily: font.BodyFont.Large.fontFamily,
    color: colors.OnBackground
  },
  address_subheading: {
    fontSize: font.BodyFont.Small.fontSize,
    fontFamily: font.BodyFont.Small.fontFamily,
    color: colors.OnBackground
  },
  addresssheet_Header: {
    fontSize: font.TitleFont.Medium.fontSize,
    fontFamily: font.TitleFont.Medium.fontFamily,
    color: colors.OnBackground,
    alignSelf: 'center',
    marginVertical: 5,
    // borderBottomColor: colors.Outline,
    // borderStyle: "solid",
    // borderBottomWidth: 1

  },
  adresssheet_addressTypesubheader: {
    fontSize: font.BodyFont.Medium.fontSize,
    fontFamily: font.BodyFont.Medium.fontFamily,
    color: colors.OnBackground,

  },
  adresssheet_addressTypeButton_active: {
    borderColor: colors.Secondry,
    borderRadius: 10,
    borderStyle: 'solid',
    borderWidth: 2,
    paddingHorizontal: 10,
    paddingVertical: 2,
    marginLeft: 8,
    elevation: 5,
    backgroundColor: colors.Surface

  },
  adresssheet_addressTypeButton_inactive: {
    borderColor: colors.SecondryColorShades.SC_500,
    borderRadius: 10,
    borderStyle: 'solid',
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 2,
    marginLeft: 8,
    elevation: 5,
    backgroundColor: colors.Surface

  },
  adresssheet_addressTypeButtonText: {
    fontSize: font.BodyFont.Medium.fontSize,
    fontFamily: font.BodyFont.Medium.fontFamily,
    color: colors.OnBackground,
    alignSelf: 'center'
  },
  adresssheet_addressFormInput: {
    borderColor: colors.Outline,
    borderStyle: 'solid',
    borderWidth: 1,
    height: 50,
    width: '100%',
    paddingHorizontal: 20,
    borderRadius: 20,
    marginVertical: 10,

  },
  adresssheet_addressFormsubmitButton: {
    backgroundColor: colors.Primary,
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 13,
  },
  adresssheet_addressFormsubmitButtonText: {
    alignSelf: 'center',
    fontFamily: font.BodyFont.Large.fontFamily,
    fontSize: font.BodyFont.Large.fontSize,
    color: colors.OnPrimary
  }

})
