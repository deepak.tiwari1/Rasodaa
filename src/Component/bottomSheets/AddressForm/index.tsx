import { View, Text, TextInput, Pressable, Dimensions, TouchableOpacity } from 'react-native'
import React, { useState, useRef, useMemo, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../../../Management'
import { createStyles, createContainers } from './styles'
import RBSheet from 'react-native-raw-bottom-sheet'

export default function AddressForm() {
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyles(colors, font), [colors, font])
    const containers = useMemo(() => createContainers(colors), [colors])
    const [firstLine, setfirstLine] = useState("");
    const [secondLine, setsecondLine] = useState("");
    const [city, setcity] = useState("");
    const [state, setstate] = useState("");
    const [zip, setzip] = useState("");
    const refBSheet = useRef()
    return (
        <RBSheet
            ref={refBSheet}
            height={Dimensions.get('window').height / 1.5}
            animationType={'slide'}
            closeOnDragDown={true}
            closeOnPressMask={false}
            customStyles={{ container: { backgroundColor: colors.Background } }}

        >
            <View>
                <Text>Enter complete address</Text>
                <View>
                    <Text>Save address as</Text>
                    <View>
                        <Pressable >
                            <Text>Home</Text>
                        </Pressable>
                        <Pressable>
                            <Text>Work</Text>
                        </Pressable>
                    </View>
                </View>
                <View>
                    <TextInput placeholder='Enter House no, floor no' />
                    <TextInput placeholder='Enter Locality' />
                    <TextInput placeholder='Enter city, pincode' />
                    <TouchableOpacity>
                        <Text>Save address</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </RBSheet>
    )
}