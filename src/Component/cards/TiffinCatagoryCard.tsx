import { View, Text, ScrollView, Pressable, StyleSheet, Dimensions, ImageBackground } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useTheme } from '@react-navigation/native'
//import { MaterialIcons } from '@expo/vector-icons'
/* -------------------------- importing local files ------------------------- */
import { ChangeOpacity } from '../../Management/Utility/GlobalStyles'

import * as Server from '../../Management/Utility/GlobalURLs'
import { TiffinType } from '../../Management/Utility/GlobalInterfaces'
import { P600, P500 } from '../../Management/Utility/GlobalStyles'
import { useSelector } from 'react-redux'
import { RootState } from '../../Management'
import { P800 } from '../../Management/Utility/GlobalStyles'
import { FlatGrid } from 'react-native-super-grid';
import { BodyLarge, BodyMedium, HeadlineMedium, HeadlineSmall, LableLarge, TitleLarge, TitleMedium } from '../Utility/TextComponent'

const staticvalue: TiffinType = {
  id: 0,
  food_preference_type: '',
  tiffin_card_image: '',
  tiffin_background_image: '',
  description: ''
}
const defaultvalue: TiffinType[] = [staticvalue, staticvalue, staticvalue, staticvalue];

export default function FoodCatagory(props) {
  const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
  const [Food_Catagories, setFood_Catagories] = useState(defaultvalue);
  useEffect(() => {
    fetch(Server.API + 'foodtype').then(res => res.json()).then(data => { setFood_Catagories(data.offer.foodtype) })
  }, [])

  const catagorieslist = ['All', 'Fasting', 'Veg', 'NonVeg', 'EGG', 'FastFood']
  return (
    <View style={{ marginVertical: 20, paddingHorizontal: 10, }}>
      <View style={{ paddingVertical: 10, paddingHorizontal: 20, alignItems: 'center' }}>
        {/* <BodyLarge text={'Top Catagories'} align={'center'} clr={colors.OnBackground}  /> */}
        {/* <Text style={{ fontFamily: P800, fontSize: 18, color: colors.OnBackground }}>TOP CATAGORIES</Text> */}
        <Text style={{ fontFamily: P800, fontSize: 18, color: colors.OnBackground }}>TOP CATAGORIES</Text>
      </View>
      <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', paddingVertical: 10 }}>
        {Food_Catagories.map((item, index) => {
          return (
            <View style={{ marginVertical: 10, marginHorizontal: 10 }} key={index}>
              <Pressable onPress={() => {
                props.navigation.navigate('UserTiffinsScreen', {
                  tiffinId: item.id,
                  tiffinName: item.food_preference_type,
                  tiffinImage: item.tiffin_background_image,
                  tiffinDescription: item.description
                })
              }}
                style={[styles.food_catagory_item_container, { borderColor: ChangeOpacity(colors.Primary, '.5') }, { backgroundColor: 'white' }, { shadowColor: colors.Tertiary }]}>
                <ImageBackground
                  source={{ uri: Server.Storage + item.tiffin_card_image }}
                  style={[{ width: '100%', height: '100%', borderRadius: 30 }]}>
                </ImageBackground>
              </Pressable>
              {/* <LableLarge text={} align={'center'} clr={colors.OnBackground} /> */}
              <Text style={{ color: colors.OnBackground, fontSize: font.LableFont.Large.fontSize, fontFamily: font.LableFont.Large.fontFamily, alignSelf: 'center' }}>{item.food_preference_type}</Text>
            </View>

          )
        })
        }

      </View>

    </View>
  )
}

const styles = StyleSheet.create({

  food_catagory_container: {
    paddingTop: 15,
    backgroundColor: 'rgba(243,243,243,1)'
  },
  food_catagory_item_container: {
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: 5,
    height: 90,
    width: 90,
    borderRadius: 45,
    overflow: 'hidden',
    elevation: 10,
    marginBottom: 10,
    // shadowColor:'black',
    // backgroundColor:'#fff',
    // borderStyle:'solid',
    // borderWidth:1,

  },
})
