import { Dimensions, StyleSheet } from 'react-native'
import { P600, P400, P500, P800 } from '../../../Management/Utility/GlobalStyles'
import type { Colors, initialFontdata } from '../../../Management/App/appApperienceManager';

const { width, height } = Dimensions.get('window');

export const createContainers = (colors: Colors) => StyleSheet.create({

})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({

    result: {
        borderRadius: 8,
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginVertical: 10,
        marginLeft: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '90%',
        elevation: 2,
        borderWidth: 1,
        borderStyle: 'solid',

    },
    result_image_container: {
        width: '30%',
        height: 130,
        position: 'relative'
    },
    result_image: {
        width: '100%',
        height: '100%',
        borderRadius: 8
    },
    result_foodtype: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        height: 20,
        width: 40,
        flexDirection: 'row'
    },
    result_foodtype_image: {
        width: '50%',
        height: '100%',
    },
    result_details_container: {
        paddingVertical: 10,

        justifyContent: 'flex-start',
    },
    result_heading: {
        fontSize: 17,
        fontFamily: P600,
        lineHeight: 20
    },
    result_subheading: {
        fontSize: 13,
        fontFamily: P400,
    },
    result_rating_container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: 40
    },
    result_rating_image: {
        width: 20,
        height: 20
    },
    result_rating_text: {

    },
    result_foottime: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 8,
        borderStyle: 'solid',
        borderWidth: 1,
        paddingVertical: 2,
        paddingHorizontal: 10,
        marginRight: 5,
        borderRadius: 4,
        alignSelf: 'flex-start'

    },
    result_foodtime_text: {

        fontSize: 12,
        fontFamily: P400

    },
    tiffin: {
        position: 'absolute',
        right: -13,
        bottom: -4.5,
        height: 50,
        width: 40,
        elevation: 5
    },
    tiffin_image: {
        width: '100%',
        height: '100%',


    },
    listing_container: {
        flex: 1,
    },
    top_cuisines_container: {
        marginVertical: 15,
        // height: 450,
        borderRadius: 10,
    },
    cheff_card: {
        justifyContent: 'space-between',
        // alignItems: 'flex-start',
        borderRadius: 10,
        marginHorizontal: 6,
        marginBottom: 10,
        // height: Dimensions.get('window').height / 2,
        elevation: 2,
        width: width / 2,

    },
    carouselImage: {
        position: 'relative',
        justifyContent: 'space-between',
        height: 150,
        width: width / 2,
    }
}
)
