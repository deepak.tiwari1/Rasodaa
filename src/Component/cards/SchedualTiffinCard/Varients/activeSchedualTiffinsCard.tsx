import { Text, View, StyleSheet, Pressable, Image, ToastAndroid, TouchableOpacity, ScrollView, ImageBackground, Dimensions } from 'react-native';
import React, { useState, useMemo, useRef } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../../Management';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { useNavigation } from '@react-navigation/native';
import { createStyles, createContainers } from '../styles'
import { P500, P400 } from '../../../../Management/Utility/GlobalStyles'
import RBSheet from 'react-native-raw-bottom-sheet'
import { LableMedium } from '../../../../Component/Utility/TextComponent';
import { LableLarge, BodyMedium } from '../../../Utility/TextComponent/index';
import BouncyCheckbox from "react-native-bouncy-checkbox";

export type TiffinCardDataType = {
    tiffinImagePath: string,
    tiffinName: string,
    tiffinCheffName: string,
    tiffinRating: number,
    tiffinType: string,
    tiffinDetails: string[],
    tiffinSeats: string[],
    tiffinNotAvailableNo: number
}

const chefflist = [
    {
        cheffImagePath: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg',
        cheffName: 'Sanjiv Kumar',
        chefRating: 4.9,
        tiffinName: 'Daal Bati Churma',
        tiffinPricechange: '+ ₹40',
        tiffinDetails: [
            '150ml Daal',
            '4 Pcs Ghee Bati',
            '2 Churma Ladu',
            'Salad'
        ],
        addonDetails: [
            { addonname: 'chach', addonprice: '20' },
            { addonname: 'papad', addonprice: '20' },
            { addonname: 'milk', addonprice: '20' },
            { addonname: 'salad', addonprice: '20' },
            { addonname: 'achar', addonprice: '20' },
        ]
    },
    {
        cheffImagePath: 'https://www.geetakiduniya.com/wp-content/uploads/2021/02/aloo-gobi.jpg',
        cheffName: 'Vikram Singh',
        chefRating: 4.2,
        tiffinName: 'Gobhi Aloo ki sabji',
        tiffinPricechange: '- ₹20',
        tiffinDetails: [
            '150ml Gobhi Aloo ki sabji',
            '4 Pcs Ghee Roti',
            '2 Churma Ladu',
            'Salad'
        ],
        addonDetails: [
            { addonname: 'papad', addonprice: '20' },
            { addonname: 'salad', addonprice: '20' },
            { addonname: 'achar', addonprice: '20' },
        ]
    },
    {
        cheffImagePath: 'https://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Quick-Bhindi-Sabzi.jpg',
        cheffName: 'Shilpa Khanna',
        chefRating: 4.8,
        tiffinName: 'Bhindi ki Sabji',
        tiffinPricechange: '- ₹45',
        tiffinDetails: [
            '150ml Sabji',
            '4 Ghee Roti',
            'Salad'
        ],
        addonDetails: [
            { addonname: 'chach', addonprice: '20' },
            { addonname: 'papad', addonprice: '20' },
            { addonname: 'salad', addonprice: '20' },
            { addonname: 'achar', addonprice: '20' },
        ]
    },
    {
        cheffImagePath: 'https://s3.envato.com/files/289816475/DSC_3457.jpg',
        cheffName: 'Riya Kalal singh',
        chefRating: 5,
        tiffinName: 'Sav Tamator',
        tiffinPricechange: '+ ₹23',
        tiffinDetails: [
            '150ml Sav Tamatorl',
            '4 Pcs Ghee Roti',
            'Salad'
        ],
        addonDetails: [
            { addonname: 'chach', addonprice: '20' },
            { addonname: 'papad', addonprice: '20' },
            { addonname: 'masala papad', addonprice: '20' },
            { addonname: 'milk', addonprice: '20' },
            { addonname: 'salad', addonprice: '20' },
            { addonname: 'achar', addonprice: '20' },
        ]
    }

]
const tiffins = [
    {
        tiffinImagePath: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg',
    },
    {
        tiffinImagePath: 'https://www.geetakiduniya.com/wp-content/uploads/2021/02/aloo-gobi.jpg',
    },
    {
        tiffinImagePath: 'https://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Quick-Bhindi-Sabzi.jpg',
    },
    {
        tiffinImagePath: 'https://s3.envato.com/files/289816475/DSC_3457.jpg',
    }

]

export default function activeSchedualTiffinCard(props) {
    // console.log(props)
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyles(colors, font), [colors, font])
    const containers = useMemo(() => createContainers(colors), [colors])
    const navigation = useNavigation();
    const tiffinActionSheet = useRef();
    const tiffinAddonSheet = useRef();
    const newtiffinRef = useRef();
    const swapPricingRef = useRef();
    let tiffinData: TiffinCardDataType = props.data;
    let [tiffinseats, settiffinseats] = useState(tiffinData.tiffinSeats);
    let tiffindetails = tiffinData.tiffinDetails;
    const [is_tiffindetails, setis_tiffindetails] = useState(false);
    const [isswap, setisswap] = useState(true);
    const [isskip, setisskip] = useState(false);
    const [isdonate, setisdonate] = useState(false);
    const [newTiffindetails, setnewTiffindetails] = useState(null);
    const [cheffaddonlist, setcheffaddonlist] = useState([])
    const [selectedaddonList, setselectedaddonList] = useState<{ price: string, addonname: string }[]>([])
    return (
        <TouchableOpacity onPress={() => tiffinActionSheet.current?.open()}
            style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]} >
            <View style={[styles.result_image_container]}>
                <Image style={[styles.result_image]} source={{ uri: tiffinData.tiffinImagePath }} />
                <View style={[styles.tiffin]}>
                    <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                </View>
            </View>

            <View style={[styles.result_details_container, { paddingHorizontal: 10, width: '65%', }]}>
                <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinData.tiffinName}</Text>
                {tiffindetails.map((detail, index) => {
                    return (
                        <View key={index} style={{ flexDirection: 'row' }}>
                            <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                            <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                        </View>
                    )
                })}
            </View>
            <RBSheet
                ref={tiffinActionSheet}
                height={Dimensions.get('window').height / 1.2}
            >
                <View style={{ paddingHorizontal: 20, paddingTop: 20, flex: 1 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingBottom: 20 }}>
                        <Text style={{ color: colors.OnBackground, fontSize: 16, fontWeight: '400', borderColor: colors.Secondery, borderStyle: 'solid', borderWidth: 1, paddingHorizontal: 5, }}>Lunch</Text>

                        <TouchableOpacity onPress={() => tiffinActionSheet.current?.close()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', fontFamily: font.LableFont.Small.fontFamily, marginRight: 10 }}>27 June Mon</Text>
                            <AntDesign name="close" color={colors.OnBackground} size={24} />

                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 10 }}>
                        <Pressable onPress={() => { setisswap(true); setisskip(false); setisdonate(false) }} style={{ width: '30%', paddingVertical: 10, borderRadius: 10, backgroundColor: (isswap == true ? colors.SecondryContainer : 'rgba(243,243,243,1)'), alignItems: 'center' }}>
                            {/* <MaterialIcons name="swap-calls" color={colors.OnBackground} size={20} /> */}
                            <Text style={{ color: colors.OnBackground, fontFamily: font.BodyFont.Small.fontFamily }}>Swap</Text>
                        </Pressable>
                        <Pressable onPress={() => { setisswap(false); setisskip(true); setisdonate(false) }} style={{ width: '30%', paddingVertical: 10, borderRadius: 10, backgroundColor: (isskip == true ? colors.SecondryContainer : 'rgba(243,243,243,1)'), alignItems: 'center' }}>
                            {/* <Ionicons name="play-skip-forward-outline" color={colors.OnBackground} size={20} /> */}
                            <Text style={{ color: colors.OnBackground, fontFamily: font.BodyFont.Small.fontFamily }}>Skip</Text>
                        </Pressable>
                        <Pressable onPress={() => { setisswap(false); setisskip(false); setisdonate(true) }} style={{ width: '30%', paddingVertical: 10, borderRadius: 10, backgroundColor: (isdonate == true ? colors.SecondryContainer : 'rgba(243,243,243,1)'), alignItems: 'center' }}>
                            {/* <FontAwesome5 name="donate" size={24} color={colors.OnBackground} /> */}
                            <Text style={{ color: colors.OnBackground, fontFamily: font.BodyFont.Small.fontFamily }}>Donate</Text>
                        </Pressable>
                    </View>
                    {isswap && (
                        <ScrollView style={{ paddingTop: 20, flex: 1 }}>

                            <View >
                                <View style={{ position: 'relative' }}>
                                    <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', marginLeft: 20 }}>New Tiffin</Text>
                                    {newTiffindetails != null ?
                                        <View>
                                            <TouchableOpacity style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]}
                                                onPress={() => {
                                                    tiffinAddonSheet.current?.open();
                                                    setselectedaddonList([]);
                                                }}
                                            >
                                                <View style={[styles.result_image_container]}>
                                                    <Image style={[styles.result_image]} source={{ uri: newTiffindetails.tiffinImage }} />
                                                    <View style={[styles.tiffin]}>
                                                        <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                                                    </View>
                                                </View>

                                                <View style={[styles.result_details_container, { paddingHorizontal: 10, width: '65%', }]}>
                                                    <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{newTiffindetails.tiffinName}</Text>
                                                    {newTiffindetails.tiffindetails.map((detail, index) => {
                                                        return (
                                                            <View key={index} style={{ flexDirection: 'row' }}>
                                                                <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                                                                <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                                                            </View>
                                                        )
                                                    })}
                                                </View>
                                            </TouchableOpacity>
                                            {selectedaddonList.length >= 1 && <View style={{ position: 'absolute', zIndex: 3, right: 5, top: 0 }}>
                                                <View style={{ backgroundColor: colors.PrimaryContainer, paddingHorizontal: 8, paddingVertical: 4, borderRadius: 20 }}>
                                                    <Text style={{ color: colors.OnPrimaryContainer }}>{selectedaddonList.length}</Text>
                                                </View>
                                            </View>}
                                        </View>
                                        : <TouchableOpacity
                                            onPress={() => {
                                                tiffinAddonSheet.current?.open()
                                            }}
                                            style={{ backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground, borderStyle: 'dashed', borderWidth: 1, height: 150, alignItems: 'center', justifyContent: 'center', marginVertical: 10, marginHorizontal: 20, borderRadius: 10 }} >
                                            <Ionicons name="add" color={colors.OnBackground} size={30} />
                                        </TouchableOpacity>}
                                </View>

                                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                                    <Ionicons name="md-swap-vertical-sharp" color={colors.OnBackground} size={30} />
                                </View>
                                <View>
                                    <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', marginLeft: 20 }}>Current Tiffin</Text>
                                    <View style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]} >
                                        <View style={[styles.result_image_container]}>
                                            <Image style={[styles.result_image]} source={{ uri: tiffinData.tiffinImagePath }} />
                                            <View style={[styles.tiffin]}>
                                                <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                                            </View>
                                        </View>

                                        <View style={[styles.result_details_container, { paddingHorizontal: 10, width: '65%', }]}>
                                            <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinData.tiffinName}</Text>
                                            {tiffindetails.map((detail, index) => {
                                                return (
                                                    <View key={index} style={{ flexDirection: 'row' }}>
                                                        <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                                                        <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                                                    </View>
                                                )
                                            })}
                                        </View>
                                    </View>
                                </View>

                            </View>
                            {newTiffindetails != null && <View style={{ marginTop: 10, marginBottom: 50, paddingHorizontal: 20 }}>
                                <Pressable onPress={() => { swapPricingRef.current?.open(); }} style={{ backgroundColor: colors.Primary, paddingVertical: 10, borderRadius: 10, alignItems: 'center' }}>
                                    <Text style={{ color: colors.OnPrimary, fontFamily: font.LableFont.Small.fontFamily }}>Swap</Text>
                                </Pressable>
                            </View>}
                            <RBSheet
                                ref={tiffinAddonSheet}
                                height={Dimensions.get('window').height / 1}
                                customStyles={{
                                    container: {
                                        backgroundColor: colors.Background,
                                        paddingHorizontal: 20,
                                        paddingTop: 20
                                    }
                                }}
                            >
                                <View style={{ flex: 1, justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingBottom: 20 }}>
                                        <Text style={{ color: colors.OnBackground, fontSize: 16, fontWeight: '400', borderColor: colors.Secondery, borderStyle: 'solid', borderWidth: 1, paddingHorizontal: 5, }}>Lunch</Text>

                                        <TouchableOpacity onPress={() => tiffinAddonSheet.current?.close()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', fontFamily: font.LableFont.Small.fontFamily, marginRight: 10 }}>Add new tiffin</Text>
                                            <AntDesign name="close" color={colors.OnBackground} size={24} />

                                        </TouchableOpacity>
                                    </View>
                                    <ScrollView >
                                        <View>
                                            <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600' }}>Swapable tiffins</Text>
                                            <ScrollView style={styles.top_cuisines_container} horizontal={true}>
                                                {chefflist.map((item, index) => {
                                                    return (
                                                        <Pressable key={index} onPress={() => {
                                                            setnewTiffindetails({
                                                                tiffinImage: item.cheffImagePath,
                                                                tiffinName: item.tiffinName,
                                                                tiffindetails: item.tiffinDetails
                                                            });
                                                            setcheffaddonlist(item.addonDetails);
                                                            setselectedaddonList([]);
                                                        }}
                                                            style={[styles.cheff_card, { backgroundColor: colors.Surface, marginTop: 10, borderRadius: 20, overflow: 'hidden' }]}>
                                                            <View
                                                                style={{ width: '100%', position: 'relative' }}>
                                                                <View style={{ width: '100%', height: 50, }}>
                                                                    <ImageBackground style={styles.carouselImage}
                                                                        source={{ uri: item.cheffImagePath }}></ImageBackground>
                                                                </View>
                                                                <View
                                                                    style={{ position: 'absolute', flexDirection: 'row', top: 10, left: 10 }}>
                                                                    <Image style={{ width: 25, height: 25, borderRadius: 15 }} source={{ uri: 'https://foodondoor.in/assets/user/img/veg.jpg' }} />

                                                                    <Image style={{ width: 25, height: 25, marginLeft: 4, borderRadius: 15 }} source={{ uri: 'https://pngimage.net/wp-content/uploads/2018/06/non-veg-symbol-png-3.jpg' }} />
                                                                </View>


                                                            </View>
                                                            <View
                                                                style={{
                                                                    width: '100%',
                                                                    height: 'auto',
                                                                    justifyContent: 'space-between',
                                                                    paddingVertical: 15,
                                                                    paddingHorizontal: 15,
                                                                    backgroundColor: colors.Surface,
                                                                    borderRadius: 12
                                                                }}>
                                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                                    <Text style={{ fontSize: 14, fontFamily: P500, color: colors.OnSurface, lineHeight: 22, width: '70%' }}>{item.cheffName}</Text>
                                                                    <View
                                                                        style={{ backgroundColor: 'green', flexDirection: 'row', borderRadius: 6, justifyContent: 'center', paddingHorizontal: 5, paddingVertical: 3, alignItems: 'center' }}>

                                                                        <Text style={{ fontSize: 11, fontFamily: font.BodyFont.Small.fontFamily, color: 'white' }}>{item.chefRating}</Text>
                                                                        <Ionicons name="ios-star" size={12} color={'white'} style={{ marginLeft: 5 }} />
                                                                    </View>
                                                                </View>
                                                                <View
                                                                    style={{
                                                                        flexDirection: 'row',
                                                                        justifyContent: 'space-between',
                                                                        alignItems: 'center',
                                                                        width: '100%',
                                                                        marginTop: 5
                                                                    }} >
                                                                    <View style={[styles.result_details_container]}>
                                                                        <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{item.tiffinName}</Text>
                                                                        {item.tiffinDetails.map((detail, index) => {
                                                                            return (
                                                                                <View key={index} style={{ flexDirection: 'row' }}>
                                                                                    <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                                                                                    <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                                                                                </View>
                                                                            )
                                                                        })}
                                                                    </View>

                                                                </View>

                                                            </View>
                                                            <View
                                                                style={{ width: '100%', backgroundColor: 'rgba(245,255,245,1)', borderBottomLeftRadius: 30, borderBottomRightRadius: 30, paddingVertical: 8, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

                                                                <LableMedium text={item.tiffinPricechange} clr={'rgba(0,180,0,1)'} align={'center'} />
                                                            </View>

                                                        </Pressable>
                                                    )
                                                })}
                                            </ScrollView>
                                            <View>
                                                <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Add-Ons</Text>
                                                <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>Addons duration is same as plan duration</Text>
                                                <View style={{ paddingVertical: 10, marginVertical: 10 }}>
                                                    {cheffaddonlist.map((addon, index) => {
                                                        return (
                                                            <View key={index} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                                                <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily }}>{addon.addonname}</Text>
                                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily, marginHorizontal: 10 }}>₹ {addon.addonprice}</Text>
                                                                    <BouncyCheckbox
                                                                        size={25}
                                                                        fillColor={colors.Primary}
                                                                        unfillColor="#FFFFFF"
                                                                        disableText={true}
                                                                        iconStyle={{ borderColor: colors.Secondry }}
                                                                        textStyle={{ fontFamily: font.HeadlineFont.Large.fontFamily }}
                                                                        onPress={(isChecked: boolean) => {
                                                                            const newAddonList = [...selectedaddonList];
                                                                            newAddonList.push(
                                                                                { price: addon.addonprice, addonname: addon.addonname })

                                                                            setselectedaddonList(newAddonList);
                                                                            // console.log(selectedaddonList);
                                                                        }}
                                                                    />
                                                                </View>
                                                            </View>
                                                        )
                                                    })}
                                                </View>
                                            </View>

                                        </View>
                                        <View style={{ marginBottom: 20 }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    // props.navigation.navigate('UserCartScreen');
                                                    tiffinAddonSheet.current?.close()
                                                }}
                                                style={{ backgroundColor: colors.Primary, paddingVertical: 10, width: '100%', borderRadius: 10, alignItems: 'center' }}>
                                                <Text style={{ color: colors.OnPrimary, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Calculate and Swap</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </ScrollView>
                                </View>
                            </RBSheet>
                            <RBSheet
                                ref={swapPricingRef}
                                height={Dimensions.get('window').height}

                            >
                                <View style={{ flex: 1, backgroundColor: colors.Background, paddingHorizontal: 20, paddingVertical: 20, justifyContent: 'space-between' }}>
                                    <View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Large.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Swap Pricing</Text>
                                            <TouchableOpacity
                                                onPress={() => { swapPricingRef.current?.close() }}
                                            >
                                                <AntDesign name="close" color={colors.OnBackground} size={24} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ paddingHorizontal: 5, marginTop: 20 }}>
                                            {newTiffindetails != null && <View>
                                                <View style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]}>
                                                    <View style={[styles.result_image_container]}>
                                                        <Image style={[styles.result_image]} source={{ uri: newTiffindetails.tiffinImage }} />
                                                        <View style={[styles.tiffin]}>
                                                            <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                                                        </View>
                                                    </View>

                                                    <View style={[styles.result_details_container, { paddingHorizontal: 10, width: '65%', }]}>
                                                        <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{newTiffindetails.tiffinName}</Text>
                                                        {newTiffindetails.tiffindetails.map((detail, index) => {
                                                            return (
                                                                <View key={index} style={{ flexDirection: 'row' }}>
                                                                    <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                                                                    <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                                                                </View>
                                                            )
                                                        })}
                                                    </View>
                                                </View>
                                            </View>}
                                            <View style={{ marginTop: 10 }}>

                                                {selectedaddonList.length >= 1 && <View style={{ marginVertical: 20 }}>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Add-Ons</Text>
                                                    {
                                                        selectedaddonList.map((addon, index) => {
                                                            return (
                                                                <View key={index} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: colors.SecondryContainer, paddingHorizontal: 15, paddingVertical: 5, borderRadius: 5 }}>
                                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>{addon.addonname}</Text>
                                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>₹ {addon.price}</Text>
                                                                </View>
                                                            )
                                                        })}

                                                </View>}
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>New tiffin price</Text>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>₹ 40</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomColor: colors.Outline, borderBottomWidth: 1, borderStyle: 'solid' }}>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Total new price</Text>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>₹ 80</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 5 }}>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Current tiffin price</Text>
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>₹ 80</Text>
                                                </View>
                                            </View>

                                        </View>

                                    </View>
                                    <View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 5 }}>
                                            <Text style={{ color: colors.OnBackground, fontSize: 18, fontFamily: font.TitleFont.Medium.fontFamily }}>Grand Total</Text>
                                            <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>₹ 0</Text>
                                        </View>
                                        <Pressable style={{ backgroundColor: colors.Primary, paddingVertical: 10, borderRadius: 10, alignItems: 'center', marginVertical: 20 }}>
                                            <Text style={{ color: colors.OnPrimary, fontFamily: font.LableFont.Small.fontFamily }}>Checkout</Text>
                                        </Pressable>
                                    </View>
                                </View>

                            </RBSheet>
                        </ScrollView>
                    )}
                    {isskip && (
                        <View style={{ paddingVertical: 20, flex: 1, justifyContent: 'space-between' }}>

                            <View>
                                <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', fontFamily: font.LableFont.Small.fontFamily, alignSelf: 'center' }}>I Want to skip this Tiffin</Text>
                                <View style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]} >
                                    <View style={[styles.result_image_container]}>
                                        <Image style={[styles.result_image]} source={{ uri: tiffinData.tiffinImagePath }} />
                                        <View style={[styles.tiffin]}>
                                            <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                                        </View>
                                    </View>

                                    <View style={[styles.result_details_container, { paddingHorizontal: 10, width: '65%', }]}>
                                        <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinData.tiffinName}</Text>
                                        {tiffindetails.map((detail, index) => {
                                            return (
                                                <View key={index} style={{ flexDirection: 'row' }}>
                                                    <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                                                </View>
                                            )
                                        })}
                                    </View>
                                </View>

                            </View>
                            <View style={{ alignItems: 'center', marginVertical: 15 }}>

                                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                                    <Ionicons name="md-swap-vertical-sharp" color={colors.OnBackground} size={30} />
                                </View>

                                <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', fontFamily: font.LableFont.Small.fontFamily, marginRight: 10 }}>13 Auguest Wed</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginHorizontal: 20 }}>
                                <TouchableOpacity onPress={() => {
                                    tiffinActionSheet.current?.close();
                                    navigation.navigate('UserSkipHistoryScreen', { UserID: 1 })
                                }}>
                                    <Text style={{ color: colors.Primary, fontSize: 12, fontFamily: font.BodyFont.Large.fontFamily, textDecorationLine: 'underline' }}>View History</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ backgroundColor: 'rgba(205,235,255,1)', paddingVertical: 20, paddingHorizontal: 15, borderRadius: 10, flexDirection: 'row', alignItems: 'center' }}>
                                <Entypo name="info-with-circle" color={colors.OnSecondryContainer} size={15} />
                                <Text style={{ marginHorizontal: 8, color: colors.OnSecondryContainer, fontSize: 12 }}>If you skip this tiffin for mentioned date and time, We extend your package by adding this tiffin to your package as a last item.</Text>
                            </View>
                            <View>
                                <Pressable style={{ backgroundColor: colors.Primary, paddingVertical: 10, borderRadius: 10, alignItems: 'center', marginTop: 10 }}>
                                    <Text style={{ color: colors.OnPrimary, fontFamily: font.LableFont.Small.fontFamily }}>Skip</Text>
                                </Pressable>
                            </View>
                        </View>
                    )}
                    {isdonate && (
                        <View style={{ paddingVertical: 20, flex: 1, justifyContent: 'space-between' }}>
                            <View>
                                <View style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]} >
                                    <View style={[styles.result_image_container]}>
                                        <Image style={[styles.result_image]} source={{ uri: tiffinData.tiffinImagePath }} />
                                        <View style={[styles.tiffin]}>
                                            <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                                        </View>
                                    </View>

                                    <View style={[styles.result_details_container, { paddingHorizontal: 10, width: '65%', }]}>
                                        <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinData.tiffinName}</Text>
                                        {tiffindetails.map((detail, index) => {
                                            return (
                                                <View key={index} style={{ flexDirection: 'row' }}>
                                                    <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                                                    <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                                                </View>
                                            )
                                        })}
                                    </View>
                                </View>
                                <View style={{ alignItems: 'center', marginVertical: 15 }}>

                                    <View style={{ marginVertical: 10, alignItems: 'center' }}>
                                        <MaterialCommunityIcons name="star-four-points-outline" color={colors.OnBackground} size={40} />
                                    </View>

                                    <Text style={{ color: colors.OnBackground, fontSize: 18, fontWeight: '600', fontFamily: font.LableFont.Small.fontFamily, marginRight: 10 }}>88 Donation Point</Text>
                                </View>
                            </View>
                            <View>
                                <View style={{ backgroundColor: 'rgba(205,235,255,1)', paddingVertical: 20, paddingHorizontal: 15, borderRadius: 10, flexDirection: 'row', alignItems: 'center', marginVertical: 20 }}>
                                    <Entypo name="info-with-circle" color={colors.OnSecondryContainer} size={15} />
                                    <Text style={{ marginHorizontal: 8, color: colors.OnSecondryContainer, fontSize: 12 }}>If you donate your tiffin, We give this tiffin to hungry people in your city. You will get donation points for donation. donation points value are equale to tiffin Price.</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                                    <TouchableOpacity onPress={() => {
                                        tiffinActionSheet.current?.close();
                                        navigation.navigate('Donation', { UserID: 1, view: 'history' })
                                    }}>
                                        <Text style={{ color: colors.Primary, fontSize: 12, fontFamily: font.BodyFont.Large.fontFamily, textDecorationLine: 'underline' }}>View History</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {
                                        tiffinActionSheet.current?.close();
                                        navigation.navigate('Donation', { UserID: 1, view: 'leaderboard' })
                                    }}>
                                        <Text style={{ color: colors.Primary, fontSize: 12, fontFamily: font.BodyFont.Large.fontFamily, textDecorationLine: 'underline' }}>Leaderboard</Text>
                                    </TouchableOpacity>
                                </View>

                                <Pressable style={{ backgroundColor: colors.Primary, paddingVertical: 10, borderRadius: 10, alignItems: 'center', marginTop: 10 }}>
                                    <Text style={{ color: colors.OnPrimary, fontFamily: font.LableFont.Small.fontFamily }}>Donate</Text>
                                </Pressable>
                            </View>
                        </View>
                    )}
                </View>

            </RBSheet>

        </TouchableOpacity>
    );
}
