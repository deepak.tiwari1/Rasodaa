import { Text, View, StyleSheet, Pressable, Image, ToastAndroid } from 'react-native';
import React, { useState, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../../Management';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import { createStyles, createContainers } from '../styles'

export type TiffinCardDataType = {
    tiffinImagePath: string,
    tiffinName: string,
    tiffinCheffName: string,
    tiffinRating: number,
    tiffinType: string,
    tiffinDetails: string[],
    tiffinSeats: string[],
    tiffinNotAvailableNo: number
}

export default function inactiveSchedualTiffinCard(props) {
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyles(colors, font), [colors, font])
    const containers = useMemo(() => createContainers(colors), [colors])

    let tiffinData: TiffinCardDataType = props.data;
    let [tiffinseats, settiffinseats] = useState(tiffinData.tiffinSeats);
    let tiffindetails = tiffinData.tiffinDetails;
    const [is_tiffindetails, setis_tiffindetails] = useState(false);
    return (
        <View
            style={[styles.result, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.OnBackground }]} onPress={() => setis_tiffindetails(!is_tiffindetails)}>
            <View style={[styles.result_image_container]}>
                <Image style={[styles.result_image]} source={{ uri: tiffinData.tiffinImagePath }} />
                <View style={[styles.tiffin]}>
                    <Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
                </View>
            </View>

            <View style={[styles.result_details_container]}>
                <Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinData.tiffinName}</Text>
                {tiffindetails.map((detail, index) => {
                    return (
                        <View key={index} style={{ flexDirection: 'row' }}>
                            <Entypo name="dot-single" size={20} color={colors.OnSurface} />
                            <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
                        </View>
                    )
                })}
            </View>

        </View>
    );
}
