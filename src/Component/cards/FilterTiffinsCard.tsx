import { View, Text, Pressable, StyleSheet, Image } from 'react-native'
import React from 'react'
import * as GS from '../../Management/Utility/GlobalStyles'


export default function Cheff_Tiffins({ props }) {

	//console.log(props)
	return (
		<Pressable style={[styles.result]}
			onPress={() => {
				props.navigation.navigate('CheffProfileScreen', {
					cheffId: 1,
					cheffName: props.cheffData.cheffName,
					cheffImage: props.cheffData.cheffImage
				})
			}}
		>
			<View style={[styles.result_image_container]}>
				<Image style={[styles.result_image]} source={{ uri: props.cheffData.cheffImage }} />
				<View style={[styles.result_foodtype]}>
					<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkey.com/png/full/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png' }} />
					<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkit.com/png/full/257-2579552_non-veg-symbol-non-veg-symbol-png.png' }} />
				</View>
			</View>
			<View style={[styles.result_details_container]}>
				<Text style={[styles.result_heading]}>{props.cheffData.cheffName}</Text>
				<Text style={[styles.result_subheading]}>Rajathani, Gujrati</Text>
				<View style={[styles.result_rating_container]}>
					<Image style={[styles.result_rating_image]} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTz5DhDXUWPfmBLh_xkhH2S2i5jlI0mdricgmC-SbztG66JNaTe7hqqXDGvT4_CJEbV_fw&usqp=CAU' }} />
					<Text style={[styles.result_rating_text]}>5</Text>
				</View>
				<View style={[styles.result_foottime]}>
					<Text style={[styles.result_foodtime_text]}>Lunch</Text>
					<Text style={[styles.result_foodtime_text]}>Dinner</Text>
				</View>
			</View>

		</Pressable>

	)
}
const styles = StyleSheet.create({
	card_Container: {
		paddingHorizontal: 5,
		marginVertical: 10,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',

	},
	results: {
		marginVertical: 20,
		paddingHorizontal: 20
	},
	result: {
		borderRadius: 8,
		backgroundColor: 'white',
		paddingVertical: 5,
		paddingHorizontal: 5,
		marginVertical: 5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: 120,
		elevation: 5

	},
	result_image_container: {
		width: '30%',
		height: '100%',
		position: 'relative'
	},
	result_image: {
		width: '100%',
		height: '100%',
		borderRadius: 8
	},
	result_foodtype: {
		position: 'absolute',
		right: 0,
		bottom: 0,
		height: 20,
		width: 40,
		flexDirection: 'row'
	},
	result_foodtype_image: {
		width: '50%',
		height: '100%',
	},
	result_details_container: {
		paddingHorizontal: 15,
		paddingVertical: 10,
		width: '65%',
		height: '100%',
		justifyContent: 'flex-start'
	},
	result_heading: {
		fontSize: 18,
		fontFamily: GS.P600,
		lineHeight: 20
	},
	result_subheading: {
		fontSize: 14,
		fontFamily: GS.P400,
	},
	result_rating_container: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		width: 40
	},
	result_rating_image: {
		width: 20,
		height: 20
	},
	result_rating_text: {
		color: 'black'
	},
	result_foottime: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		height: 35,
	},
	result_foodtime_text: {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: 'rgba(242,82,112,1)',
		paddingVertical: 2,
		paddingHorizontal: 10,
		marginRight: 5,
		borderRadius: 4,
		fontSize: 12,
		fontFamily: GS.P400

	},
})
