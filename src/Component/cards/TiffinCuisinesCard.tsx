import { View, Text, Pressable, ScrollView, StyleSheet, Dimensions, Image, ImageBackground } from 'react-native'
import React, { useEffect, useState, useRef } from 'react';
import * as GS from '../../Management/Utility/GlobalStyles';
import { useTheme } from '@react-navigation/native';
//import { MaterialIcons } from '@expo/vector-icons';
import * as Server from '../../Management/Utility/GlobalURLs';
import { TiffinCuidines } from '../../Management/Utility/GlobalInterfaces';
import { useSelector } from 'react-redux'
import { RootState } from '../../Management'
import { LableLarge } from '../Utility/TextComponent';
const staticvalue: TiffinCuidines = {
  id: 0,
  title: '',
  tiffin_card_image: '',
  tiffin_background_image: '',
  description: ''
}

const defaultvalue: TiffinCuidines[] = [staticvalue, staticvalue, staticvalue, staticvalue];

export default function FoodCuisines(props) {
  const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
  const [cuisines, Setcuisines] = useState(defaultvalue);
  const scrollref = useRef<ScrollView>();
  useEffect(() => {

    fetch(Server.API + 'cuisines')
      .then(res => res.json())
      .then(data => { Setcuisines(data.cuisines.cuisines) })
      .catch(err => console.log(err))
    scrollref.current?.scrollTo({ x: 100, y: 0, animated: false })
    // console.log(cuisines)
  }, [])

  const cuisineslist = ['Rajasthani', 'Gujarati', 'Chinese', 'Hydrabadi', 'Italian', 'Maharastra', 'Mexican', 'Mughlai', 'Panjabi', 'Sindhi', 'Thai', 'Tamil', 'Bangali']

  return (
    <View
      style={{ marginVertical: 10, backgroundColor: colors.Background, borderBottomColor: colors.BackgroundColorShades.BC_100, borderBottomWidth: 3, borderStyle: 'solid' }}>
      <View
        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginHorizontal: 25, marginBottom: 15 }}>
        <Text style={{ fontFamily: GS.P800, fontSize: 18, color: colors.OnBackground }}>TOP CUISINES</Text>
      </View>
      <ScrollView
        style={styles.top_cuisines_container}
        ref={scrollref}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        pagingEnabled={true} >

        {cuisines.reverse().map((item: TiffinCuidines, index) => {
          return (
            <View key={index}>
              <Pressable
                onPress={() => {
                  props.navigation.navigate('UserTiffinsScreen', {
                    tiffinId: item.id,
                    tiffinName: item.title,
                    tiffinImage: "thumb/" + item.image,
                    tiffinDescription: item.description,
                  })
                }}
                style={[styles.food_cuisines_item_container, { borderColor: colors.Outline },
                (Server.Storage + item.tiffin_card_image != undefined) ? { backgroundColor: colors.Outline } : { backgroundColor: 'white' }]}>
                {/* <MaterialIcons name="lunch-dining" size={40} color={colors.text} /> */}

                <ImageBackground
                  source={{ uri: Server.Storage + "thumb/" + item.image }}
                  style={[{ width: '100%', height: '100%' }]} >
                  <View
                    style={[{ width: '100%', height: '100%', justifyContent: 'flex-end', alignItems: 'center', paddingVertical: 10 }]}>

                  </View>
                </ImageBackground>

              </Pressable>
              <Text style={{ color: colors.OnBackground, fontSize: font.LableFont.Large.fontSize, fontFamily: font.LableFont.Large.fontFamily, alignSelf: 'center' }}>{item.title}</Text>

            </View>
          )
        })}

      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  top_cuisines_container: {
    marginTop: 15,
    marginBottom: 30,
    paddingLeft: 10

  },
  food_cuisines_item_container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 5,
    height: 150,
    width: Dimensions.get('window').width / 3.8,
    borderRadius: 20,
    overflow: 'hidden',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    elevation: 10,
    marginBottom: 10,
    // shadowColor:'black',
    // backgroundColor:'#fff',
    // borderStyle:'solid',
    // borderWidth:1,
  },
})
