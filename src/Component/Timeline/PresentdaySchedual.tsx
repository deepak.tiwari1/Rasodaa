import { View, Text, StyleSheet, TouchableOpacity, Pressable, Image, ToastAndroid } from 'react-native'
import React, { useState } from 'react'
import * as GS from '../../Management/Utility/GlobalStyles'
import * as DT from 'date-fns'
import { useTheme } from '@react-navigation/native';
//import { Entypo, FontAwesome5, AntDesign } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../Management';
// import * as Animatable from 'react-native-animatable';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';

export type TiffinCardDataType = {
	tiffinImagePath: string,
	tiffinName: string,
	tiffinCheffName: string,
	tiffinRating: number,
	tiffinType: string,
	tiffinDetails: string[],
	tiffinSeats: string[],
	tiffinNotAvailableNo: number
}

export default function PresentdaySchedual({ props }) {
	let today = new Date();
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const Timeline_Date = DT.format(new Date(today.getFullYear(), today.getMonth(), today.getDate() + props.dateoftime - 1), 'dd MMMMMM yyyy');
	const Dayofweek = DT.format(new Date(today.getDay() + props.dateoftime - 1), 'E');
	const totaltime = Array.from({ length: today.getHours() }, (_, i) => i + 1)

	let tiffinlunch: TiffinCardDataType = props.lunch;
	let [tiffinlunchseats, settiffinlunchseats] = useState(tiffinlunch.tiffinSeats);
	let tiffinlunchdetails = tiffinlunch.tiffinDetails;
	const [is_tiffinlunchdetails, setis_tiffinlunchdetails] = useState(false);

	let tiffinDinner: TiffinCardDataType = props.dinner;
	let [tiffinDinnerseats, settiffinDinnerseats] = useState(tiffinDinner.tiffinSeats);
	let tiffinDinnerdetails = tiffinDinner.tiffinDetails;
	const [is_tiffinDinnerdetails, setis_tiffinDinnerdetails] = useState(false);

	return (

		<View style={styles.container}>
			<View style={{ flexDirection: 'row', justifyContent: 'center', width: '100%', marginBottom: 20 }}>
				<View style={{ backgroundColor: colors.Secondry, height: 30, borderRadius: 20, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20, elevation: 4 }}>

					<Text style={{ fontFamily: GS.P600, fontSize: 16, color: colors.OnSecondry }}>Today</Text>
				</View>
			</View>

			{/* /* ---------------------------------- Lunch ---------------------------------  */}
			<View style={[styles.result, { borderColor: colors.Outline, backgroundColor: colors.Surface, shadowColor: colors.OnSurface }]} >
				<View style={[styles.result_image_container]}>
					<Image style={[styles.result_image]} source={{ uri: tiffinlunch.tiffinImagePath }} />
					<View style={[styles.tiffin]}>
						<Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
					</View>
				</View>

				<View style={[styles.result_details_container]}>
					<Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinlunch.tiffinName}</Text>
					{tiffinlunchdetails.map((detail, index) => {
						return (
							<View key={index} style={{ flexDirection: 'row' }}>
								<Entypo name="dot-single" size={20} color="rgba(51,51,51,1)" />
								<Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
							</View>
						)
					})}
				</View>


			</View>
			{/* /* ---------------------------------- Dinner ---------------------------------  */}
			<View
				style={[styles.result, { borderColor: colors.Outline, backgroundColor: colors.Surface, shadowColor: colors.OnSurface }]}
				onPress={() => setis_tiffinDinnerdetails(!is_tiffinDinnerdetails)}>
				<View style={[styles.result_image_container]}>
					<Image style={[styles.result_image]} source={{ uri: tiffinDinner.tiffinImagePath }} />
					<View style={[styles.tiffin]}>
						<Image style={[styles.tiffin_image]} source={{ uri: 'https://5.imimg.com/data5/PU/HY/MY-364237/steel-tiffin-box-500x500.png' }} />
					</View>
				</View>
				<View style={[styles.result_details_container]}>
					<Text style={[styles.result_heading, { color: colors.OnSurface }]}>{tiffinDinner.tiffinName}</Text>
					{tiffinDinnerdetails.map((detail, index) => {
						return (
							<View key={index} style={{ flexDirection: 'row' }}>
								<Entypo name="dot-single" size={20} color="rgba(51,51,51,1)" />
								<Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>{detail}</Text>
							</View>
						)
					})}
				</View>


			</View>


		</View>

	)
}

const styles = StyleSheet.create({
	container: {
		// height: 430,
		width: '100%',
		paddingTop: 5,
		paddingBottom: 30,
		// flexDirection: 'row',
		alignItems: 'flex-start',
		// marginBottom: 20,
		// borderBottomWidth: 1,
		// borderBottomColor: 'rgba(51,51,51,.2)',
		// borderStyle: 'solid',


	},
	timeline_details_container: {
		width: '100%',
		justifyContent: 'center',
	},
	result: {
		borderRadius: 8,
		paddingVertical: 5,
		paddingHorizontal: 5,
		marginVertical: 10,
		marginLeft: 20,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderWidth: 1,
		borderStyle: 'solid',
		// width: '90%',
		elevation: 2

	},
	result_image_container: {
		width: '30%',
		height: 130,
		position: 'relative'
	},
	result_image: {
		width: '100%',
		height: '100%',
		borderRadius: 8
	},
	result_foodtype: {
		position: 'absolute',
		right: 0,
		bottom: 0,
		height: 20,
		width: 40,
		flexDirection: 'row'
	},
	result_foodtype_image: {
		width: '50%',
		height: '100%',
	},
	result_details_container: {
		paddingLeft: 25,
		paddingRight: 10,
		paddingVertical: 10,
		width: '65%',
		justifyContent: 'flex-start',
		alignItems: 'flex-start',
	},
	result_heading: {
		fontSize: 17,
		fontFamily: GS.P600,
		lineHeight: 20
	},
	result_subheading: {
		fontSize: 13,
		fontFamily: GS.P400,
	},
	result_rating_container: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		width: 40
	},
	result_rating_image: {
		width: 20,
		height: 20
	},
	result_rating_text: {

	},
	result_foottime: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginTop: 8,
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: 'rgba(242,82,112,1)',
		paddingVertical: 2,
		paddingHorizontal: 10,
		marginRight: 5,
		borderRadius: 4,
		alignSelf: 'flex-start',
	},
	result_foodtime_text: {
		fontSize: 12,
		fontFamily: GS.P400

	},
	tiffin: {
		position: 'absolute',
		right: -13,
		bottom: -4.5,
		height: 50,
		width: 40,
		elevation: 5
	},
	tiffin_image: {
		width: '100%',
		height: '100%',

	}

})
