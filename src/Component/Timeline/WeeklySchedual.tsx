import { View, Text, StyleSheet, TouchableOpacity, Dimensions, Image, Pressable } from 'react-native'
import React from 'react'
//import { AntDesign } from '@expo/vector-icons';
import * as GS from '../../Management/Utility/GlobalStyles'
import * as DT from 'date-fns'
import ActiveSchedualTiffinsCard from '../cards/SchedualTiffinCard/Varients/activeSchedualTiffinsCard';
import InactiveSchedualTiffinsCard from '../cards/SchedualTiffinCard/Varients/inactiveSchedualTiffinCard';
import type { TiffinCardDataType } from '../cards/SchedualTiffinCard'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../Management';

export default function WeeklySchedual({ props, cardhavefunction }) {
    // console.log(cardhavefunction)
    let today = new Date();
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const Timeline_Date = DT.format(new Date(today.getFullYear(), today.getMonth(), today.getDate() + props.dateoftime - 1), 'LLLL, E');
    // const Dayofweek = DT.format(today.getDate()+props.dateoftime,'EEEEEE');
    // console.log(Timeline_Date)

    let lunchTiffinData: TiffinCardDataType = props.lunch;
    let dinnerTiffinData: TiffinCardDataType = props.dinner;

    return (

        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', width: '100%' }}>
                <View style={{ backgroundColor: colors.Secondry, height: 30, borderRadius: 20, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20, elevation: 4 }}>
                    <Text style={{ fontFamily: GS.P800, fontSize: 16, color: colors.OnSecondry, marginRight: 10 }}>{today.getDate() + props.dateoftime - 1}</Text>
                    <Text style={{ fontFamily: GS.P600, fontSize: 16, color: colors.OnSecondry }}>{Timeline_Date}</Text>
                </View>
            </View>
            <View style={styles.timeline_details_container}>

                {/* /* ---------------------------------- Lunch ---------------------------------  */}
                {!cardhavefunction ? (<InactiveSchedualTiffinsCard data={lunchTiffinData} />) : (<ActiveSchedualTiffinsCard data={lunchTiffinData} />)}
                {/* /* ---------------------------------- Dinner ---------------------------------  */}
                {!cardhavefunction ? (<InactiveSchedualTiffinsCard data={dinnerTiffinData} />) : (<ActiveSchedualTiffinsCard data={dinnerTiffinData} />)}
            </View>

        </View>


    )
}

const styles = StyleSheet.create({
    container: {
        height: 400,
        width: '100%',
        // flexDirection: 'row',
        alignItems: 'flex-start',
        marginVertical: 10,
        // marginHorizontal: 10,
        // borderWidth: 1,
        // borderColor: 'black',
        // borderStyle: 'dotted',


    },
    timelinebar_container: {
        marginRight: 10,
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    timebar_background: {
        height: '100%',
        width: 15,
        borderColor: 'black',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderStyle: 'solid',
        // borderRadius: 10,
    },
    timebar_forground: {
        // height:16.66,
        width: '100%',
    },

    timeline_details_container: {
        marginTop: 10,
        // height: '100%',
        width: '100%',
        justifyContent: 'center',
    },
    timeline_details: {
        height: '50%',
        width: '100%',
        alignItems: 'center',
        // justifyContent: 'flex-start',
        // borderWidth: 1,
        // borderColor: 'black',
        // borderStyle: 'solid',
    },
    bordertop: {
        borderRightWidth: 1,
        borderStyle: 'solid',
    },

})
