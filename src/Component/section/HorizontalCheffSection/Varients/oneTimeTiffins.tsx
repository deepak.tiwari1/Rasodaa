import { View, Text, Pressable, ScrollView, StyleSheet, Dimensions, Image, ImageBackground, FlatList, TouchableOpacity } from 'react-native'
import React, { useEffect, useState, useRef } from 'react'
import { useTheme } from '@react-navigation/native'
import * as GS from '../../../../Management/Utility/GlobalStyles'
import * as Server from '../../../../Management/Utility/GlobalURLs'
import { useSelector } from 'react-redux'
import { RootState } from '../../../../Management'
import { TitleLarge, TitleMedium, LableLarge, LableMedium, LableSmall, BodyLarge, BodySmall, BodyMedium } from '../../../Utility/TextComponent';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { P800 } from '../../../../Management/Utility/GlobalStyles'
import LottieView from 'lottie-react-native'
import { CHEFFOFFER } from '../../../../assets'
import RBSheet from 'react-native-raw-bottom-sheet'
import BouncyCheckbox from 'react-native-bouncy-checkbox'

const chefflist = [
    {
        cheffImagePath: 'https://foodfood.b-cdn.net/images/Awy4hBMasterchef-Sanjeev-Kapoor.jpg',
        cheffName: 'Sanjiv Kumar',
        chefRating: 4.9,
    },
    {
        cheffImagePath: 'https://static.wixstatic.com/media/d574e5_9a34e19d514941b6a1557b7cc882c759~mv2_d_4000_6000_s_4_2.jpg/v1/crop/x_0,y_594,w_4000,h_4000/fill/w_400,h_400,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/IMG_1617.jpg',
        cheffName: 'Vikram Singh',
        chefRating: 4.2,
    },
    {
        cheffImagePath: 'https://www.funmansite.com/wp-content/uploads/PicsArt_11-20-06.14.43.jpg',
        cheffName: 'Shilpa Khanna',
        chefRating: 4.8,
    },
    {
        cheffImagePath: 'https://foodandtravelsecrets.com/wp-content/uploads/2018/04/IMG_7308-1000x1000.jpg',
        cheffName: 'Riya Kalal singh',
        chefRating: 5,
    }

]

const tiffins = [
    {
        tiffinImagePath: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg',
    },
    {
        tiffinImagePath: 'https://www.geetakiduniya.com/wp-content/uploads/2021/02/aloo-gobi.jpg',
    },
    {
        tiffinImagePath: 'https://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Quick-Bhindi-Sabzi.jpg',
    },
    {
        tiffinImagePath: 'https://s3.envato.com/files/289816475/DSC_3457.jpg',
    }

]

export default function HorizontalOneTimeTiffin({ props, title }) {
    const [cheffs, Setcheffs] = useState([1]);
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const scrollref = useRef<ScrollView>();
    // const cuisineslist = ['1', 3, 4, 5, 5, 6, 6, 6]
    const oneTimeTiffinRef = useRef();
    const Container_title = title
    useEffect(() => {
        // fetch(Server.API+'cheffs').then(res=>res.json()).then(data=>{Setcheffs(data.cheffs.cheffs)}).catch(err=>console.log(err))
        scrollref.current?.scrollTo({ x: 100, y: 0, animated: false })
    }, [])

    return (
        <View
            style={[styles.listing_container, { paddingLeft: 10, paddingTop: 20 }]}>
            <View
                style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    marginHorizontal: 13, paddingRight: 10
                }}>
                <Text style={{ fontFamily: P800, fontSize: 18, color: colors.OnBackground }}>{Container_title}</Text>
            </View>
            <ScrollView style={styles.top_cuisines_container} ref={scrollref} horizontal={true}>
                {chefflist.map((item, index) => {
                    return (
                        <Pressable key={index} onPress={() => {
                            oneTimeTiffinRef.current?.open();
                        }}
                            style={[styles.cheff_card, { backgroundColor: colors.Surface, marginTop: 10, borderRadius: 20, overflow: 'hidden' }]}>
                            <View
                                style={{ width: '100%', position: 'relative' }}>
                                <View style={{ width: '100%', height: 150, }}>
                                    <ImageBackground style={styles.carouselImage}
                                        source={{ uri: tiffins[index].tiffinImagePath }}></ImageBackground>
                                </View>
                                <View
                                    style={{ position: 'absolute', flexDirection: 'row', top: 10, left: 10 }}>
                                    <Image style={{ width: 25, height: 25, borderRadius: 15 }} source={{ uri: 'https://foodondoor.in/assets/user/img/veg.jpg' }} />

                                    <Image style={{ width: 25, height: 25, marginLeft: 4, borderRadius: 15 }} source={{ uri: 'https://pngimage.net/wp-content/uploads/2018/06/non-veg-symbol-png-3.jpg' }} />
                                </View>
                                {index % 2 == 0 ? <View
                                    style={{ position: 'absolute', flexDirection: 'row', bottom: 10, right: 0, backgroundColor: 'rgba(250,250,250,.8)', borderTopLeftRadius: 10, borderBottomLeftRadius: 10, paddingHorizontal: 10 }}>

                                    <LottieView source={CHEFFOFFER} autoPlay={true} style={{ height: 30, }} />
                                </View> : []}

                            </View>
                            <View
                                style={{
                                    width: '100%',
                                    height: 'auto',
                                    justifyContent: 'space-between',
                                    paddingVertical: 15,
                                    paddingHorizontal: 15,
                                    backgroundColor: colors.Surface,
                                    borderRadius: 12
                                }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 12, fontFamily: GS.P500, color: colors.OnSurface, lineHeight: 22, width: '70%' }}>{item.cheffName}</Text>
                                    <View
                                        style={{ backgroundColor: 'green', flexDirection: 'row', borderRadius: 6, justifyContent: 'center', paddingHorizontal: 5, paddingVertical: 3, alignItems: 'center' }}>

                                        <Text style={{ fontSize: 11, fontFamily: font.BodyFont.Small.fontFamily, color: 'white' }}>{item.chefRating}</Text>
                                        <Ionicons name="ios-star" size={12} color={'white'} style={{ marginLeft: 5 }} />
                                    </View>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        width: '100%',
                                        marginTop: 5
                                    }} >
                                    <View style={{ flexDirection: 'row' }}>
                                        <View
                                            style={{
                                                // marginHorizontal: 5,
                                                borderRadius: 6,
                                                paddingVertical: 2,
                                                paddingHorizontal: 5,
                                                borderColor: colors.Secondry,
                                                borderWidth: 1,
                                                borderStyle: 'solid'
                                            }}>
                                            <Text style={{ fontSize: 10, fontFamily: GS.P400, color: colors.OnSurface }}>Dinner</Text>
                                        </View>
                                        <View
                                            style={{
                                                marginLeft: 5,
                                                borderRadius: 6,
                                                paddingVertical: 2,
                                                paddingHorizontal: 5,
                                                borderColor: colors.Secondry,
                                                borderWidth: 1,
                                                borderStyle: 'solid'
                                            }}>
                                            <Text style={{ fontSize: 10, fontFamily: GS.P400, color: colors.OnSurface }}>Lunch</Text>
                                        </View>
                                    </View>

                                </View>

                            </View>
                            <View
                                style={{ width: '100%', backgroundColor: 'rgba(245,255,245,1)', borderBottomLeftRadius: 30, borderBottomRightRadius: 30, paddingVertical: 8, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

                                <LableMedium text={'₹60 for 1 Tiffin'} clr={'rgba(0,180,0,1)'} align={'center'} />
                            </View>

                        </Pressable>
                    )
                })}
            </ScrollView>
            <RBSheet
                ref={oneTimeTiffinRef}
                height={Dimensions.get('window').height / 1.4}
                customStyles={{
                    container: {
                        backgroundColor: colors.Background,
                        paddingHorizontal: 20,
                        paddingVertical: 20
                    }
                }}
            >
                <View style={{ justifyContent: 'space-between', flex: 1 }}>
                    <View>
                        <View style={{ borderBottomColor: 'rgba(51,51,51,.2)', borderBottomWidth: 1, borderStyle: 'solid', paddingVertical: 10, marginVertical: 10 }}>
                            <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Large.fontSize, fontFamily: font.TitleFont.Large.fontFamily }}>{'Bhindi Ki Sabji'}</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily }}>{'1 Day'} plan</Text>
                                <TouchableOpacity style={{ marginHorizontal: 10 }} onPress={() => { oneTimeTiffinRef.current?.close() }}>
                                    <Text style={{ color: colors.Primary, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>change</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Add-Ons</Text>
                            <Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>Addons duration is same as plan duration</Text>
                            <View style={{ paddingVertical: 10, marginVertical: 10 }}>
                                {['chach', 'papad', 'milk', 'salad', 'achar'].map((addon, index) => {
                                    return (
                                        <View key={index} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                            <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily }}>{addon}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                                                <Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily, marginHorizontal: 10 }}>₹45</Text>
                                                <BouncyCheckbox
                                                    size={25}
                                                    fillColor={colors.Primary}
                                                    unfillColor="#FFFFFF"
                                                    disableText={true}
                                                    iconStyle={{ borderColor: colors.Secondry }}
                                                    textStyle={{ fontFamily: font.HeadlineFont.Large.fontFamily }}
                                                    onPress={(isChecked: boolean) => { }}
                                                />
                                            </View>
                                        </View>
                                    )
                                })}
                            </View>
                        </View>

                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                props.navigation.navigate('UserCartScreen');
                                oneTimeTiffinRef.current?.close()
                            }}
                            style={{ backgroundColor: colors.Primary, paddingVertical: 10, width: '100%', borderRadius: 10, alignItems: 'center' }}>
                            <Text style={{ color: colors.OnPrimary, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Add addon to cart</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    listing_container: {
        flex: 1,
    },
    top_cuisines_container: {
        marginVertical: 15,
        // height: 450,
        borderRadius: 10,
    },
    cheff_card: {
        justifyContent: 'space-between',
        // alignItems: 'flex-start',
        borderRadius: 10,
        marginHorizontal: 6,
        marginBottom: 10,
        // height: Dimensions.get('window').height / 2,
        elevation: 2,
        width: Dimensions.get('window').width / 2.4,

    },
    carouselImage: {
        position: 'relative',
        justifyContent: 'space-between',
        height: 150,
        width: Dimensions.get('window').width / 2.4,
    },
})
