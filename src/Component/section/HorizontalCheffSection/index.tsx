import { View, Text, Pressable, ScrollView, StyleSheet, Dimensions, Image, ImageBackground, FlatList } from 'react-native'
import React, { useEffect, useState, useRef } from 'react'
import { useTheme } from '@react-navigation/native'
import * as GS from '../../../Management/Utility/GlobalStyles'
import * as Server from '../../../Management/Utility/GlobalURLs'
import { useSelector } from 'react-redux'
import { RootState } from '../../../Management'
import { TitleLarge, TitleMedium, LableLarge, LableMedium, LableSmall, BodyLarge, BodySmall, BodyMedium } from '../../Utility/TextComponent';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { P800 } from '../../../Management/Utility/GlobalStyles'
import LottieView from 'lottie-react-native'
import { CHEFFOFFER } from '../../../assets'

const chefflist = [
	{
		cheffImagePath: 'https://foodfood.b-cdn.net/images/Awy4hBMasterchef-Sanjeev-Kapoor.jpg',
		cheffName: 'Sanjiv Kumar',
		chefRating: 4.9,
	},
	{
		cheffImagePath: 'https://static.wixstatic.com/media/d574e5_9a34e19d514941b6a1557b7cc882c759~mv2_d_4000_6000_s_4_2.jpg/v1/crop/x_0,y_594,w_4000,h_4000/fill/w_400,h_400,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/IMG_1617.jpg',
		cheffName: 'Vikram Singh',
		chefRating: 4.2,
	},
	{
		cheffImagePath: 'https://www.funmansite.com/wp-content/uploads/PicsArt_11-20-06.14.43.jpg',
		cheffName: 'Shilpa Khanna',
		chefRating: 4.8,
	},
	{
		cheffImagePath: 'https://foodandtravelsecrets.com/wp-content/uploads/2018/04/IMG_7308-1000x1000.jpg',
		cheffName: 'Riya Kalal singh',
		chefRating: 5,
	},
	{
		cheffImagePath: 'https://foodandtravelsecrets.com/wp-content/uploads/2018/04/IMG_7308-1000x1000.jpg',
		cheffName: 'Riya Kalal singh',
		chefRating: 5,
	},
	{
		cheffImagePath: 'https://foodandtravelsecrets.com/wp-content/uploads/2018/04/IMG_7308-1000x1000.jpg',
		cheffName: 'Riya Kalal singh',
		chefRating: 5,
	},
	{
		cheffImagePath: 'https://foodandtravelsecrets.com/wp-content/uploads/2018/04/IMG_7308-1000x1000.jpg',
		cheffName: 'Riya Kalal singh',
		chefRating: 5,
	},
	{
		cheffImagePath: 'https://foodandtravelsecrets.com/wp-content/uploads/2018/04/IMG_7308-1000x1000.jpg',
		cheffName: 'Riya Kalal singh',
		chefRating: 5,
	}


]


export default function HorizontalCheffSection({ props, title }) {

	const [cheffs, Setcheffs] = useState([1]);
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	// const cuisineslist = ['1', 3, 4, 5, 5, 6, 6, 6]
	const Container_title = title
	// const scrollref = useRef<FlatList>();
	useEffect(() => {
		// fetch(Server.API+'cheffs').then(res=>res.json()).then(data=>{Setcheffs(data.cheffs.cheffs)}).catch(err=>console.log(err))
		// scrollref.current?.scrollToIndex({ index: 0, animated: true });
	}, [])

	return (
		<View
			style={[styles.listing_container, { paddingLeft: 10, paddingTop: 20 }]}>
			<View
				style={{
					flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
					marginHorizontal: 13, paddingRight: 10
				}}>
				<Text style={{ fontFamily: P800, fontSize: 18, color: colors.OnBackground }}>{Container_title}</Text>
			</View>
			<FlatList
				data={chefflist}
				// ref={scrollref}
				// keyExtractor={(item,index)=> index}
				style={styles.top_cuisines_container}
				horizontal={true}
				// initialNumToRender={4}
				initialScrollIndex={chefflist.length / 2}
				renderItem={({ item, index }) => {
					return (
						<Pressable onPress={() => {
							props.navigation.navigate('UserCheffProfileScreen', {
								cheffID: 1,
								cheffImage: item.cheffImagePath,
								cheffName: item.cheffName
							})
						}}
							style={[styles.cheff_card, { backgroundColor: colors.Surface, marginTop: 10, borderRadius: 20, overflow: 'hidden' }]}>
							<View
								style={{ width: '100%', position: 'relative' }}>
								<View style={{ width: '100%', height: 150, }}>
									<ImageBackground
										source={{ uri: item.cheffImagePath }}
										style={[{ position: 'relative', height: 150, width: Dimensions.get('window').width / 2.4, justifyContent: 'flex-end' }]} >
									</ImageBackground>
								</View>
								<View
									style={{ position: 'absolute', flexDirection: 'row', top: 10, left: 10 }}>
									<Image style={{ width: 25, height: 25, borderRadius: 15 }} source={{ uri: 'https://foodondoor.in/assets/user/img/veg.jpg' }} />

									<Image style={{ width: 25, height: 25, marginLeft: 4, borderRadius: 15 }} source={{ uri: 'https://pngimage.net/wp-content/uploads/2018/06/non-veg-symbol-png-3.jpg' }} />
								</View>
								{index % 2 == 0 ? <View
									style={{ position: 'absolute', flexDirection: 'row', bottom: 10, right: 0, backgroundColor: 'rgba(250,250,250,.8)', borderTopLeftRadius: 10, borderBottomLeftRadius: 10, paddingHorizontal: 10 }}>

									<LottieView source={CHEFFOFFER} autoPlay={true} style={{ height: 30, }} />
								</View> : []}

							</View>
							<View
								style={{
									width: '100%',
									height: 'auto',
									justifyContent: 'space-between',
									paddingVertical: 15,
									paddingHorizontal: 15,
									backgroundColor: colors.Surface,
									borderRadius: 12
								}}>
								<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
									<Text style={{ fontSize: 12, fontFamily: GS.P500, color: colors.OnSurface, lineHeight: 22, width: '70%' }}>{item.cheffName}</Text>
									<View
										style={{ backgroundColor: 'green', flexDirection: 'row', borderRadius: 6, justifyContent: 'center', paddingHorizontal: 5, paddingVertical: 3, alignItems: 'center' }}>

										<Text style={{ fontSize: 11, fontFamily: font.BodyFont.Small.fontFamily, color: 'white' }}>{item.chefRating}</Text>
										<Ionicons name="ios-star" size={12} color={'white'} style={{ marginLeft: 5 }} />
									</View>
								</View>
								<View
									style={{
										flexDirection: 'row',
										justifyContent: 'space-between',
										alignItems: 'center',
										width: '100%',
										marginTop: 5
									}} >
									<View style={{ flexDirection: 'row' }}>
										<View
											style={{
												// marginHorizontal: 5,
												borderRadius: 6,
												paddingVertical: 2,
												paddingHorizontal: 5,
												borderColor: colors.Secondry,
												borderWidth: 1,
												borderStyle: 'solid'
											}}>
											<Text style={{ fontSize: 10, fontFamily: GS.P400, color: colors.OnSurface }}>Dinner</Text>
										</View>
										<View
											style={{
												marginLeft: 5,
												borderRadius: 6,
												paddingVertical: 2,
												paddingHorizontal: 5,
												borderColor: colors.Secondry,
												borderWidth: 1,
												borderStyle: 'solid'
											}}>
											<Text style={{ fontSize: 10, fontFamily: GS.P400, color: colors.OnSurface }}>Lunch</Text>
										</View>
									</View>

								</View>

							</View>
							<View
								style={{ width: '100%', backgroundColor: 'rgba(245,255,245,1)', borderBottomLeftRadius: 30, borderBottomRightRadius: 30, paddingVertical: 8, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

								<LableMedium text={'₹340 for 3 Days'} clr={'rgba(0,180,0,1)'} align={'center'} />
							</View>

						</Pressable>
					)
				}}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	listing_container: {
		flex: 1,
	},
	top_cuisines_container: {
		marginVertical: 15,
		// height: 450,
		borderRadius: 10,
	},
	cheff_card: {
		justifyContent: 'space-between',
		// alignItems: 'flex-start',
		borderRadius: 10,
		marginHorizontal: 6,
		marginBottom: 10,
		// height: Dimensions.get('window').height / 2,
		elevation: 2,
		width: Dimensions.get('window').width / 2.4,

	}
})


import HeaderIconVarient from './Varients/headerHaveIcon'
import BackgroundHaveSliderVarient from './Varients/backgroundHaveSlider'
import HorizontalOneTimeTiffin from './Varients/oneTimeTiffins'

export function HorizontalCheffHeaderIconVarient({ props }) {
	return (
		<HeaderIconVarient props={props.props} title={props.title} iconimage={props.iconimage} />
	)
}

export function HorizontalCheffBackgroundHaveSliderVarient({ props }) {
	return (
		<BackgroundHaveSliderVarient props={props.props} title={props.title} />
	)
}

export function HorizontalOneTimeTiffinVarient({ props }) {
	return (
		<HorizontalOneTimeTiffin props={props.props} title={props.title} />
	)
}