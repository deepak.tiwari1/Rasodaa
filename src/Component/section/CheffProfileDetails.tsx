import { View, Text, StyleSheet, Dimensions } from 'react-native';
import React from 'react';
import { P400, P600 } from '../../Management/Utility/GlobalStyles';
import { UserCheffProfileScreenProps } from '../../Navigation/Screens/UserScreensPropTypes';
import { useSelector } from 'react-redux';
import { RootState } from '../../Management';

export default function cheffDetails_Comp({ navigation, route }: UserCheffProfileScreenProps) {
	const { colors } = useSelector((state: RootState) => state.appapperiencemanager);

	return (
		<View style={[styles.cheffDetail, { backgroundColor: colors.Background }]}>
			<View style={[styles.cheffexperience_wrapper]}>
				<View style={[styles.cheffexperience, { backgroundColor: colors.Surface, borderColor: colors.Outline }]}>
					<Text style={[styles.cheffexperience_text_1, { color: colors.OnSurface }]}>Tiffins Prepared</Text>
					<Text style={[styles.cheffexperience_text_2, { color: colors.OnSurface }]}>180</Text>
				</View>
				<View style={[styles.cheffexperience, { backgroundColor: colors.Surface, borderColor: colors.Outline }]}>
					<Text style={[styles.cheffexperience_text_1, { color: colors.OnSurface }]}>Total Year</Text>
					<Text style={[styles.cheffexperience_text_2, { color: colors.OnSurface }]}>0.3</Text>
				</View>
				<View style={[styles.cheffexperience, { backgroundColor: colors.Surface, borderColor: colors.Outline }]}>
					<Text style={[styles.cheffexperience_text_1, { color: colors.OnSurface }]}>Customers</Text>
					<Text style={[styles.cheffexperience_text_2, { color: colors.OnSurface }]}>12</Text>
				</View>
			</View>
			<View>

			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	cheffDetail: {
		paddingHorizontal: 20,
		paddingVertical: 20
	},
	cheffDetail_text: {
		//fontFamily: P400,
		color: 'rgba(51,51,51,1)',
	},
	cheffexperience_wrapper: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginVertical: 20,
	},
	cheffexperience: {
		alignItems: 'center',
		backgroundColor: 'white',
		borderRadius: 8,
		paddingVertical: 8,
		paddingHorizontal: 10,
		width: Dimensions.get('window').width / 3.4,
		borderWidth: 1,
		borderStyle: 'solid',

	},
	cheffexperience_text_1: {
		fontFamily: P400,
		color: 'rgba(51,51,51,.7)',
		fontSize: 12
	},
	cheffexperience_text_2: {
		fontFamily: P600,
		color: 'rgba(51,51,51,1)',
		fontSize: 18
	}
})
