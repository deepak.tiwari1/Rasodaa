import { View, ScrollView, Text, Dimensions, StyleSheet, Pressable, ImageBackground } from 'react-native'
import React from 'react'
import * as GS from '../../Management/Utility/GlobalStyles'
import { UserCheffProfileScreenProps } from '../../Navigation/Screens/UserScreensPropTypes'
import TiffinCard from '../cards/SchedualTiffinCard'
import { useSelector } from 'react-redux'
import { RootState } from '../../Management'
import { TitleLarge } from '../Utility/TextComponent'
import FoodCuisines from '../../Component/cards/TiffinCuisinesCard'

export default function OnDemandtiffins_Comp({ navigation, route }: UserCheffProfileScreenProps) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const OnDemandList = [
		{
			addonImagePath: 'https://images.news18.com/ibnkhabar/uploads/2021/07/masala-papad.jpg',
			addonName: 'Papad',
		},
		{
			addonImagePath: 'https://www.acouplecooks.com/wp-content/uploads/2019/05/Chopped-Salad-001_1.jpg',
			addonName: 'Salad',
		},
		{
			addonImagePath: 'https://upload.wikimedia.org/wikipedia/commons/6/67/Mango-achar.jpg',
			addonName: 'Achar',
		},
		{
			addonImagePath: 'https://media-cdn.tripadvisor.com/media/photo-s/13/9f/bc/f6/masala-chach-or-butter.jpg',
			addonName: 'Chach',

		},
		{
			addonImagePath: 'https://images.immediate.co.uk/production/volatile/sites/30/2020/02/Glass-and-bottle-of-milk-fe0997a.jpg?quality=90&resize=960,872',
			addonName: 'Butter milk',
		},
	];

	return (
		<View style={{
			paddingLeft: 20,
			paddingVertical: 20,
			marginVertical: 5,
			backgroundColor: colors.Background,
			borderStyle: 'solid',
			borderBottomWidth: 1,
			borderBottomColor: 'rgba(51,51,51,.2)',
			borderTopWidth: 1,
			borderTopColor: 'rgba(51,51,51,.2)'

		}}>
			<View
				style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginHorizontal: 5, }}>
				<Text style={{ fontFamily: GS.P800, fontSize: 18, color: colors.OnBackground }}>Addons items</Text>
			</View>
			<ScrollView
				style={styles.top_cuisines_container} horizontal={true} showsHorizontalScrollIndicator={false} pagingEnabled={true}>

				{OnDemandList.map((item, index) => {
					return (
						<View key={index}>
							<ImageBackground
								source={{ uri: item.addonImagePath }}
								style={[styles.food_cuisines_item_container, { borderColor: colors.Outline },
								{ backgroundColor: 'white' }]} >
								<View
									style={[{ width: '100%', height: '100%', justifyContent: 'flex-end', alignItems: 'center', paddingVertical: 10 }]}>
								</View>
							</ImageBackground>
							<Text style={{ color: colors.OnBackground, alignSelf: 'center', fontFamily: font.LableFont.Large.fontFamily }}>{item.addonName}</Text>
						</View>
					)
				})}

			</ScrollView>
		</View>
	)
}


const styles = StyleSheet.create({
	top_cuisines_container: {
		marginTop: 15,
		// marginBottom: 30,
		// paddingLeft: 10

	},
	food_cuisines_item_container: {
		justifyContent: 'space-between',
		alignItems: 'center',
		marginHorizontal: 5,
		height: 150,
		width: Dimensions.get('window').width / 3.8,
		borderRadius: 10,
		overflow: 'hidden',
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: 0.5,
		elevation: 2,
		marginBottom: 10,
		// shadowColor:'black',
		// backgroundColor:'#fff',
		// borderStyle:'solid',
		// borderWidth:1,
	}
}
)
