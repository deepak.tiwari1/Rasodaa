import { View, Text, ScrollView, StyleSheet, Image, Pressable, Dimensions, Check, TouchableOpacity } from 'react-native';
import React, { useState, useRef } from 'react'
import * as GS from '../../Management/Utility/GlobalStyles'
import { useTheme } from '@react-navigation/native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { UserCheffProfileScreenProps } from '../../Navigation/Screens/UserScreensPropTypes';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../Management';
import { TitleLarge } from '../Utility/TextComponent';
import { saveGuestMode_TO_LOCALDB } from '../../Management/App/appNavigationManager';
import RBSheet from 'react-native-raw-bottom-sheet'
import BouncyCheckbox from "react-native-bouncy-checkbox";

export default function TiffinSubsPlans_Comp({ navigation, route }: UserCheffProfileScreenProps) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const navigationMode = useSelector((state: RootState) => state.navigationmanager);
	let dispatch = useDispatch();
	const cartBSheet = useRef();
	let [plans, setplans] = useState([
		{
			basic_plan: [{
				plan_name: 'Basic plan',
				is_plan_selected: false,
				plan_duration: '3 Days',
				plan_cost: '₹600',
				plan_discount: '20%',
				plan_discounted_cost: '₹420',
			},
			{
				plan_name: 'Basic plan',
				is_plan_selected: false,
				plan_duration: '7 Days',
				plan_cost: '₹1400',
				plan_discount: '20%',
				plan_discounted_cost: '₹1120',
			},
			{
				plan_name: 'Basic plan',
				is_plan_selected: false,
				plan_duration: '15 Days',
				plan_cost: '₹3000',
				plan_discount: '20%',
				plan_discounted_cost: '₹2400',
			},
			{
				plan_name: 'Basic plan',
				is_plan_selected: false,
				plan_duration: '30 Days',
				plan_cost: '₹6000',
				plan_discount: '20%',
				plan_discounted_cost: '₹4800',
			}]
		},
		{
			gold_plan: [{
				plan_name: 'Gold plan',
				is_plan_selected: false,
				plan_duration: '3 Days',
				plan_cost: '₹800',
				plan_discount: '20%',
				plan_discounted_cost: '₹640',
			},
			{
				plan_name: 'Gold plan',
				is_plan_selected: false,
				plan_duration: '7 Days',
				plan_cost: '₹1800',
				plan_discount: '20%',
				plan_discounted_cost: '₹1440',
			},
			{
				plan_name: 'Gold plan',
				is_plan_selected: false,
				plan_duration: '15 Days',
				plan_cost: '₹3300',
				plan_discount: '20%',
				plan_discounted_cost: '₹2740',
			},
			{
				plan_name: 'Gold plan',
				is_plan_selected: false,
				plan_duration: '30 Days',
				plan_cost: '₹6600',
				plan_discount: '20%',
				plan_discounted_cost: '₹5480',
			}]
		},
		{
			premium_plan: [{
				plan_name: 'Premium plan',
				is_plan_selected: true,
				plan_duration: '3 Days',
				plan_cost: '₹1000',
				plan_discount: '20%',
				plan_discounted_cost: '₹800',
			},
			{
				plan_name: 'Premium plan',
				is_plan_selected: false,
				plan_duration: '7 Days',
				plan_cost: '₹2200',
				plan_discount: '20%',
				plan_discounted_cost: '₹1780',
			},
			{
				plan_name: 'Premium plan',
				is_plan_selected: false,
				plan_duration: '15 Days',
				plan_cost: '₹3600',
				plan_discount: '20%',
				plan_discounted_cost: '₹2840',
			},
			{
				plan_name: 'Premium plan',
				is_plan_selected: false,
				plan_duration: '30 Days',
				plan_cost: '₹7200',
				plan_discount: '20%',
				plan_discounted_cost: '₹5760',
			}]
		}
	])

	let [selected_plan, setselected_plan] = useState({
		plan_name: 'Premium plan',
		is_plan_selected: true,
		plan_duration: '3 Days',
		plan_cost: '₹1000',
		plan_discount: '20%',
		plan_discounted_cost: '₹800',
	})

	return (
		<View style={[{
			marginVertical: 20,
			paddingVertical: 10,
			backgroundColor: colors.Background,
			// borderBottomColor: 'rgba(51,51,51,.2)',
			// borderStyle: 'solid',
			// borderBottomWidth: 1,
		}]}>
			<View
				style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 17, justifyContent: 'flex-start', marginHorizontal: 5, }}>
				<Text style={{ fontFamily: GS.P800, fontSize: 18, color: colors.OnBackground }}>Chef packages</Text>
			</View>
			<ScrollView horizontal={true} showsHorizontalScrollIndicator={false} snapToAlignment='start' style={{ backgroundColor: colors.Background }}>
				<View style={[{ paddingHorizontal: 17, marginBottom: 10, width: Dimensions.get('window').width / 1, backgroundColor: colors.Background }]}>
					<View style={{ marginTop: 10 }}>
						<View style={[styles.plans_wrapper, { backgroundColor: colors.Background, shadowColor: (colors.modeCode == 'light' ? 'rgba(51,51,51,51,.5)' : 'rgba(243,243,243,.5)') }]}>
							<Text style={{ color: colors.OnBackground, fontWeight: '800', fontSize: 16 }}>Premium Plan</Text>
							<View style={{ marginVertical: 5, paddingVertical: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
								<View>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>All Includes in Cheff Schedual</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Gulam jamun</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Glass fress lassi</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Masala papad</Text>
								</View>
								<View>
									<Image source={{ uri: 'https://cdn-icons-png.flaticon.com/512/1478/1478930.png' }} style={{ width: 100, height: 100 }} />
								</View>
							</View>
							{plans[2].premium_plan?.map((plan, index) => {
								return (
									<Pressable key={index}
										style={[styles.plan, (plan.is_plan_selected ? { borderColor: colors.Primary } : { borderColor: 'white' }), { backgroundColor: colors.Background }]}
										onPress={() => {
											setselected_plan(plan)
											setplans(plans.map((item, itemindex) => {
												if (itemindex == 2) {
													for (let x = 0; x < item.premium_plan?.length; x++) {
														if (x == index) {
															item.premium_plan[x].is_plan_selected = true
														}
														else {
															item.premium_plan[x].is_plan_selected = false
														}
													}
													return item
												}
												else {
													for (let y = 0; y < item.gold_plan?.length; y++) {
														item.gold_plan[y].is_plan_selected = false
													}
													for (let z = 0; z < item.basic_plan?.length; z++) {
														item.basic_plan[z].is_plan_selected = false
													}
													return item
												}
											}))

										}}>
										<Text style={[styles.plan_text, { color: colors.OnSurface }]}>{plan.plan_duration}</Text>
										<View style={[styles.plan_amount_wrapper]}>
											<View style={styles.plan_amount}>

												<Text style={[styles.plan_discount_cost_text, { color: colors.OnSurface }]}>{plan.plan_cost}</Text>
												<Text style={[styles.plan_amount_text, { color: colors.OnSurface }]}>{plan.plan_discounted_cost}</Text>

											</View>

											<Text style={[styles.plan_gst_text, { color: colors.OnSurface }]}>GST Included</Text>
										</View>
									</Pressable>

								)
							})}
							<Pressable onPress={() => {
								//console.log(navigationMode.isGuestMode)
								if (navigationMode.isGuestMode) {

									navigation.navigate('AppLoginScreen');
									dispatch(saveGuestMode_TO_LOCALDB(false));
								}
								else {
									cartBSheet.current?.open()
								}
							}}
								style={[styles.plan, { backgroundColor: colors.Primary, justifyContent: 'center', alignItems: 'center' }]}>
								<Text style={{ fontFamily: GS.P600, color: colors.OnPrimary, fontSize: 18, marginHorizontal: 5 }}>{navigationMode.isGuestMode ? 'Login Now' : 'Add to Cart'}</Text>
								<Ionicons name="arrow-forward-sharp" size={24} color={colors.OnPrimary} />
							</Pressable>
						</View>
					</View>
				</View>
				<View style={[{ paddingHorizontal: 17, marginBottom: 10, width: Dimensions.get('window').width / 1, backgroundColor: colors.Background }]}>
					<View style={{ marginTop: 10 }}>
						<View style={[styles.plans_wrapper, { backgroundColor: colors.Background, shadowColor: (colors.modeCode == 'light' ? 'rgba(51,51,51,51,.5)' : 'rgba(243,243,243,.5)') }]}>
							<Text style={{ color: colors.OnBackground, fontWeight: '800', fontSize: 16 }}>Gold Plan</Text>
							<View style={{ marginVertical: 5, paddingVertical: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
								<View>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>All Includes in Cheff Schedual</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Gulam jamun</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Glass fress lassi</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Masala papad</Text>
								</View>
								<View>
									<Image source={{ uri: 'https://www.jasaf.com/assets/images/medal.png' }} style={{ width: 100, height: 100 }} />
								</View>
							</View>
							{plans[1].gold_plan?.map((plan, index) => {
								return (
									<Pressable key={index}
										style={[styles.plan, (plan.is_plan_selected ? { borderColor: colors.Primary } : { borderColor: 'white' }), { backgroundColor: colors.Background }]}
										onPress={() => {

											setselected_plan(plan)
											setplans(plans.map((item, itemindex) => {
												if (itemindex == 1) {
													for (let x = 0; x < item.gold_plan?.length; x++) {
														if (x == index) {
															item.gold_plan[x].is_plan_selected = true
														}
														else {
															item.gold_plan[x].is_plan_selected = false
														}
													}
													return item
												}
												else {
													for (let y = 0; y < item.basic_plan?.length; y++) {
														item.basic_plan[y].is_plan_selected = false
													}
													for (let z = 0; z < item.premium_plan?.length; z++) {
														item.premium_plan[z].is_plan_selected = false
													}
													return item
												}
											}))
										}}>
										<Text style={[styles.plan_text, { color: colors.OnSurface }]}>{plan.plan_duration}</Text>
										<View style={[styles.plan_amount_wrapper]}>
											<View style={styles.plan_amount}>

												<Text style={[styles.plan_discount_cost_text, { color: colors.OnSurface }]}>{plan.plan_cost}</Text>
												<Text style={[styles.plan_amount_text, { color: colors.OnSurface }]}>{plan.plan_discounted_cost}</Text>

											</View>

											<Text style={[styles.plan_gst_text, { color: colors.OnSurface }]}>GST Included</Text>
										</View>
									</Pressable>
								)
							})}
							<Pressable onPress={() => {
								//console.log(navigationMode.isGuestMode)
								if (navigationMode.isGuestMode) {

									navigation.navigate('AppLoginScreen');
									dispatch(saveGuestMode_TO_LOCALDB(false));
								}
								else {
									cartBSheet.current?.open()
									// navigation.navigate('UserCartScreen', {
									// 	userID: 1,
									// 	plan: selected_plan,
									// 	cheff: {
									// 		cheffID: 1
									// 	}
									// })
								}
							}}
								style={[styles.plan, { backgroundColor: colors.Primary, justifyContent: 'center', alignItems: 'center' }]}>
								<Text style={{ fontFamily: GS.P600, color: colors.OnPrimary, fontSize: 18, marginHorizontal: 5 }}>{navigationMode.isGuestMode ? 'Login Now' : 'Add to Cart'}</Text>
								<Ionicons name="arrow-forward-sharp" size={24} color={colors.OnPrimary} />
							</Pressable>

						</View>
					</View>
				</View>
				<View style={[{ paddingHorizontal: 17, marginBottom: 10, width: Dimensions.get('window').width / 1, backgroundColor: colors.Background }]}>
					<View style={{ marginTop: 10 }}>
						<View style={[styles.plans_wrapper, { backgroundColor: colors.Background, shadowColor: (colors.modeCode == 'light' ? 'rgba(51,51,51,51,1)' : 'rgba(243,243,243,1)') }]}>
							<Text style={{ color: colors.OnBackground, fontWeight: '800', fontSize: 16 }}>Basic Plan</Text>
							<View style={{ marginVertical: 5, paddingVertical: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
								<View>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>All Includes in Cheff Schedual</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Gulam jamun</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Glass fress lassi</Text>
									<Text style={{ color: colors.OnBackground, borderBottomWidth: 1, borderBottomColor: colors.PrimaryContainer, borderStyle: 'solid', paddingVertical: 10, fontFamily: font.LableFont.Large.fontFamily }}>2 Masala papad</Text>
								</View>
								<View>
									<Image source={{ uri: 'https://5.imimg.com/data5/SELLER/Default/2021/6/LC/XK/ZS/115719409/basic-icon-png-500x500.png' }} style={{ width: 100, height: 100 }} />
								</View>
							</View>
							{plans[0].basic_plan?.map((plan, index) => {
								return (
									<Pressable key={index}
										style={[styles.plan, (plan.is_plan_selected ? { borderColor: colors.Primary } : { borderColor: 'white' }), { backgroundColor: colors.Background, }]}
										onPress={() => {

											setselected_plan(plan)
											setplans(plans.map((item, itemindex) => {
												if (itemindex == 0) {
													for (let x = 0; x < item.basic_plan?.length; x++) {
														if (x == index) {
															item.basic_plan[x].is_plan_selected = true
														}
														else {
															item.basic_plan[x].is_plan_selected = false
														}
													}

													return item
												}
												else {
													for (let y = 0; y < item.gold_plan?.length; y++) {
														item.gold_plan[y].is_plan_selected = false
													}
													for (let z = 0; z < item.premium_plan?.length; z++) {
														item.premium_plan[z].is_plan_selected = false
													}
													return item
												}
											}))

										}}>
										<Text style={[styles.plan_text, { color: colors.OnSurface }]}>{plan.plan_duration}</Text>
										<View style={[styles.plan_amount_wrapper]}>
											<View style={styles.plan_amount}>

												<Text style={[styles.plan_discount_cost_text, { color: colors.OnSurface }]}>{plan.plan_cost}</Text>
												<Text style={[styles.plan_amount_text, { color: colors.OnSurface }]}>{plan.plan_discounted_cost}</Text>

											</View>

											<Text style={[styles.plan_gst_text, { color: colors.OnSurface }]}>GST Included</Text>
										</View>
									</Pressable>
								)
							})}

							<Pressable onPress={() => {
								//console.log(navigationMode.isGuestMode)
								if (navigationMode.isGuestMode) {

									navigation.navigate('AppLoginScreen');
									dispatch(saveGuestMode_TO_LOCALDB(false));
								}
								else {
									cartBSheet.current?.open()
									// navigation.navigate('UserCartScreen', {
									// 	userID: 1,
									// 	plan: selected_plan,
									// 	cheff: {
									// 		cheffID: 1
									// 	}
									// })
								}
							}}
								style={[styles.plan, { backgroundColor: colors.Primary, justifyContent: 'center', alignItems: 'center' }]}>
								<Text style={{ fontFamily: GS.P600, color: colors.OnPrimary, fontSize: 18, marginHorizontal: 5 }}>{navigationMode.isGuestMode ? 'Login Now' : 'Add to Cart'}</Text>
								<Ionicons name="arrow-forward-sharp" size={24} color={colors.OnPrimary} />
							</Pressable>
						</View>
					</View>
				</View>

			</ScrollView >
			<RBSheet
				ref={cartBSheet}
				height={Dimensions.get('window').height / 1.4}
				customStyles={{
					container: {
						backgroundColor: colors.Background,
						paddingHorizontal: 20,
						paddingVertical: 20
					}
				}}
			>
				<View style={{ justifyContent: 'space-between', flex: 1 }}>
					<View>
						<View style={{ borderBottomColor: 'rgba(51,51,51,.2)', borderBottomWidth: 1, borderStyle: 'solid', paddingVertical: 10, marginVertical: 10 }}>
							<Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Large.fontSize, fontFamily: font.TitleFont.Large.fontFamily }}>{selected_plan.plan_name}</Text>
							<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
								<Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily }}>{selected_plan.plan_duration} plan</Text>
								<TouchableOpacity style={{ marginHorizontal: 10 }} onPress={() => { cartBSheet.current?.close() }}>
									<Text style={{ color: colors.Primary, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>change</Text>
								</TouchableOpacity>
							</View>
						</View>
						<View>
							<Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Add-Ons</Text>
							<Text style={{ color: colors.OnBackground, fontSize: font.BodyFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily }}>Addons duration is same as plan duration</Text>
							<View style={{ paddingVertical: 10, marginVertical: 10 }}>
								{['chach', 'papad', 'milk', 'salad', 'achar'].map((addon, index) => {
									return (
										<View key={index} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
											<Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily }}>{addon}</Text>
											<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
												<Text style={{ color: colors.OnBackground, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.TitleFont.Small.fontFamily, marginHorizontal: 10 }}>₹45</Text>
												<BouncyCheckbox
													size={25}
													fillColor={colors.Primary}
													unfillColor="#FFFFFF"
													disableText={true}
													iconStyle={{ borderColor: colors.Secondry }}
													textStyle={{ fontFamily: font.HeadlineFont.Large.fontFamily }}
													onPress={(isChecked: boolean) => { }}
												/>
											</View>
										</View>
									)
								})}
							</View>
						</View>

					</View>
					<View>
						<TouchableOpacity
							onPress={() => {
								navigation.navigate('UserCartScreen');
								cartBSheet.current?.close()
							}}
							style={{ backgroundColor: colors.Primary, paddingVertical: 10, width: '100%', borderRadius: 10, alignItems: 'center' }}>
							<Text style={{ color: colors.OnPrimary, fontSize: font.TitleFont.Medium.fontSize, fontFamily: font.TitleFont.Medium.fontFamily }}>Add addon to cart</Text>
						</TouchableOpacity>
					</View>
				</View>
			</RBSheet>

		</View >
	)
}

const styles = StyleSheet.create({
	plans_wrapper: {
		flex: 1,
		borderRadius: 10,
		paddingVertical: 18,
		paddingHorizontal: 15,
		backgroundColor: 'white',
		elevation: 2,
		// borderStyle: 'solid',
		// borderWidth: 1,


	},
	plan: {
		height: 60,
		width: '100%',
		borderRadius: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: 20,
		paddingVertical: 10,
		marginVertical: 10,
		borderStyle: 'solid',
		borderWidth: 1,
		elevation: 1,
	},
	plan_text: {
		fontFamily: GS.P600,
		fontSize: 18
	},
	plan_amount_wrapper: {
		alignItems: 'flex-end'
	},
	plan_amount: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	plan_discount_text: {
		fontFamily: GS.P500,
		fontSize: 15,
	},
	plan_discount_cost_text: {
		fontFamily: GS.P500,
		fontSize: 15,
		textDecorationLine: 'line-through',
		color: 'rgba(51,51,51,.7)'
	},
	plan_amount_text: {
		fontFamily: GS.P600,
		fontSize: 18,
		marginHorizontal: 7,
	},
	plan_gst_text: {
		fontFamily: GS.P400,
		fontSize: 10
	}

})
