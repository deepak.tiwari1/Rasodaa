import { View, Text, ScrollView, StyleSheet, Pressable } from 'react-native';
import React, { useState } from 'react'
import PresentdaySchedual from '../Timeline/PresentdaySchedual'
import WeeklySchedual from '../Timeline/WeeklySchedual'
import * as GS from '../../Management/Utility/GlobalStyles'
import { useTheme } from '@react-navigation/native';
import { UserCheffProfileScreenProps } from '../../Navigation/Screens/UserScreensPropTypes';
import { useSelector } from 'react-redux';
import { RootState } from '../../Management';
export type TiffineSheduleDataType = {

}

export default function TiffineShedule_Comp({ navigation, route }: UserCheffProfileScreenProps) {
	const timeline = [2, 3, 4, 5, 6, 7]
	const [isWeeklySchedual, setisWeeklySchedual] = useState(false)
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);

	const tiffins = {
		lunch: [
			{
				tiffinImagePath: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg',
				tiffinName: 'Dal Bati Churma',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Lunch',
				tiffinDetails: ['150ml Daal', '4 Pcs Ghee Bati', '2 Churma Ladu', 'Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,

			},
			{
				tiffinImagePath: 'https://www.geetakiduniya.com/wp-content/uploads/2021/02/aloo-gobi.jpg',
				tiffinName: 'Gobhi Aloo or Roti',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Lunch',
				tiffinDetails: ['150ml Gobhi Sabji', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://www.dinedelicious.in/wp-content/uploads/2019/07/bhindi-ki-sabzi-e1587646044790.jpg',
				tiffinName: 'Bhindi Ki Sabji Roti',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Lunch',
				tiffinDetails: ['150ml Bhindi Sabji', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://s3.envato.com/files/289816475/DSC_3457.jpg',
				tiffinName: 'Dudh Sev or Roti',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Lunch',
				tiffinDetails: ['150ml Dush Sev Sabji ', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://allwaysdelicious.com/wp-content/uploads/2019/10/palak-paneer-oh-sq-scaled.jpg',
				tiffinName: 'Palak Panner',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Lunch',
				tiffinDetails: ['150ml Palak Panner', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://vismaifood.com/storage/app/uploads/public/1b8/afa/a77/thumb__700_0_0_0_auto.jpg',
				tiffinName: 'Matter Aloo ki sabji',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Lunch',
				tiffinDetails: ['150ml Matter Aloo', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},

		],

		dinner: [
			{
				tiffinImagePath: 'https://www.archanaskitchen.com/images/archanaskitchen/0-Archanas-Kitchen-Recipes/Articles/Punjabi_Toor_Dal_Tadka_Recipe_Video_Tuvar_dal_recipe_2.jpg',
				tiffinName: 'Toor ki Dal or Roti',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Dinner',
				tiffinDetails: ['150ml Toor Daal', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://www.cubesnjuliennes.com/wp-content/uploads/2020/09/Instant-Pot-Whole-Masoor-Dal.jpg',
				tiffinName: 'Masoor ki Dal',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Dinner',
				tiffinDetails: ['150ml Masoor Daal', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://www.sailusfood.com/wp-content/uploads/2016/01/urad-dal-recipe.jpg',
				tiffinName: 'Urad ki Dal or Roti',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Dinner',
				tiffinDetails: ['150ml Urad Daal', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://1.bp.blogspot.com/-eTCh-J-J0ic/XwPm0tgt9fI/AAAAAAAAM8s/oT21wsxURYUacwbROrAaxnHcmdpHiQU1ACK4BGAsYHg/s3456/IMG_3818.JPG',
				tiffinName: 'Simla Mirch ki Sabji',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Dinner',
				tiffinDetails: ['150ml Sabji', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://www.teaforturmeric.com/wp-content/uploads/2021/10/Aloo-Baingan-Eggplant-and-Potato-Curry-8.jpg',
				tiffinName: 'Baigan ki sabji ',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Dinner',
				tiffinDetails: ['150ml Baigan Sabji', '6 Pcs Ghee Roti', '2 Churma Roti', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},
			{
				tiffinImagePath: 'https://www.chefatlarge.in/wp-content/uploads/2018/08/40209999_881656468707482_5983292221066051584_n.jpg',
				tiffinName: 'Besan ki Sabji',
				tiffinCheffName: 'Cheff. Sanjiv Kumar',
				tiffinRating: 5,
				tiffinType: 'Dinner',
				tiffinDetails: ['150ml Besan', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
				tiffinSeats: [true, true, true, false, false],
				tiffinNotAvailableNo: 3,
			},

		]
	}
	return (
		<View
			style={[styles.container, { backgroundColor: colors.Surface }]}>
			<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%', marginBottom: 10 }}>
				<Text style={{ fontFamily: GS.P800, fontSize: 18, color: colors.OnBackground }}>Chef Schedual</Text>
				<Pressable onPress={() => { setisWeeklySchedual(!isWeeklySchedual) }}>
					<Text style={{ fontFamily: font.LableFont.Medium.fontFamily, color: colors.Primary, fontSize: 12 }}>View 7 days</Text>
				</Pressable>
			</View>
			<View style={{ marginTop: 15, marginHorizontal: 10 }}>
				<View>
					<PresentdaySchedual props={{ dateoftime: 1, lunch: tiffins.lunch[0], dinner: tiffins.dinner[0] }} />
				</View>
				<View>
					{isWeeklySchedual && (

						<View style={{
							borderTopWidth: 1,
							borderTopColor: 'rgba(51,51,51,.2)',
							borderStyle: 'solid',
							paddingVertical: 20,
						}}>

							{timeline.map((timeline_item, index) => {
								return (
									<View key={index} style={{ marginBottom: 15 }}>

										<WeeklySchedual
											cardhavefunction={false}
											props={{ dateoftime: timeline_item, lunch: tiffins.lunch[index], dinner: tiffins.dinner[index] }} />
									</View>
								)
							})}
						</View>
					)}

				</View>
			</View>
		</View>
	)
}
const styles = StyleSheet.create({
	container: {
		// flex: 1,

		paddingHorizontal: 20,
	}
})
