import { View, Text, StyleSheet, Dimensions, Image, TextInput, Pressable, ScrollView, TouchableOpacity } from 'react-native'
import React, { useRef } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import * as GS from '../../Management/Utility/GlobalStyles'
import { UserCheffProfileScreenProps } from '../../Navigation/Screens/UserScreensPropTypes';
import { useSelector } from 'react-redux';
import { RootState } from '../../Management';
import RBSheet from 'react-native-raw-bottom-sheet'
import { SLIDER_CHEF } from '../../Constant';

export default function ReviewsTab_Comp({ navigation, route }: UserCheffProfileScreenProps) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const { userprofilePic } = useSelector((state: RootState) => state.userprofiledata)
	const reviewBSheet = useRef();
	// console.log(userprofilePic)
	return (
		<View style={[styles.reviewRatingWrapper, { backgroundColor: colors.Background }]}>
			{/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginHorizontal: 5, marginBottom: 5 }}>
				<Text style={{ fontFamily: GS.P800, fontSize: 18, color: colors.OnBackground }}>Reviews and Ratings</Text>
			</View> */}
			<View style={styles.rating}>
				<View style={styles.rating_header}>
					<View style={[styles.header_details, { backgroundColor: colors.Surface }]}>
						<View style={styles.header_rating}>
							{[1, 2, 3, 4, 5].map((rating, index) => {
								return (
									<AntDesign key={index} name="star" size={24} color="rgba(255,204,72,1)" />
								)
							})}
						</View>
						<Text style={[styles.header_rating_text, { color: colors.OnSurface }]}>4.7 out of 5</Text>
					</View>
					<Text style={[styles.header_customer_text, { color: colors.OnBackground }]}>40 customers rating</Text>
				</View>
				<View style={styles.rating_bar_wrapper}>
					{['60%', '16%', '8%', '8%', '8%'].map((ratingdata, index) => {
						return (
							<View key={index} style={styles.rating_bar}>
								<Text style={[styles.bar_text, { color: colors.OnBackground }]}>{5 - index} star</Text>
								<View style={[styles.bar_background]}>
									<View style={[styles.bar_foreground, { width: ratingdata }]}></View>
								</View>
								<Text style={[styles.bar_percentage, { color: colors.OnBackground }]}>{ratingdata}</Text>
							</View>
						)
					})}
				</View>
			</View>
			<Pressable style={{ width: '100%', paddingHorizontal: 0, marginVertical: 20, borderBottomColor: colors.Outline, borderBottomWidth: 1, borderStyle: 'solid', paddingVertical: 10 }} onPress={() => { reviewBSheet.current?.open() }}>
				<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
					<Text style={{ color: colors.OnBackground }}>Review 5</Text>
					<MaterialCommunityIcons name="arrow-expand-up" color={colors.OnBackground} size={14} />
				</View>
				<View style={[styles.review_card, { backgroundColor: colors.SecondryContainer }]} >
					<View style={styles.card_header}>
						<Image style={styles.card_header_image} source={{ uri: 'https://img.freepik.com/free-photo/pleasant-looking-serious-man-stands-profile-has-confident-expression-wears-casual-white-t-shirt_273609-16959.jpg?w=360' }} />
						<View style={styles.card_user}>
							{/* <Text style={[styles.user_text, { color: colors.OnSurface }]}></Text> */}
							<Text style={[styles.footer_text, { color: colors.OnSurface }]}>Deepak Kumar Nice hand bag feels premium quality . Box are haveily tough and durable......</Text>
						</View>
					</View>
				</View>

			</Pressable>
			<RBSheet
				ref={reviewBSheet}
				height={Dimensions.get('window').height / 1.3}
			>
				<View style={styles.review}>
					<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
						<Text style={{ color: colors.OnBackground, fontFamily: font.TitleFont.Large.fontFamily, fontSize: 18, fontWeight: '800' }}>Reviews</Text>
						<Pressable onPress={() => { reviewBSheet.current?.close() }}>
							<AntDesign name="close" color={colors.OnBackground} size={24} />
						</Pressable>
					</View>
					<View style={{ flexDirection: 'row', alignItems: 'center', borderBottomColor: colors.Outline, borderStyle: 'solid', borderBottomWidth: 1 }}>
						<Image source={{ uri: SLIDER_CHEF + userprofilePic }} style={{ height: 30, width: 30, borderRadius: 30 }} />
						<TextInput
							placeholder='Add a review...'
							placeholderTextColor={'rgba(51,51,51,.5)'}
							style={{ width: '100%', color: colors.OnBackground, marginLeft: 5, fontSize: font.TitleFont.Small.fontSize, fontFamily: font.BodyFont.Small.fontFamily, fontWeight: '400' }}
						// onFocus={() => { reviewBSheet.current?.open() }}
						/>
					</View>
					<ScrollView style={{ marginBottom: 50 }}>
						<View >
							{[1, 2, 3, 4, 5].map((review, index) => {
								return (
									<View style={[styles.review_card, { backgroundColor: colors.Background, borderBottomColor: (colors.modeCode == 'light' ? 'rgba(51,51,51,.3)' : 'rgba(243,243,243,.3)'), borderStyle: 'solid', borderBottomWidth: 1 }]} key={index}>
										<View style={[styles.card_header]}>
											<Image style={styles.card_header_image} source={{ uri: 'https://img.freepik.com/free-photo/pleasant-looking-serious-man-stands-profile-has-confident-expression-wears-casual-white-t-shirt_273609-16959.jpg?w=360' }} />
											{/* <MaterialIcons style={styles.verified_image} name="verified-user" size={24} color="rgba(51,51,51,1)" /> */}
											<View style={styles.card_user}>
												<Text style={[styles.user_text, { color: colors.OnSurface }]}>Deepak Kumar</Text>
												<View>
													<Text style={{ fontFamily: font.LableFont.Small.fontFamily, color: colors.OnSurface, marginLeft: 8, fontSize: 12 }}>6 Days ago</Text>

												</View>

											</View>
										</View>
										<View style={[styles.card_footer]}>
											<Text style={[styles.footer_text, { color: colors.OnSurface }]}>Nice hand bag feels premium quality . Box are haveily tough and durable , air tight , leakprrof is awsome. bottle looks nice enough space in bag . another box also easy to fit inside . Level is nice and free mask One drawback ... color options not available . But black feels premium</Text>
										</View>
										<TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5, marginHorizontal: 4 }}>
											<AntDesign name="like2" color={colors.OnBackground} size={18} />
											<Text style={{ color: colors.OnBackground, marginLeft: 8 }}>10</Text>
										</TouchableOpacity>
									</View>
								)
							})}
						</View>
					</ScrollView>
				</View>

			</RBSheet >
		</View >
	)
}

const styles = StyleSheet.create({
	reviewRatingWrapper: {
		// borderTopColor: 'rgba(51,51,51,.2)',
		// borderStyle: 'solid',
		// borderTopWidth: 1,
		paddingHorizontal: 20,
		marginVertical: 20,
		// paddingVertical: 20

	},
	rating: {
		borderRadius: 8,
	},
	rating_header: {
		paddingVertical: 20,
		alignItems: 'center',
	},
	header_text: {
		fontFamily: GS.P600,
		fontSize: 24,
		color: 'rgba(51,51,51,1)',
	},
	header_details: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		backgroundColor: 'white',
		paddingVertical: 8,
		paddingHorizontal: 20,
		borderRadius: 10,
		width: Dimensions.get('window').width / 1.5,
		height: 50,
		elevation: 2,
		// borderWidth: 1,
		// borderStyle: 'solid',
	},
	header_rating: {
		flexDirection: 'row'
	},
	header_rating_text: {
		fontFamily: GS.P400,
	},
	header_customer_text: {

		fontFamily: GS.P400,
		color: 'rgba(51,51,51,1)',
		marginVertical: 10
	},
	rating_bar_wrapper: {
		alignItems: 'center',
	},
	rating_bar: {
		flexDirection: 'row',
		marginVertical: 2,

	},
	bar_text: {
		fontFamily: GS.P400
	},
	bar_background: {
		width: Dimensions.get('window').width / 1.7,
		height: 20,
		backgroundColor: 'white',
		borderRadius: 5,
		marginHorizontal: 10,
	},
	bar_foreground: {
		height: 20,
		backgroundColor: 'rgba(255,204,72,1)',
		borderRadius: 5
	},
	bar_percentage: {
		fontFamily: GS.P400
	},
	review: {
		paddingVertical: 20,
		paddingHorizontal: 20
	},
	review_card: {
		marginVertical: 10,
		borderRadius: 10,
		backgroundColor: 'white',
		// elevation: 2,
		paddingHorizontal: 20,
		paddingVertical: 20,
		// borderWidth: 1,
		// borderStyle: 'solid',
	},
	card_header: {
		flexDirection: 'row',
		// alignItems: 'center',
		justifyContent: 'flex-start'
	},
	card_header_image: {
		height: 25,
		width: 25,
		borderRadius: 15,
	},
	verified_image: {
		position: 'absolute',
		zIndex: 5,
		bottom: 0,
		left: 28,
	},
	card_user: {
		marginHorizontal: 10,
		flexDirection: 'row',
		alignItems: 'center',
	},
	user_text: {
		fontSize: 14,
		fontFamily: GS.P600,
	},
	user_rating: {
		flexDirection: 'row',
	},
	card_footer: {
		marginTop: 10,
	},
	footer_text: {
		fontSize: 14,
		fontFamily: GS.P400,
		marginLeft: 5
	}
})
