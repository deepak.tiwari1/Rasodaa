import { Dimensions, StyleSheet } from "react-native"
import { P300, P400, P500, P600, P800 } from "../../../Management/Utility/GlobalStyles"
import type { Colors, initialFontdata } from "../../../Management/App/appApperienceManager"


export const createStyle = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    main: {
        paddingVertical: 20,
        paddingHorizontal: 5,
        borderBottomColor: colors.BackgroundColorShades.BC_100,
        borderBottomWidth: 3,
        borderStyle: 'solid',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 13
    },
    headertext: {
        fontFamily: P800,
        fontSize: 17,
        color: colors.OnBackground,
        marginLeft: 20
    },
    // headerbutton: {
    //     paddingHorizontal: 5,
    //     paddingVertical: 5
    // },
    scrollview: {
        marginVertical: 5,
        borderRadius: 10,
    },
    scrollitem: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        borderRadius: 20,
        marginHorizontal: 6,
        marginBottom: 10,
        elevation: 2,
        backgroundColor: colors.Surface,
        marginTop: 10,
        overflow: 'hidden'
    },
    scrollitem_imageSection: {
        position: 'relative'
    },
    imageSection_carouselContainer: {
        width: '100%',
        height: 200,
    },
    caroiselPagination: {
        position: 'absolute',
        bottom: 0,
        right: 10
    },
    caroiselPagination_ActiveItem: {
        width: 16,
        height: 5,
        borderRadius: 2,
        backgroundColor: colors.OnPrimary
    },
    caroiselPagination_InactiveItem: {
        width: 8,
        height: 5,
        borderRadius: 2,
        backgroundColor: colors.OnPrimary
    },
    carouselImage: {
        position: 'relative',
        justifyContent: 'space-between',
        height: 200,
        width: Dimensions.get('window').width / 1,
    },
    imageSecton_foottypeContainer: {
        position: 'absolute',
        flexDirection: 'row',
        top: 10,
        left: 10
    },
    foodtypeImage1: {
        width: 25,
        height: 25,
        borderRadius: 15
    },
    foodtypeImage2: {
        width: 25,
        height: 25,
        marginLeft: 4,
        borderRadius: 15
    },
    scrollitem_detailsSection: {
        width: '100%',
        justifyContent: 'flex-start',
        paddingVertical: 18,
        paddingHorizontal: 15,
        backgroundColor: colors.Surface,
        borderRadius: 12
    },
    detailsSection1: {
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    detail1_LeftSection: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    cheffImage: {
        height: 45,
        width: 45,
        borderRadius: 22
    },
    cheffHeader: {
        height: 50,
        marginLeft: 10,
        justifyContent: 'center'
    },
    cheffHeading: {
        fontSize: 18,
        fontFamily: P500,
        color: colors.OnSurface,
        lineHeight: 22
    },
    cheffSubheading: {
        fontSize: 12,
        fontFamily: P300,
        color: colors.OnSurface
    },
    detail1_RightSection: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    cheffratingContainer: {
        backgroundColor: 'green',
        flexDirection: 'row',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        paddingHorizontal: 10,
        paddingVertical: 3,
        alignItems: 'center'
    },
    cheffrating_Icon: {
        marginRight: 5
    },
    detailsSection2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    detail2_LeftSection: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    availableSeatsImage: {
        height: 20,
        width: 20,
        borderRadius: 10,
        marginHorizontal: 3
    },
    detail2_RightSection: {
        flexDirection: 'row'
    },
    foodtimeContainer: {
        marginHorizontal: 2,
        borderRadius: 6,
        paddingVertical: 2,
        paddingHorizontal: 5,
        borderColor: colors.Secondry,
        borderWidth: 1,
        borderStyle: 'solid'
    },
    foodtimeText: {
        fontSize: 12,
        fontFamily: P400,
        color: colors.OnSurface
    },
    scrollitem_bottomSection: {
        width: '100%',
        backgroundColor: 'rgba(245,255,245,1)',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    offerIcon: {
        marginHorizontal: 5
    },
})