import { createStyle } from './styles'
import { useSelector } from 'react-redux'
import React, { useEffect, useState, useMemo } from 'react'
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { RootState } from '../../../Management'
import * as Server from '../../../Management/Utility/GlobalURLs'
import { View, Text, Pressable, ScrollView, StyleSheet, Image, ImageBackground } from 'react-native'
import { TitleLarge, TitleMedium, LableLarge, LableMedium, LableSmall, BodyLarge, BodySmall, BodyMedium } from '../../Utility/TextComponent';

export default function VerticalCheffSection({ props, title }) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const styles = useMemo(() => createStyle(colors, font), [colors, font])
	const [cheffs, Setcheffs] = useState([1]);
	const cheffList = [1, 2, 3, 4, 5]

	return (
		<View style={styles.main}>
			<Text style={styles.headertext}>{cheffList.length} {title}</Text>
			<ScrollView style={styles.scrollview}
				horizontal={false}
				showsHorizontalScrollIndicator={false}
				pagingEnabled={true}>
				{cheffList.map((item, index) => {
					return (
						<Pressable key={index} style={styles.scrollitem}
							onPress={() => {
								props.navigation.navigate('UserCheffProfileScreen', {
									cheffID: 1,
									// cheffImage: item.cheffImagePath,
									cheffName: 'Sanjiv Kapur'
								})
							}}>
							<View style={styles.scrollitem_imageSection}>
								<View style={styles.imageSection_carouselContainer}>
									<SwiperFlatList
										autoplay
										autoplayDelay={5}
										autoplayLoop
										index={0}
										showPagination={true}
										paginationStyle={styles.caroiselPagination}
										paginationStyleItemInactive={styles.caroiselPagination_InactiveItem}
										paginationStyleItemActive={styles.caroiselPagination_ActiveItem}
										data={tiffins}
										renderItem={({ item }) => (
											<ImageBackground style={styles.carouselImage}
												source={{ uri: item.tiffinImagePath }}></ImageBackground>
										)} />
								</View>
								<View style={styles.imageSecton_foottypeContainer}>
									<Image style={styles.foodtypeImage1} source={{ uri: vegsymbol }} />
									<Image style={styles.foodtypeImage2} source={{ uri: nonvegsymbol }} />
								</View>
							</View>
							<View style={styles.scrollitem_detailsSection}>
								<View style={styles.detailsSection1}>
									<View style={styles.detail1_LeftSection}>
										<Image style={styles.cheffImage} source={{ uri: cheffprofileimage }} />
										<View style={styles.cheffHeader}>
											<Text style={styles.cheffHeading}>Sanjiv Kapur</Text>
											<Text style={styles.cheffSubheading}>Ashok Vihar, Udaipur, Rajasthan</Text>
										</View>
									</View>
									<View style={styles.detail1_RightSection}>
										<View style={styles.cheffratingContainer}>
											<Ionicons name="ios-star" size={16} color={'white'} style={styles.cheffrating_Icon} />
											<LableLarge text={'5'} clr={'white'} align={'center'} />
										</View>
									</View>
								</View>
								<View style={styles.detailsSection2}>
									<View style={styles.detail2_LeftSection}>
										<Image style={styles.availableSeatsImage} source={{ uri: availableSeatImage[0] }} />
										<Image style={styles.availableSeatsImage} source={{ uri: availableSeatImage[1] }} />
										<Image style={styles.availableSeatsImage} source={{ uri: availableSeatImage[2] }} />
										<Image style={styles.availableSeatsImage} source={{ uri: availableSeatImage[3] }} />
										<Text> +2 more</Text>
									</View>
									<View style={styles.detail2_RightSection}>
										<View style={styles.foodtimeContainer}>
											<Text style={styles.foodtimeText}>Dinner</Text>
										</View>
										<View style={styles.foodtimeContainer}>
											<Text style={styles.foodtimeText}>Lunch</Text>
										</View>
									</View>
								</View>
							</View>
							<View style={styles.scrollitem_bottomSection}>
								<MaterialIcons name="local-offer" size={14} color="rgba(0,180,0,1)" style={styles.offerIcon} />
								<LableMedium text={'60% off on 30 days Package'} clr={'rgba(0,180,0,1)'} align={'center'} />
							</View>
						</Pressable>
					)
				})}

			</ScrollView>
		</View>
	)
}




const tiffins = [
	{
		tiffinImagePath: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg',
		tiffinName: 'Dal Bati Churma',
		tiffinCheffName: 'Cheff. Sanjiv Kumar',
		tiffindate: 'Today'
	},
	{
		tiffinImagePath: 'https://www.geetakiduniya.com/wp-content/uploads/2021/02/aloo-gobi.jpg',
		tiffinName: 'Gobhi Aloo or Roti',
		tiffinCheffName: 'Cheff. Sanjiv Kumar',
		tiffindate: 'Tommorow'
	},
	{
		tiffinImagePath: 'https://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Quick-Bhindi-Sabzi.jpg',
		tiffinName: 'Bhindi Ki Sabji Roti',
		tiffinCheffName: 'Cheff. Sanjiv Kumar',
		tiffindate: '1 June'
	},
	{
		tiffinImagePath: 'https://s3.envato.com/files/289816475/DSC_3457.jpg',
		tiffinName: 'Dudh Sev or Roti',
		tiffinCheffName: 'Cheff. Sanjiv Kumar',
		tiffindate: '2 June'
	}

]

const vegsymbol = 'https://foodondoor.in/assets/user/img/veg.jpg'
const nonvegsymbol = 'https://pngimage.net/wp-content/uploads/2018/06/non-veg-symbol-png-3.jpg'
const eggsymbol = ''

const cheffprofileimage = 'https://www.thespruceeats.com/thmb/qOvuUe2qIzHKxG_xqKXkoFewAaA=/2086x1173/smart/filters:no_upscale()/GettyImages-480379752-588cb5de3df78caebc869bcf.jpg'

const availableSeatImage = ['https://thumbs.dreamstime.com/b/profile-picture-smiling-indian-female-employee-profile-picture-smiling-millennial-indian-female-employee-posing-office-198022033.jpg', 'https://www.specialtyfood.com/media/uploads/articles/Chitra_s_Headshot_f.jpg.370x370_q85.jpg', 'https://www.veethi.com/images/people/profile/Tej_(south_Indian_Actor).png', 'https://thenewscrunch.com/wp-content/uploads/2020/04/Anirudha-Singh.jpg']