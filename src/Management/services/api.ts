import React, { Component } from 'react';



// const BASE_URL = "http://192.168.1.200/rasodaa/public/api/"
// const BASE_URL = "http://192.168.2.200/rasodaa/public/api/"
const BASE_URL = "http://admin.rasodaa.com/api/"

class Users extends Component {

  // static BASEURL_IMAGE = "http://192.168.2.200/rasodaa/public/storage/thumb/"
  static SLIDER_CHEF = "http://admin.rasodaa.com/storage/"
  // static SLIDER_CHEF = "http://192.168.1.200/rasodaa/public/storage/"
  // static SLIDER_CHEF = "http://192.168.2.200/rasodaa/public/storage/"
  // static BASEURL_IMAGE = "http://192.168.1.200/rasodaa/public/storage/thumb/"
  static BASEURL_IMAGE = "http://admin.rasodaa.com/storage/thumb/"

  constructor(props) {
    super(props);
    this.state = {
      token: ""
    }
  }
  Registration(object) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'register', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(object)
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            console.log('Success')
            resolve(response);
          }
          else {
            console.log('UnSuccess')
            reject(response)
          }
        })
        .catch(error =>
          reject(error)

        )
    })

  }

  Login(object) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'login', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(object)
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            return response
          }
          else {
            return response
          }
        })
        .catch(error => {
          return error
        }
        )
    })
  }

  changePassword(object, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/changepassword', {
        method: 'post',
        headers: {
          'Authorization': ' Bearer ' + token
        },
        body: object
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getCuisines() {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'cuisines?start=0&length=all&search=a', {
        headers: {
          'Content-Type': 'application/json; charset=utf-8',

        },
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          console.log(response)
          if (response.status == 200) {

            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>

          reject(error)
        )
    })

  }

  getDishes() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'dishes?start=0&length=all&search=a', {
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': ' Bearer ' + token
        },
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })



  }
  getUserProfile(token) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/profile', {
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': ' Bearer ' + token
        },
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })



  }

  getAuthData(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'getAuthUser', {
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': ' Bearer ' + token
        },
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  getCountries() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'getcountries?start=0&length=all&search=a', {
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getStates(id) {


    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'getstates/' + id + '?start=0&length=all&search=a', {
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }


  getCities(id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'getcities/' + id + '?start=0&length=all&search=a', {
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  updateProfile(object, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/profile', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-type': 'application/json; charset=utf-8'
        },
        body: object
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error => {

          reject(error)
        }
        )
    })

  }
  getLocalities(id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'getlocalities/' + id + '?start=0&length=all&search=a', {
        method: 'GET'
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            resolve(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getAllPackages(token, length) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'packages?start=0&length=2&search=a', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }

  getPackagesDetails(token, id) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'packages?chef_id=' + id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }



  updateProfilePicture(token, object) {


    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/updateprofilepic', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        body: object
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }

  addAddress(token, object) {


    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/address/add', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        body: object
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  updateAddress(token, object, id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/address/update/' + id, {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        body: object
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  getAllUserAddress(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/addresses', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  getAddressDetails(token, id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/address/' + id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  deleteItem(token, id) {

    let addId = id.toString()
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/address/delete/' + addId, {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>

          reject(error)
        )
    })
  }

  defaultAddress(token, id) {

    let addId = id.toString()
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/address/default/' + id, {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getAllRealtedPackages(token, id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chef/' + id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }
  getAllRelatedPackges(token, chefid) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'packages?chef_id=' + chefid, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getAllTipsTrick(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'tips?start=0&length=10&search=a', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getAllRatingReviews(token, chef_id) {
    //  alert(chef_id)
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chef-review/' + chef_id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  submitReviews(token, object) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/review/add', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        body: object
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getAllChefs() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs', {
        method: 'GET',
        headers: {
          // 'Authorization': 'Bearer '+ token,
        }
      }).then(res => res.json())
        .then((response) => {
          console.log(response)
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  subscribeChefs() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?sortBy=most_subscribed', {
        method: 'GET',
        headers: {
          // 'Authorization': 'Bearer '+ token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }
  TopRatedChef() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?sortBy=most_rated', {
        method: 'GET',
        headers: {
          // 'Authorization': 'Bearer '+ token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  FastFill() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?sortBy=filling_fast', {
        method: 'GET',
        headers: {
          // 'Authorization': 'Bearer '+ token,
        }
      }).then(res => res.json())
        .then((response) => {
          console.log(response)
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getSelctedChefs(selected) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?cuisines=' + selected, {
        method: 'GET',
        headers: {
          // 'Authorization': 'Bearer '+ token,
        }
      }).then(res => res.json())
        .then((response) => {
          console.log(response)
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }


  chefTypeMenu(chef_type) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?food_preference=' + chef_type, {
        method: 'GET',
        headers: {
          // 'Authorization': 'Bearer '+ token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }
  getAllCuisines(token) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'cuisines?start=0&length=all&search=a', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }
  applyCuisines(cuisines, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?cuisine=' + cuisines, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  applyFoodPreference(foodPrefernce, token) {
    let type;
    type = foodPrefernce;
    if (type == 'all') {
      type = ''
    }

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?food_preference=' + type, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  searchChefBytext(text, token) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?q=' + text, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }
  getChefDetails(uniqueId, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chef/' + uniqueId, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getChefDetailsvthToken(token, uniqueId) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chef/' + uniqueId, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getChefMenu(chefid) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'menus?chef_id=' + chefid, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getChefPackages(chefid) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'packages?chef_id=' + chefid, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  getChefoffers(chefid) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'offers?chef_id=' + chefid, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }

  getChefMenuDetails(slug) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'menu/' + slug, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getAllOffers() {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'offers?start=0&length=10&search=a', {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getAllOffersDetails(slug) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'offer/' + slug, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  searchPackageBytext(text, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'packages?q=' + text, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  applyPackagesFilter(search, foodPrefernce, type, cuisine, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'packages?q=' + search + '&type=' + type + '&food_preference=' + foodPrefernce +
        '&cuisine=' + cuisine + '&length=all' + '&start=0', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }
  applyallFilterChef(search, cuisine, foodPrefernce, token) {
    console.log(BASE_URL + 'chefs?q=' + search + '&food_preference=' + foodPrefernce + '&cuisines=' + cuisine + '&length=all' + '&start=0');
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'chefs?q=' + search + '&food_preference=' + foodPrefernce + '&cuisines=' + cuisine + '&length=all' + '&start=0', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  getAllChefoffer(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'offers?' + 'start=0' + '&length=all&search=a', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  ContactUs(data, token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'contactus', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        body: data
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  checkSchedules(type, order_Time) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'schedules?type=' + type + '&order_time=' + order_Time, {
        method: 'GET',
      }).then(res => res.json())
        .then((response) => {
          console.log(response);
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })




  }

  getAddOns(token, chef_id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'addons/' + chef_id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }
  getMenuByChefId(token, chef_id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'menus?chef_id=' + chef_id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,

        }
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  addToPackageCart(token, object) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/cart/add', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-type': 'application/json;  charset=utf-8'
        },
        body: object
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)

          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  updatePackageCart(token, object, cart_id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/cart/additem/' + cart_id, {
        method: 'post',
        headers: {
          'Authorization': ' Bearer ' + token,
          'Content-type': 'application/json;  charset=utf-8'
        },
        body: object
      }).then(res => res.json())

        .then((response) => {


          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)

          }
        })
        .catch(error =>
          reject(error)
        )
    })



  }
  availabity(package_type, chef_id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'package/availablity/' + chef_id + '?type=' + package_type, {
        method: 'GET',
        headers: {
          // 'Authorization': ' Bearer '+token,
          'Content-type': 'application/json; charset=utf-8'
        },
        // body: object
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  getCartList(token) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/carts', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  ClearCart(token) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/cart/clear', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  deleteCartItem(token, cartID) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/cart/delete/' + cartID, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {
          alert(JSON.stringify(response))
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  allcoupons(token, id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'offers', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  clearOffer(token, cartId) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/offer/clear/' + cartId, {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })



  }
  PlaceOrder(token, order_data) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/order/add', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-type': 'application/json; charset=utf-8'
        },
        body: order_data
      }).then(res => res.json())

        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>

          reject(error)
        )
    })

  }



  generateInvoice(token, object) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/invoice/save', {
        method: 'post',
        headers: {
          'Authorization': ' Bearer ' + token,
          'Content-type': 'application/json; charset=utf-8'
        },
        body: object
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  addPayment(token, object) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/payment/add', {
        method: 'post',
        headers: {
          'Authorization': ' Bearer ' + token,
          'Content-type': 'application/json; charset=utf-8'
        },
        body: object
      }).then(res => res.json())
        .then((response) => {
          alert(JSON.stringify(response))
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  updatePaymentStatus(token, orderid, object) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/order/update/' + orderid, {
        method: 'post',
        headers: {
          'Authorization': ' Bearer ' + token,
          'Content-type': 'application/json; charset=utf-8'
        },
        body: object
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }

  getAllOrder(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/orders', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,


        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }


  getOrderDetailsByid(token, order_id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/order/item/' + order_id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,


        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }


  getOrderStatusList(token, order_id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/orderlist/' + order_id, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,


        }
      }).then(res => res.json())

        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>

          reject(error)
        )
    })
  }
  getOrderInvoice(token, orderId) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/invoice/' + orderId, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getPaymentDetails(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/payments', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })

  }
  markChefAsFav(token, id) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/markfavourite/' + id, {
        method: 'post',
        headers: {
          'Authorization': ' Bearer ' + token,
          'Content-type': 'application/json; charset=utf-8'
        },
      }).then(res => res.json())
        .then((response) => {
          if (response.status == 200) {
            resolve(response);
          }
          else {
            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getAllfavourite(token) {

    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'customer/favourites', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })
  }

  getAllPackagesMenus(token, packageId) {
    return new Promise((resolve, reject) => {
      fetch(BASE_URL + 'menus?Package_id=' + packageId, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {
            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch(error =>
          reject(error)
        )
    })


  }

  SearchFilter = (text) => {

    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'searchall?q=' + text, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");

        })



    })
  }

  availableDates = (chef_id, package_type) => {
    // console.log(chef_id ,package_type)
    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'package/availablity/' + chef_id + '?type=' + package_type, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");

        })



    })
  }


  filteravailabledays = (skipdays, package_type, chef_id) => {



    return new Promise((resolve, reject) => {
      console.log(BASE_URL + 'package/availablity/' + package_type + '?type=' + chef_id + '&skip_days=' + skipdays)
      fetch(BASE_URL + 'package/availablity/' + package_type + '?type=' + chef_id + '&skip_days=' + skipdays, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {
          console.log(response)
          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })
  }



  getsliders = () => {



    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'getsliders/', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {
          console.log(response)
          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })
  }



  getMenus = () => {



    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'menus', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })
  }


  dinnerMenu = (day) => {

    if (day == undefined) {
      day = '';
    }
    // alert(day)
    if (day != null && day != '') {
      return new Promise((resolve, reject) => {

        fetch(BASE_URL + 'menus?start=0&length=3&menu_type=dinner&tomorrow=' + day, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': 'application/json; charset=utf-8'
          }

        }).then(res => res.json())
          .then((response) => {

            if (response.status == 200) {

              resolve(response);
            }
            else {

              reject(response)
            }
          })
          .catch((error) => {
            console.log("Api call error");
            alert(error.message);
          })



      })
    }
    else {

      return new Promise((resolve, reject) => {

        fetch(BASE_URL + 'menus?today=1&start=0&length=3&menu_type=dinner', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': 'application/json; charset=utf-8'
          }

        }).then(res => res.json())
          .then((response) => {

            if (response.status == 200) {

              resolve(response);
            }
            else {

              reject(response)
            }
          })
          .catch((error) => {
            console.log("Api call error");
            alert(error.message);
          })



      })
    }

  }

  lunchMenu = (day) => {

    if (day == undefined) {
      day = '';
    }

    if (day != null && day != '') {
      return new Promise((resolve, reject) => {

        fetch(BASE_URL + 'menus?&start=0&length=3&menu_type=lunch&tomorrow=' + day, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': 'application/json; charset=utf-8'
          }

        }).then(res => res.json())
          .then((response) => {

            if (response.status == 200) {

              resolve(response);
            }
            else {

              reject(response)
            }
          })
          .catch((error) => {
            console.log("Api call error");
            alert(error.message);
          })



      })
    }
    else {

      return new Promise((resolve, reject) => {

        fetch(BASE_URL + 'menus?today=1&start=0&length=3&menu_type=lunch', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': 'application/json; charset=utf-8'
          }

        }).then(res => res.json())
          .then((response) => {

            if (response.status == 200) {

              resolve(response);
            }
            else {

              reject(response)
            }
          })
          .catch((error) => {
            console.log("Api call error");
            alert(error.message);
          })



      })
    }

  }


  getChefTips = (chef_id) => {

    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'tips/chef_id=' + chef_id, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })




  }

  getskipFood = (customer_order_id, reason_data, token) => {


    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'customer/skipfood/' + customer_order_id, {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json; charset=utf-8',

        },
        body: JSON.stringify({ reason: reason_data })
      }).then(res => res.json())
        .then((response) => {
          alert(JSON.stringify(response))
          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })

  }

  getDonateFood = (customer_order_id, reason_data, token) => {

    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'customer/donatefood/' + customer_order_id, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({ reason: reason_data })
      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })

  }
  swapMenus = (menu_type, swap_prize) => {



    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'menus?today=1&menu_type=' + menu_type + '&swap=' + swap_prize, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Accept': 'application/json; charset=utf-8'
        }

      }).then(res => res.json())
        .then((response) => {

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })
  }



  swapNow = (token, customer_order_id, menu_id) => {


    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'customer/swapfood/' + customer_order_id, {
        method: 'POST',
        headers: {

          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({ menu_id: menu_id })

      }).then(res => res.json())
        .then((response) => {
          JSON.stringify(response)

          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })
  }

  UpdateSwapOrder = (token, payment_id, swap_id, order_id) => {


    return new Promise((resolve, reject) => {

      fetch(BASE_URL + 'customer/swapfood/update/' + swap_id, {
        method: 'POST',
        headers: {

          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({
          payment_id: payment_id,
          order_id: order_id
        })

      }).then(res => res.json())
        .then((response) => {


          if (response.status == 200) {

            resolve(response);
          }
          else {

            reject(response)
          }
        })
        .catch((error) => {
          console.log("Api call error");
          alert(error.message);
        })



    })
  }


}
export default Users; 