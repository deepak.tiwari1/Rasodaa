import React, { useState } from 'react';
//import * as SecureStore from 'expo-secure-store';
import { EncryptedStoreKeys } from '../LocalDBStoreKeys';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const saveGoogleAccessToken = async (value: string) => {
    try {
        await AsyncStorage.setItem(EncryptedStoreKeys.GoogleAccessToken, value);
    }
    catch (error) {
        console.log(error);
    }
};

export const saveRasodaaAccessToken = async (value: string) => {
    try {
        await AsyncStorage.setItem(EncryptedStoreKeys.RasodaaAccessToken, value);
    }
    catch (error) {
        console.log(error);
    }
};

export const saveFirebaseAccessToken = async (value: string) => {
    try {
        await AsyncStorage.setItem(EncryptedStoreKeys.FirebaseAccessToken, value);
    }
    catch (error) {
        console.log(error);
    }
};

export const getGoogleAccessToken = async (callback: (value: string | undefined) => void) => {
    try {
        const tokenFromlogin = await AsyncStorage.getItem(EncryptedStoreKeys.GoogleAccessToken)
        if (tokenFromlogin) {
            callback(tokenFromlogin);
        }
    }
    catch (error) {
        console.log(error);
    }
};

export const getRasodaaAccessToken = async (callback: (value: string | undefined) => void) => {
    try {
        const tokenFromlogin = await AsyncStorage.getItem(EncryptedStoreKeys.RasodaaAccessToken)
        if (tokenFromlogin) {
            callback(tokenFromlogin);
        }
    }
    catch (error) {
        console.log(error);
    }
};

export const getFirebaseAccessToken = async (callback: (value: string | undefined) => void) => {
    try {
        const tokenFromlogin = await AsyncStorage.getItem(EncryptedStoreKeys.FirebaseAccessToken)
        if (tokenFromlogin) {
            callback(tokenFromlogin);
        }
    }
    catch (error) {
        console.log(error);
    }
};

