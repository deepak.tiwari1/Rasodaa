
export type theme = {
    themeID: number;
    dark: boolean;
    colors: {
        primary: string;
        background: string;
        card: string;
        text: string;
        border: string;
        notification: string;
    }
}

export interface Cuisines {
    id: number;
    title: string;
    image: string;
    description: string;
}
export interface TiffinType {
    id: number,
    food_preference_type: string,
    tiffin_card_image: string,
    tiffin_background_image: string,
    description: string
}
export interface TiffinCuidines {
    id: number,
    title: string,
    tiffin_card_image: string,
    tiffin_background_image: string,
    description: string
}