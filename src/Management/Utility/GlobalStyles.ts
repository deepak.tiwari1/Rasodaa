import { StyleSheet } from 'react-native';
import React from 'react';
import styled from 'styled-components/native';
import { useFonts, Poppins_100Thin, Poppins_200ExtraLight, Poppins_300Light, Poppins_400Regular, Poppins_500Medium, Poppins_600SemiBold, Poppins_700Bold, Poppins_800ExtraBold, Poppins_900Black } from '@expo-google-fonts/poppins';
/* ------------------------------ global styles ----------------------------- */


/* --------------------------------- colors Themes --------------------------------- */
export function ChangeOpacity(theme_Color: String, opacity: String) {
  let RGB_TO_RGBA = theme_Color.replace(/rgb/i, "rgba");
  RGB_TO_RGBA = RGB_TO_RGBA.replace(/\)/i, ',' + opacity + ')');
  return RGB_TO_RGBA
}
export function rgba2hex(orig: string) {
  let a, isPercent,
    rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
    alpha = (rgb && rgb[4] || "").trim(),
    hex = rgb ?
      (rgb[1] | 1 << 8).toString(16).slice(1) +
      (rgb[2] | 1 << 8).toString(16).slice(1) +
      (rgb[3] | 1 << 8).toString(16).slice(1) : orig;

  if (alpha !== "") {
    a = alpha;
  } else {
    a = 1;
  }
  // multiply before convert to HEX
  a = ((a * 255) | 1 << 8).toString(16).slice(1)
  hex = '#' + hex + a;

  return hex;
}
export function IDtoTheme(ID: number) {
  if (ID === 0) {
    return Light
  }
  else if (ID === 1) {
    return NonVeg
  }
  else if (ID === 2) {
    return Veg
  }
  else if (ID === 3) {
    return Egg
  }
  else if (ID === 4) {
    return Dark
  }
}

export async function AddFont(callback) {

  let [fontsLoaded] = await useFonts({
    Poppins_100Thin, Poppins_200ExtraLight, Poppins_300Light, Poppins_400Regular, Poppins_500Medium, Poppins_600SemiBold, Poppins_700Bold, Poppins_800ExtraBold, Poppins_900Black
  });
  callback(fontsLoaded);
}

export const P100 = 'Poppins_100Thin';
export const P200 = 'Poppins_200ExtraLight';
export const P300 = 'Poppins_300Light';
export const P400 = 'Poppins_400Regular';
export const P500 = 'Poppins_500Medium';
export const P600 = 'Poppins_600SemiBold';
export const P700 = 'Poppins_700Bold';
export const P800 = 'Poppins_800ExtraBold';
export const P900 = 'Poppins_900Black';



export const card_container = (H: string | number, W: string | number, MV: number, MH: number, PH: number, PV: number) => {
  const returnStyles = {
    height: H,
    width: W,
    marginVertical: MV,
    marginHorizontal: MH,
    paddingHorizontal: PH,
    paddingVertical: PV,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    elevation: 5,
    shadowColor: 'black',
  }
  return returnStyles
}


export const CreateShadow = (elevation: number, Shcolor: string,) => {
  const returnStyles = {
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    elevation: elevation,
    shadowColor: Shcolor,
  }
  return returnStyles
}

export const CreateBorder = (Bocolor: string,) => {
  const returnStyles = {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Bocolor,
  }
  return returnStyles
}


export const X_axis = () => {
  return {
    flexDirection: 'row',
    alignItems: 'center',
  }
}

export const Y_axis = () => {
  return {
    flexDirection: 'column',
    justifyContent: 'center',
  }
}
export const Createring = (size: { H: number, V: number }, color: string, Bwidth: number) => {
  const returndata = {
    paddingHorizontal: size.H,
    paddingVertical: size.V,
    borderStyle: 'solid',
    borderWidth: Bwidth,
    borderColor: color,
  }
  return returndata
}

export const Transformer = (Rotation: string, Scale: number) => {
  const dataToReturn = {
    transform: [
      { rotate: Rotation },
      { scale: Scale },
    ]
  }
  return dataToReturn
}
export const PTop = (Top: number) => {
  const dataToReturn = {
    // position:include_absolute?'absolute':'relative',
    top: Top,
  }
  return dataToReturn
}
export const PBottom = (Bottom: number) => {
  const dataToReturn = {
    // position:include_absolute?'absolute':'relative',
    bottom: Bottom,
  }
  return dataToReturn
}
export const PLeft = (Left: number) => {
  const dataToReturn = {
    // position:include_absolute?'absolute':'relative',
    left: Left,
  }
  return dataToReturn
}
export const PRight = (Right: number) => {
  const dataToReturn = {
    // position:include_absolute?'absolute':'relative',
    right: Right,
  }
  return dataToReturn
}


export const setTimeBlock = (hour: number, howmany: number, BGcolor: string) => {
  const dataToReturn = {
    position: 'absolute',
    top: (hour - 1) * 16.67,
    backgroundColor: BGcolor,
    height: howmany * 16.67
  }
  return dataToReturn
}
export const setTimeLableBlock = (hour: number, TXTcolor: string) => {
  const dataToReturn = {
    position: 'absolute',
    top: (hour - 1) * 16.67,
    color: TXTcolor,
  }
  return dataToReturn
}








