import { configureStore } from '@reduxjs/toolkit';
import { offlinecheckerreducer } from './App/appOfflineManager';
import { locationAccessCheckerReducer } from './App/appLocationManager';
import { addressproviderReducer } from './User/userAddressManager';
import { navigationManagerReducer } from './App/appNavigationManager';
import { userProfileDataManagerReducer } from './User/userProfileDataManager';
import { appPinandFingerprintManagerReducer } from './App/appPinandFingerprintManager';
import { appApperienceManagerReducer } from './App/appApperienceManager';
import { userActivePackageDataManagerReducer } from './User/userActivePackageManager';
import { userRegisterationManagerReducer } from './App/userRegisterationManager';


export const store = configureStore({
    reducer: {
        offlinechecker: offlinecheckerreducer,
        locationaccess: locationAccessCheckerReducer,
        useraddressprovider: addressproviderReducer,
        navigationmanager: navigationManagerReducer,
        userprofiledata: userProfileDataManagerReducer,
        apppinandfingerprintmanager: appPinandFingerprintManagerReducer,
        appapperiencemanager: appApperienceManagerReducer,
        useractivepackagedata: userActivePackageDataManagerReducer,
        userregisterationmanager: userRegisterationManagerReducer,
    }
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;