import React from 'react';

export enum EncryptedStoreKeys {
    GoogleAccessToken = '@GoogleaccessToken',
    FirebaseAccessToken = '@FirebaseaccessToken',
    RasodaaAccessToken = '@RasodaaaccessToken',
}

export enum UnEncryptedStoreKeys {
    UserLoginedStatus = 'userLogined',
    isOldUser = 'isOldUser',
    AppPin = 'appPin',
    AppFingerprint = 'appFingerprint',
    AppTheme = 'appTheme',
    AppLanguage = 'appLanguage',
    AppFontSize = 'appFontSize',
    AppGuestMode = 'appGuestMode',
    ActivePackageData = 'activePackageData',
    UserRegisterationStatus = 'userRegisterationStatus',
    GoogleVerificationstate = 'googleVerificationstate',

}