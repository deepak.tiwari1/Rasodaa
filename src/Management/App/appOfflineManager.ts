import {createSlice} from '@reduxjs/toolkit';


const offlineChecker = createSlice({
    name: 'offlineChecker',
    initialState: {
        isNetworkAvailable: true,
    },
    reducers: {
        setOffline: (state) => {
           state.isNetworkAvailable = false; 
        },
        setOnline: (state) => {
            state.isNetworkAvailable = true; 
        },
        
    }

})

export const {setOffline,setOnline} = offlineChecker.actions;
export const offlinecheckerreducer =  offlineChecker.reducer;
export type offlineCheckerState = ReturnType<typeof offlineChecker.reducer>;
export type offlineCheckerActions = typeof offlineChecker.actions;


