import AsyncStorage from '@react-native-async-storage/async-storage';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { UnEncryptedStoreKeys } from '../LocalDBStoreKeys';

// /* save PIN to LocalDB ------------------------------------------------------- */
// const saveAppPin = async (value: string) => {
//     await SecureStore.setItemAsync(SecureStoreKeys.AppPin, value);
// }

// /* get PIN from LocalDB ----------------------------------------------------- */
// export const getsavedPin = async (callbackfunction) => {
//     let savedPin = await SecureStore.getItemAsync(SecureStoreKeys.AppPin);
//     if (savedPin) {
//         callbackfunction(savedPin);
//     }
// }

// /* save FINGERPRINT to LocalDB ---------------------------------------------- */
// const savefingerprint = async (value: string) => {
//     await SecureStore.setItemAsync(SecureStoreKeys.AppFingerprint, value);
// }

// /* get FINGERPRINT from LocalDB --------------------------------------------- */
// export const getsavedfingerprint = async (callbackfunction) => {
//     let savedfingerprint = await SecureStore.getItemAsync(SecureStoreKeys.AppFingerprint);
//     if (savedfingerprint) {
//         callbackfunction(savedfingerprint);
//     }
// }

export const setappPindata = createAsyncThunk(
    'app/setappPindata',
    async (value: string, thunkAPI) => {
        // keep some data in localStorage
        await AsyncStorage.setItem(UnEncryptedStoreKeys.AppPin, value);

        // proceed to send data to reducers
        let savedPin = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppPin);
        if (savedPin) {
            //console.log('savedPin: ' + savedPin);
            return savedPin
        }
        else {
            return '0000'
        }
    }
)

export const setappfingerprintdata = createAsyncThunk(
    'app/setappfingerprintdata',
    async (value: boolean, thunkAPI) => {
        // keep some data in localStorage
        if (value == true) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.AppFingerprint, 'true');
        }
        else if (value == false) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.AppFingerprint, 'false');
        }
        let savedfingerprint = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppFingerprint);
        if (savedfingerprint) {
            //console.log('fingerprint Status: ' + savedfingerprint);
            if (savedfingerprint == 'true') {
                return true
            }
            else if (savedfingerprint == 'false') {
                return false
            }
        }
        else {
            return false
        }
    }
)


/* Redux state management slice for PIN AND FINGERPRINT --------------------------- */
const appPinandFingerprintManager = createSlice({
    name: 'appPinLoginManager',
    initialState: {
        appPinLogin: '',
        appFingerprintLogin: false,
    },
    reducers: {
        setAppPin: (state, action) => {
            state.appPinLogin = action.payload;
            //saveAppPin(action.payload);
        },
        EnableFingerprint: (state) => {
            state.appFingerprintLogin = true;
            //savefingerprint('true');
        },
        DisableFingerprint: (state) => {
            state.appFingerprintLogin = false;
            //savefingerprint('false');
        }

    },
    extraReducers: {
        [setappPindata.fulfilled]: (state, action) => {
            state.appPinLogin = action.payload;
        },
        [setappfingerprintdata.fulfilled]: (state, action) => {
            state.appFingerprintLogin = action.payload;
        }
    }

})

/* exporting Reducer -------------------------------------------------------- */
export const { setAppPin, EnableFingerprint, DisableFingerprint } = appPinandFingerprintManager.actions;
export const appPinandFingerprintManagerReducer = appPinandFingerprintManager.reducer; 