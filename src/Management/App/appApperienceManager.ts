import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { P100, P200, P300, P400, P500, P600, P700, P800, P900 } from '../Utility/GlobalStyles';
import { UnEncryptedStoreKeys } from '../../Management/LocalDBStoreKeys';
import AsyncStorage from '@react-native-async-storage/async-storage';
import i18n from 'i18n-js';

/* type declaration --------------------------------------------------------- */



type state = {
    fontMode: '',
    themeMode: '',
    languageMode: ''
}

export type Colors = {
    modeCode: string;
    Primary: string;
    OnPrimary: string;
    PrimaryContainer: string;
    OnPrimaryContainer: string;
    Secondry: string;
    OnSecondry: string;
    SecondryContainer: string;
    OnSecondryContainer: string;
    Tertiary: string;
    OnTertiary: string;
    TertiaryContainer: string;
    OnTertiaryContainer: string;
    Background: string;
    OnBackground: string;
    Surface: string;
    OnSurface: string;
    SurfaceVarient: string;
    OnSurfaceVarient: string;
    Outline: string;
    PrimaryColorShades: {
        PC_100: string;
        PC_200: string;
        PC_300: string;
        PC_400: string;
        PC_500: string;
        PC_600: string;
        PC_700: string;
        PC_800: string;
        PC_900: string;
    };
    SecondryColorShades: {
        SC_100: string;
        SC_200: string;
        SC_300: string;
        SC_400: string;
        SC_500: string;
        SC_600: string;
        SC_700: string;
        SC_800: string;
        SC_900: string;
    };
    TertiaryColorShades: {
        TC_100: string;
        TC_200: string;
        TC_300: string;
        TC_400: string;
        TC_500: string;
        TC_600: string;
        TC_700: string;
        TC_800: string;
        TC_900: string;
    };
    BackgroundColorShades: {
        BC_100: string;
        BC_200: string;
        BC_300: string;
        BC_400: string;
        BC_500: string;
        BC_600: string;
        BC_700: string;
        BC_800: string;
        BC_900: string;
    }
}

type FontAttributes = {
    fontSize: number;
    fontWeight: number;
    lineHeight: number;
    latterSpacing: number;
    fontFamily: string;
}

type Font = {
    Large: FontAttributes;
    Medium: FontAttributes;
    Small: FontAttributes;
}

export type initialFontdata = {
    DisplayFont: Font;
    HeadlineFont: Font;
    TitleFont: Font;
    LableFont: Font;
    BodyFont: Font;
}
/* -------------------------------------------------------------------------- */

/* data declearation -------------------------------------------------------- */
let fontdata: initialFontdata = {
    DisplayFont: {
        Large: {
            fontSize: 57,
            fontWeight: 400,
            lineHeight: 64,
            latterSpacing: 0,
            fontFamily: P400
        },
        Medium: {
            fontSize: 45,
            fontWeight: 400,
            lineHeight: 52,
            latterSpacing: 0,
            fontFamily: P400
        },
        Small: {
            fontSize: 36,
            fontWeight: 400,
            lineHeight: 44,
            latterSpacing: 0,
            fontFamily: P400
        }
    },
    HeadlineFont: {
        Large: {
            fontSize: 32,
            fontWeight: 400,
            lineHeight: 40,
            latterSpacing: 0.5,
            fontFamily: P400
        },
        Medium: {
            fontSize: 28,
            fontWeight: 400,
            lineHeight: 36,
            latterSpacing: 0,
            fontFamily: P400
        },
        Small: {
            fontSize: 24,
            fontWeight: 400,
            lineHeight: 32,
            latterSpacing: 0,
            fontFamily: P400
        }
    },
    TitleFont: {
        Large: {
            fontSize: 22,
            fontWeight: 400,
            lineHeight: 28,
            latterSpacing: 0,
            fontFamily: P400
        },
        Medium: {
            fontSize: 16,
            fontWeight: 500,
            lineHeight: 26,
            latterSpacing: 0.15,
            fontFamily: P500
        },
        Small: {
            fontSize: 16,
            fontWeight: 500,
            lineHeight: 26,
            latterSpacing: 0.15,
            fontFamily: P500
        }
    },
    LableFont: {
        Large: {
            fontSize: 14,
            fontWeight: 500,
            lineHeight: 20,
            latterSpacing: 1,
            fontFamily: P500
        },
        Medium: {
            fontSize: 12,
            fontWeight: 500,
            lineHeight: 16,
            latterSpacing: 0.5,
            fontFamily: P500
        },
        Small: {
            fontSize: 11,
            fontWeight: 500,
            lineHeight: 6,
            latterSpacing: 1,
            fontFamily: P500
        }
    },
    BodyFont: {
        Large: {
            fontSize: 16,
            fontWeight: 400,
            lineHeight: 24,
            latterSpacing: 0.5,
            fontFamily: P400
        },
        Medium: {
            fontSize: 14,
            fontWeight: 400,
            lineHeight: 20,
            latterSpacing: 0.25,
            fontFamily: P400
        },
        Small: {
            fontSize: 12,
            fontWeight: 400,
            lineHeight: 16,
            latterSpacing: 0.4,
            fontFamily: P400
        }
    }
}
let DarkMode: Readonly<Colors> = {
    modeCode: 'dark',
    Primary: '#FD9BAF',
    OnPrimary: '#000000',
    PrimaryContainer: '#32010B',
    OnPrimaryContainer: '#FECDD7',
    Secondry: '#FFE399',
    OnSecondry: '#000000',
    SecondryContainer: '#000000',
    OnSecondryContainer: '#FFF1CC',
    Tertiary: '#A6D1F2',
    OnTertiary: '#000000',
    TertiaryContainer: '#061C2D',
    OnTertiaryContainer: '#D2E8F9',
    Background: '#061C2D',
    OnBackground: '#E6E6E6',
    Surface: '#030303',
    OnSurface: '#E6E6E6',
    SurfaceVarient: '#A1A1A1',
    OnSurfaceVarient: '#A1A1A1',
    Outline: '#808080',
    PrimaryColorShades: {
        PC_900: '#fff5f5',
        PC_800: '#ffefef',
        PC_700: '#ffeaea',
        PC_600: '#ffe0e0',
        PC_500: '#ffd9d9',
        PC_400: '#ffd0d0',
        PC_300: '#ffc7c7',
        PC_200: '#ffbdbd',
        PC_100: '#ffb3b3',
    },
    SecondryColorShades: {
        SC_900: '#f2f2f2',
        SC_800: '#ebebeb',
        SC_700: '#e1e1e1',
        SC_600: '#d8d8d8',
        SC_500: '#d0d0d0',
        SC_400: '#c7c7C7',
        SC_300: '#BDBDBD',
        SC_200: '#B3B3B3',
        SC_100: '#AFAFAF',
    },
    TertiaryColorShades: {
        TC_900: '#E6E6E6',
        TC_800: '#D9D9D9',
        TC_700: '#CCCCCC',
        TC_600: '#C0C0C0',
        TC_500: '#B3B3B3',
        TC_400: '#A6A6A6',
        TC_300: '#999999',
        TC_200: '#8C8C8C',
        TC_100: '#808080',
    },
    BackgroundColorShades: {
        BC_900: '#E6E6E6',
        BC_800: '#CCCCCC',
        BC_700: '#B3B3B3',
        BC_600: '#999999',
        BC_500: '#808080',
        BC_400: '#666666',
        BC_300: '#4D4D4D',
        BC_200: '#333333',
        BC_100: '#280109',
    }
}
let LightMode: Readonly<Colors> = {
    modeCode: 'light',
    Primary: '#C8042D',
    OnPrimary: '#FFFFFF',
    PrimaryContainer: '#FECDD7',
    OnPrimaryContainer: '#32010B',
    Secondry: '#CC9400',
    OnSecondry: '#FFFFFF',
    SecondryContainer: '#FFF1CC',
    OnSecondryContainer: '#332500',
    Tertiary: '#1A70B2',
    OnTertiary: '#FFFFFF',
    TertiaryContainer: '#D2E8F9',
    OnTertiaryContainer: '#061C2D',
    Background: '#FCFCFC',
    OnBackground: '#1A1A1A',
    Surface: '#FCFCFC',
    OnSurface: '#1A1A1A',
    SurfaceVarient: '#E6E6E6',
    OnSurfaceVarient: '#4D4D4D',
    Outline: '#808080',
    PrimaryColorShades: {
        PC_100: '#fff5f5',
        PC_200: '#ffefef',
        PC_300: '#ffeaea',
        PC_400: '#ffe0e0',
        PC_500: '#ffd9d9',
        PC_600: '#ffd0d0',
        PC_700: '#ffc7c7',
        PC_800: '#ffbdbd',
        PC_900: '#ffb3b3',
    },
    SecondryColorShades: {
        SC_100: '#f2f2f2',
        SC_200: '#ebebeb',
        SC_300: '#e1e1e1',
        SC_400: '#d8d8d8',
        SC_500: '#d0d0d0',
        SC_600: '#c7c7C7',
        SC_700: '#BDBDBD',
        SC_800: '#B3B3B3',
        SC_900: '#AFAFAF',
    },
    TertiaryColorShades: {
        TC_100: '#E6E6E6',
        TC_200: '#D9D9D9',
        TC_300: '#CCCCCC',
        TC_400: '#C0C0C0',
        TC_500: '#B3B3B3',
        TC_600: '#A6A6A6',
        TC_700: '#999999',
        TC_800: '#8C8C8C',
        TC_900: '#808080',
    },
    BackgroundColorShades: {
        BC_100: '#E6E6E6',
        BC_200: '#CCCCCC',
        BC_300: '#B3B3B3',
        BC_400: '#999999',
        BC_500: '#808080',
        BC_600: '#666666',
        BC_700: '#4D4D4D',
        BC_800: '#333333',
        BC_900: '#1A1A1A',
    }

}
export let language_Code = {
    En: 'en-IN',
    Hi: 'hi-IN',
    Mr: 'mr-IN',
}

export const fontCode = {
    A: 'small',
    AA: 'normal',
    AAA: 'large',
}
/* -------------------------------------------------------------------------- */

/* function to ADD and Get Data --------------------------------------------- */
export const saveFontSizeData_TO_LOCALDB = createAsyncThunk(
    'fontSize/saveFontSizeData_TO_LOCALDB',
    async function (size: string, thunkAPI) {
        //console.log(size)
        try {
            if (size == fontCode.A) {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppFontSize, fontCode.A);
            }
            else if (size == fontCode.AA) {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppFontSize, fontCode.AA);
            }
            else if (size == fontCode.AAA) {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppFontSize, fontCode.AAA);
            }
            else {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppFontSize, fontCode.A);
            }
            const fontSize = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppFontSize);
            //console.log('fontSize', fontSize);
            if (fontSize) {
                return fontSize
            }
        }
        catch (error) {
            console.log(error);
        }
    }
)

export const saveAppThemeData_TO_LOCALDB = createAsyncThunk(
    'theme/saveAppThemeData_TO_LOCALDB',
    async function (theme: string, thunkAPI) {
        try {
            if (theme == 'light') {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppTheme, 'light');
            }
            else if (theme == 'dark') {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppTheme, 'dark');
            }
            else {
                await AsyncStorage.setItem(UnEncryptedStoreKeys.AppTheme, 'light');
            }
            const themedata = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppTheme);
            if (themedata) {
                //console.log(themedata);
                return themedata
            }
        }
        catch (error) {
            console.log(error);
        }
    }
)


export const saveAppLanguageData_TO_LOCALDB = createAsyncThunk(
    'language/saveAppLanguageData_TO_LOCALDB',
    async function (language: string, thunkAPI) {
        try {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.AppLanguage, language);
            const languagedata = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppLanguage);
            if (languagedata) {
                return languagedata
            }
        }
        catch (error) {
            console.log(error);
        }
    }
)


/* -------------------------------------------------------------------------- */

/* reducer config for AppApperienceManger ----------------------------------- */
const appApperienceManager = createSlice({
    name: 'appApperienceManager',
    initialState: {
        fontSize: 0,
        font: fontdata,
        languageMode: '',
        colors: LightMode
    },
    reducers: {},
    extraReducers: {
        [saveFontSizeData_TO_LOCALDB.fulfilled]: (state, action) => {
            if (action.payload == fontCode.A) {
                state.fontSize = -4;
            }
            else if (action.payload == fontCode.AA) {
                state.fontSize = 0;
            }
            else if (action.payload == fontCode.AAA) {
                state.fontSize = 4;
            }
        },
        [saveAppThemeData_TO_LOCALDB.fulfilled]: (state, action) => {
            if (action.payload == 'light') {
                state.colors = LightMode;
            }
            else if (action.payload == 'dark') {
                state.colors = DarkMode;
                //console.log(state.theme.modeCode)
            }
        },
        [saveAppLanguageData_TO_LOCALDB.fulfilled]: (state, action) => {
            i18n.locale = action.payload;
            state.languageMode = action.payload;
        }
    }
})


export const appApperienceManagerReducer = appApperienceManager.reducer;