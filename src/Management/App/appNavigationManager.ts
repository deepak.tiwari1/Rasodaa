import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
//import * as SecureStore from 'expo-secure-store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UnEncryptedStoreKeys } from '../LocalDBStoreKeys';

export const setappLogindata = createAsyncThunk(
    'app/setappLogindata',
    async (value: boolean, thunkAPI) => {
        // keep some data in localStorage
        if (value == true) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.UserLoginedStatus, 'true');
        }
        else if (value == false) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.UserLoginedStatus, 'false');
        }
        let UserLoginstatus = await AsyncStorage.getItem(UnEncryptedStoreKeys.UserLoginedStatus);
        if (UserLoginstatus) {
            //console.log('UserLoginstatus: ' + UserLoginstatus);
            if (UserLoginstatus == 'true') {
                return true
            }
            else if (UserLoginstatus == 'false') {
                return false
            }
        }
        else {
            return false
        }
    }
)



export const setolduserdata = createAsyncThunk(
    'app/setolduserdata',
    async (value: boolean, thunkAPI) => {
        // keep some data in localStorage
        if (value == true) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.isOldUser, 'true');
        }
        else if (value == false) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.isOldUser, 'false');
        }
        let oldUserStatus = await AsyncStorage.getItem(UnEncryptedStoreKeys.isOldUser);
        if (oldUserStatus) {
            //console.log('oldUserStatus: ' + oldUserStatus);
            if (oldUserStatus == 'true') {
                return true
            }
            else if (oldUserStatus == 'false') {
                return false
            }
        }
        else {
            return false
        }
    }
)

export const saveGuestMode_TO_LOCALDB = createAsyncThunk(
    'app/saveGuestMode_TO_LOCALDB',
    async (value: boolean, thunkAPI) => {
        // keep some data in localStorage
        if (value == true) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.AppGuestMode, 'true');
        }
        else if (value == false) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.AppGuestMode, 'false');
        }
        let GuestMode = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppGuestMode);
        //console.log('GuestMode: ' + GuestMode);
        if (GuestMode) {
            if (GuestMode == 'true') {
                return true
            }
            else if (GuestMode == 'false') {
                return false
            }
        }
        else {
            return false
        }
    }
)

type initialState = {
    isUser: boolean;
    isApp: boolean;
    isUtility: boolean;
    isGuestMode: boolean;
    isOldUser: boolean;
    isLogined: boolean;
    isUserHaveActivePackage: boolean;
}

const NavigationManager = createSlice({
    name: 'NavigationManager',
    initialState: {
        isUser: true,
        isApp: true,
        isUtility: true,
        isGuestMode: false,
        isLogined: false,
        isOldUser: false
    },
    reducers: {},
    extraReducers: {
        [setappLogindata.fulfilled]: (state, action) => {
            state.isLogined = action.payload;
        },
        [setolduserdata.fulfilled]: (state, action) => {
            state.isOldUser = action.payload;
        },
        [saveGuestMode_TO_LOCALDB.fulfilled]: (state, action) => {
            state.isGuestMode = action.payload;
        }
    }
})

//export const {  } = NavigationManager.actions;
export const navigationManagerReducer = NavigationManager.reducer;
export type navigationManagerState = ReturnType<typeof NavigationManager.reducer>;
export type navigationManagerActions = typeof NavigationManager.actions;
export default NavigationManager;