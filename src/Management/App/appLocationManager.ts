import { createSlice } from "@reduxjs/toolkit";

const locationAccessChecker = createSlice({
    name: "locationAccessChecker",
    initialState: {
        location_Granted: false,
        is_custom_location: false,
        location_Name: "",
        location_Address: "",
    },
    reducers: {
        setLocationGranted: (state, action) => {
            state.location_Granted = action.payload;
        },
        setLocation: (state, action) => {
            state.location_Name = action.payload.currentLocationName;
            state.location_Address = action.payload.currentLocationAddress;
        },
        setCustomLocationstatus: (state, action) => {
            state.is_custom_location = action.payload;
        }

    },

})
export const { setLocationGranted, setLocation, setCustomLocationstatus } = locationAccessChecker.actions;
export const locationAccessCheckerReducer = locationAccessChecker.reducer;
export type locationAccessCheckerState = ReturnType<typeof locationAccessChecker.reducer>;
export type locationAccessCheckerActions = typeof locationAccessChecker.actions;

