import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UnEncryptedStoreKeys } from '../LocalDBStoreKeys';

type RegisterationData = {
    email: string;
    password: string;
    username: string;
    phoneNumber: string;
}

const userRegisterationManagerSlice = createSlice({
    name: 'userRegisterationManager',
    initialState: {
        email: '',
        password: '',
        username: '',
        phoneNumber: '',
        role: 'customer',
    },
    reducers: {
        setEmail: (state, action) => {
            state.email = action.payload;
        },
        setPassword: (state, action) => {
            state.password = action.payload;
        },
        setUsername: (state, action) => {
            state.username = action.payload;
        },
        setPhoneNumber: (state, action) => {
            state.phoneNumber = action.payload;
        }
    },
    extraReducers: {}
})

export const { setEmail, setPassword, setUsername, setPhoneNumber } = userRegisterationManagerSlice.actions;
export const userRegisterationManagerReducer = userRegisterationManagerSlice.reducer;