import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UnEncryptedStoreKeys } from '../LocalDBStoreKeys';

export const saveActivePackageData_TO_LOCALDB = createAsyncThunk(
    'app/saveActivePackageData_TO_LOCALDB',
    async (value: activepackageData, thunkAPI) => {
        // keep some data in localStorage
        if (value) {
            await AsyncStorage.setItem(UnEncryptedStoreKeys.ActivePackageData, JSON.stringify(value));
        }
        let ActivePackageData = await AsyncStorage.getItem(UnEncryptedStoreKeys.ActivePackageData);
        //console.log('ActivePackageData: ' + ActivePackageData);
        if (ActivePackageData) {
            console.log(JSON.parse(ActivePackageData));
            return JSON.parse(ActivePackageData);
        }
        else {
            return null
        }
    }
)

export type activepackageData = {
    packageId: string,
    packageCheffId: string,
    packagePrice: string,
    packageDuration: string,
    packageStatus: boolean,
    packageCreatedDate: string,
    packageExpiryDate: string,

}

const userActivePackageDataManager = createSlice({
    name: 'userActivePackageDataManager',
    initialState: {
        packageId: '',
        packageCheffId: '',
        packagePrice: '',
        packageDuration: '',
        packageStatus: false,
        packageCreatedDate: '',
        packageExpiryDate: '',
    } as activepackageData,
    reducers: {},
    extraReducers: {
        [saveActivePackageData_TO_LOCALDB.fulfilled]: (state: activepackageData, action) => {
            if (action.payload) {
                state.packageId = action.payload.packageId;
                state.packageCheffId = action.payload.packageCheffId;
                state.packagePrice = action.payload.packagePrice;
                state.packageDuration = action.payload.packageDuration;
                state.packageStatus = action.payload.packageStatus;
                state.packageCreatedDate = action.payload.packageCreatedDate;
                state.packageExpiryDate = action.payload.packageExpiryDate;
            }
        }
    }
})

export const userActivePackageDataManagerReducer = userActivePackageDataManager.reducer;