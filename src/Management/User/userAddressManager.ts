import { createSlice } from '@reduxjs/toolkit';

export type address = {
    data: {
        addressId: number,
        firstLine: string,
        secondLine: string,
        city: string,
        state: string,
        zip: string,
        addresstype: string,
        is_default_address: boolean,
    },
    ui_data: {
        is_edit_address: boolean,
    }
}

const addressList: Array<address> = []

const addressprovider = createSlice({
    name: 'addressprovider',
    initialState: addressList,
    reducers: {
        addAddress: (state, action) => {
            state.push(action.payload);
        },
        removeAddress: (state, action) => {
            state.splice(action.payload, 1);
        },
        updateAddress: (state, action) => {
            state[action.payload.index] = action.payload.address;
        },
        setDefaultAddress: (state, action) => {
            state[action.payload.index].data.is_default_address = true;
            state.forEach((element, index) => {
                if (index != action.payload.index) {
                    element.data.is_default_address = false;
                }
            })
        },
        setEditAddress: (state, action) => {
            (action.payload.mode == 0 ? state[action.payload.index].ui_data.is_edit_address = true : state[action.payload.index].ui_data.is_edit_address = false);
            state.forEach((element, index) => {
                if (index != action.payload.index) {
                    element.ui_data.is_edit_address = false;
                }
            })
        }

    }
})
export const { addAddress, removeAddress, updateAddress, setDefaultAddress, setEditAddress } = addressprovider.actions;
export const addressproviderReducer = addressprovider.reducer;
export type addressproviderState = ReturnType<typeof addressprovider.reducer>;
export type addressproviderActions = typeof addressprovider.actions;
