import { createSlice } from "@reduxjs/toolkit";

type ProfileData = {
    username: string;
    useremail: string;
    userphone: string;
    userprofilePic: string;
    userVerified: boolean;

}
export const defaultProfilePic = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQkfF6nBhidhIzL330CYtg70I8tpDBGJ2YjBPnE9D9gY0iLmGu563WBIab4KBexSDv7kG8&usqp=CAU'

const userProfileDataManager = createSlice({
    name: "userProfileDataManager",
    initialState: {
        username: 'Loading...',
        useremail: 'Loading...',
        userphone: '',
        userprofilePic: defaultProfilePic,
        userVerified: false,
        id: '',
        user_id: '',
        unique_id: '',
        dob: '',
        gender: '',
        food_preference: '',
        spicy_level: '',
        fast_days: '',
    },
    reducers: {
        setUserProfileData: (state, action) => {
            state.username = action.payload.username;
            state.useremail = action.payload.useremail;
            state.userVerified = action.payload.userVerified;
        },
        setUserProfileDatafromRasodaaAPI: (state, action) => {
            state.username = action.payload.username;
            state.useremail = action.payload.useremail;
            state.id = action.payload.id;
            state.user_id = action.payload.user_id;
            state.unique_id = action.payload.unique_id;
            state.dob = action.payload.dob;
            state.gender = action.payload.gender;
            state.food_preference = action.payload.food_preference
            state.spicy_level = action.payload.spicy_level
            state.fast_days = action.payload.fast_days
        },
        setUserPhone: (state, action) => {
            state.userphone = action.payload.userphone;
        },
        setProfilePic: (state, action) => {
            state.userprofilePic = action.payload.userprofilePic;
        },
        setProfiletoDefault: (state) => {
            state.username = 'Loading...',
                state.useremail = 'Loading...',
                state.userphone = '',
                state.userprofilePic = defaultProfilePic,
                state.userVerified = '',
                state.id = '',
                state.user_id = '',
                state.unique_id = '',
                state.dob = '',
                state.gender = '',
                state.food_preference = '',
                state.spicy_level = '',
                state.fast_days = ''

        }
    }
})

export const { setUserProfileData, setUserPhone, setUserProfileDatafromRasodaaAPI, setProfilePic, setProfiletoDefault } = userProfileDataManager.actions;
export const userProfileDataManagerReducer = userProfileDataManager.reducer;
export type UserProfileDataManagerState = ReturnType<typeof userProfileDataManager.reducer>;
export type UserProfileDataManagerActions = typeof userProfileDataManager.actions;
