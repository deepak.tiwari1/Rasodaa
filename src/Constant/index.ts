

export const googleKeys = {
    expoClientId: '743279779049-oh0de5r25st2tqv3m1ne1sntf02i8pbb.apps.googleusercontent.com',
    androidClientId: '743279779049-o013ggpf3kfeca9j06g9n7aqigqtq1ea.apps.googleusercontent.com',
    iosClientId: '743279779049-jlgumjl48gv9569mcb24goocgngvv0d0.apps.googleusercontent.com',
    //webClientId: 'GOOGLE_GUID.apps.googleusercontent.com',
}

export const firebaseConfig: any = {
    apiKey: "AIzaSyCGLWuk9bAEjNVSaErUrUv02WikKFCmLmg",
    authDomain: "rasodaa-351304.firebaseapp.com",
    databaseURL: "https://rasodaa-351304.firebaseio.com",
    projectId: "rasodaa-351304",
    storageBucket: "rasodaa-351304.appspot.com",
    messagingSenderId: "743279779049",
    appId: "1:743279779049:android:3d3275c36b61b0fd842478",
    //measurementId: "G-measurement-id",
};

export const BASE_URL = "http://admin.rasodaa.com/api/"
export const SLIDER_CHEF = "http://admin.rasodaa.com/storage/"
export const BASEURL_IMAGE = "http://admin.rasodaa.com/storage/thumb/"