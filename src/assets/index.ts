/* Image Files -------------------------------------------------------------- */


/* Icon Files --------------------------------------------------------------- */



/* Animation Files ---------------------------------------------------------- */
import Cooking from './Lottie Files/Cooking.json';
import restorantFood from './Lottie Files/restorantFood.json';
import Rasodaa from './Lottie Files/Rasodaa.json';
import WomenCooking from './Lottie Files/WomenCooking.json';
import chooseCheff from './Lottie Files/chooseCheff.json';
import delivery from './Lottie Files/delivery.json';
import location from './Lottie Files/location.json';
import successfull from './Lottie Files/successfull.json';
import fingerprintsuccess from './Lottie Files/fingerprintsuccess.json';
import fingerprintrequest from './Lottie Files/fingerprintneeded.json';
import verifycode from './Lottie Files/verifycode.json';
import forgotpassword from './Lottie Files/forgotpassword.json';
import cheffoffer from './Lottie Files/cheffoffer.json';
/* Language Files ----------------------------------------------------------- */
import language from './JSONFiles/language.json';

/* District Files ----------------------------------------------------------- */
import district from './JSONFiles/IndianDistrict.json';




export const ONBOARDING_ANIMATION = {
    Screen1: Cooking,
    Screen2: restorantFood,
    Screen3: Rasodaa,
    Screen4: WomenCooking,
    Screen5: chooseCheff,
    Screen6: delivery,
}

export const FINGERPRINTREQUEST = fingerprintrequest;

export const FINGERPRINTSUCCESS = fingerprintsuccess;

export const REGISTERATION = successfull

export const LOCATION_ANIMATION = location;

export const LANGUAGE = language;

export const DISTRICT = district;

export const VERIFYCODE = verifycode;

export const FORGOTPASSWORD = forgotpassword;

export const CHEFFOFFER = cheffoffer;