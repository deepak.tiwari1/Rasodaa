import { View, Text, StyleSheet, Image } from 'react-native';
import React, { useEffect, useState } from 'react';
import * as GS from '../../Management/Utility/GlobalStyles';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import i18n from 'i18n-js';
//import { Ionicons, MaterialCommunityIcons, Feather, FontAwesome5, FontAwesome } from '@expo/vector-icons';
import { useTheme, useNavigationState, NavigationContainer } from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';


import AccountTabHeader from './HomeScreen/ProfileTab/header';

import HomeTabScreen from './HomeScreen/HomeTab';
import SearchTabScreen from './HomeScreen/SearchTab';
import ProfileTabScreen from './HomeScreen/ProfileTab';
import ActivePackageTabScreen from './HomeScreen/PackageTab';
import DonationTab from './HomeScreen/DonationTab';

import { RootState } from '../../Management';
import { useSelector, useDispatch } from 'react-redux';
import { setOffline, setOnline } from '../../Management/App/appOfflineManager';
import type { UserHomeScreenProps } from '../Screens/UserScreensPropTypes';
import type { BottomTabScreenProps } from '@react-navigation/bottom-tabs'
import { saveActivePackageData_TO_LOCALDB } from '../../Management/User/userActivePackageManager';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const HomeTab = createBottomTabNavigator();


export function HomeBottomTabManager(props: UserHomeScreenProps): JSX.Element {


  const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
  const navigationmanager = useSelector((state: RootState) => state.navigationmanager);
  const activepackageData = useSelector((state: RootState) => state.useractivepackagedata);
  const dispatch = useDispatch();
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (state.isConnected == false) {
        dispatch(setOffline());
        props.navigation.navigate('UtilityNetworkScreen');
      };
      if (state.isConnected == true) {
        dispatch(setOnline())
        props.navigation.navigate('UserHomeScreen');
      };
    });
  }, [])
  let network: boolean = useSelector((state: RootState) => state.offlinechecker.isNetworkAvailable);

  return (
    <HomeTab.Navigator initialRouteName='Home'
      screenOptions={{
        tabBarHideOnKeyboard: true,
        tabBarInactiveTintColor: colors.OnBackground,
        tabBarActiveTintColor: colors.OnBackground,
        tabBarItemStyle: {},
        tabBarStyle: { backgroundColor: colors.Background, height: 60, borderTopWidth: 0, borderTopColor: colors.OnBackground },
      }}>
      <HomeTab.Screen name="Home" component={HomeTabScreen}
        options={{
          headerShown: false,
          tabBarLabel: ({ focused, color }) =>
            (<Text style={[{ fontSize: 12, fontFamily: GS.P400, marginBottom: 5 }, (focused) ? { color: colors.OnBackground } : { color: colors.BackgroundColorShades.BC_400 }]}>{i18n.t('meal')}</Text>),
          tabBarIcon: ({ color, size, focused }) => (
            (focused) ? <FontAwesome5 name="home" size={24} style={[{ marginTop: 5 }]} color={colors.OnBackground} /> : <Ionicons name="home-outline" size={24} style={[{ marginTop: 5 }]} color={colors.BackgroundColorShades.BC_400} />
          ),
        }} />

      <HomeTab.Screen name="Search" component={SearchTabScreen}
        options={{
          headerShown: false,
          tabBarLabel: ({ focused, color }) =>
            (<Text style={[{ fontSize: 12, fontFamily: GS.P400, marginBottom: 5 }, (focused) ? { color: colors.OnBackground } : { color: colors.BackgroundColorShades.BC_400 }]}>{'Explore'}</Text>),
          tabBarIcon: ({ color, size, focused }) => (
            (focused) ? <Ionicons name="search" size={24} style={[{ marginTop: 5 }]} color={colors.OnBackground} /> : <Ionicons name="search-outline" size={24} style={[{ marginTop: 5 }]} color={colors.BackgroundColorShades.BC_400} />
          ),
        }} />
      {!navigationmanager.isGuestMode && <HomeTab.Screen name="ActivePackage" component={ActivePackageTabScreen}
        options={{
          headerShown: false,
          tabBarLabel: ({ focused, color }) =>
            (<Text style={[{ fontSize: 12, fontFamily: GS.P400, marginBottom: 5 }, (focused) ? { color: colors.OnBackground } : { color: colors.BackgroundColorShades.BC_400 }]}>{'Schedule'}</Text>),
          tabBarIcon: ({ color, size, focused }) => (
            (focused) ? <Ionicons name="fast-food" size={24} style={[{ marginTop: 5 }]} color={colors.OnBackground} /> : <Ionicons name="fast-food-outline" size={24} style={[{ marginTop: 5 }]} color={colors.BackgroundColorShades.BC_400} />
          ),
        }} />}
      {!navigationmanager.isGuestMode && <HomeTab.Screen name="Account" component={ProfileTabScreen}
        options={{
          header: ({ navigation, route, options, layout }) => (<AccountTabHeader data={navigation} />),
          tabBarLabel: ({ focused, color }) =>
            (<Text style={[{ fontSize: 12, fontFamily: GS.P400, marginBottom: 5 }, (focused) ? { color: colors.OnBackground } : { color: colors.BackgroundColorShades.BC_400 }]}>{'Profile'}</Text>),
          tabBarIcon: ({ color, size, focused }) => (
            (focused) ? <MaterialCommunityIcons name="account-circle" size={24} style={[{ marginTop: 5 }]} color={colors.OnBackground} /> : <MaterialCommunityIcons name="account-circle-outline" size={24} style={[{ marginTop: 5 }]} color={colors.BackgroundColorShades.BC_400} />
          ),
        }} />}
      {!navigationmanager.isGuestMode && <HomeTab.Screen name="Donation" component={DonationTab} initialParams={{ view: 'history' }}
        options={{
          headerShown: false,
          tabBarLabel: ({ focused, color }) =>
            (<Text style={[{ fontSize: 12, fontFamily: GS.P400, marginBottom: 5 }, (focused) ? { color: colors.OnBackground } : { color: colors.BackgroundColorShades.BC_400 }]}>{'Donation'}</Text>),
          tabBarIcon: ({ color, size, focused }) => (
            (focused) ? <MaterialIcons name="leaderboard" size={24} style={[{ marginTop: 5 }]} color={colors.OnBackground} /> : <MaterialIcons name="leaderboard" size={24} style={[{ marginTop: 5 }]} color={colors.BackgroundColorShades.BC_400} />
          ),
        }} />}


    </HomeTab.Navigator>

  )
}
