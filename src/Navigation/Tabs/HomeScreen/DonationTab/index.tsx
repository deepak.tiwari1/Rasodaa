import { View, Text, ScrollView, Pressable, Image } from 'react-native'
import React, { useMemo, useState, useEffect } from 'react';
import { createStyles, createContainers } from './styles'
import { useSelector } from 'react-redux'
import type { RootState } from '../../../../Management'
import * as LeaderboardData from './leaderboard.json';
import * as DonationData from './donationhistory.json';

export default function DonationTab(props) {
    // console.log(props);
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyles(colors, font), [colors, font]);
    const containers = useMemo(() => createContainers(colors), [colors]);
    const [isleaderboard, setisleaderboard] = useState(false)

    function top3(index: number) {
        if (index === 0) {
            return containers.index1
        }
        else if (index === 1) {
            return containers.index2
        }
        else if (index === 2) {
            return containers.index3
        }
    }

    useEffect(() => {
        // console.log(props.route.params.view);
        if (props.route.params.view == 'leaderboard') {
            setisleaderboard(true);
        }
        else if (props.route.params.view == 'history') {
            setisleaderboard(false);
        }
        else {
            setisleaderboard(false);
        }
    }, [props.route.params.view])
    return (
        <View style={containers.main}>
            <Text style={styles.headlingText}>Food Donation</Text>
            <View style={containers.headerContainer}>
                <Pressable style={[containers.headerbuttonContainer, (!isleaderboard ? containers.activeTab : containers.inactiveTab)]}
                    onPress={() => { setisleaderboard(false) }}>

                    <Text style={styles.headerButtonText}>My History</Text>
                </Pressable>
                <Pressable style={[containers.headerbuttonContainer, (isleaderboard ? containers.activeTab : containers.inactiveTab)]}
                    onPress={() => { setisleaderboard(true) }}>
                    <Text style={styles.headerButtonText}>Leaderboard</Text>
                </Pressable>
            </View>
            {isleaderboard ? (
                <View style={{ flex: 1 }}>
                    <View style={[containers.leaderboardTableContainer, { elevation: 10 }]}>
                        <View style={containers.leaderboardPositionConatiner}>
                            <Text style={styles.leaderboardTableText}>Points</Text>
                            <Text style={[styles.leaderboardTableText, { marginLeft: 20 }]}>Name</Text>
                        </View>
                        <View>
                            <Text style={styles.leaderboardTableText}>Donation Points</Text>
                        </View>
                    </View>
                    <ScrollView >
                        <View style={containers.leaderboardContainer}>
                            {LeaderboardData.data.map((item, index) => {
                                return (
                                    <View key={index} style={[containers.leaderboarditemContainer, top3(index)]}>
                                        <View style={containers.leaderboardPositionConatiner}>
                                            <Text style={[styles.leaderboardPostionText, (
                                                index === 0 || index === 1 || index === 2 ? { fontWeight: 'bold' } : {}
                                            )]}>{item.position}</Text>
                                            <View style={containers.leaderboardImageContainer}>
                                                <Image source={{ uri: item.image }} style={styles.leaderboardImage} />
                                                <Text style={styles.leaderboardPersonText}>{item.name}</Text>

                                            </View>

                                        </View>
                                        <View>
                                            <Text style={styles.leaderboardPointText}>{item.points}</Text>
                                        </View>
                                    </View>
                                )
                            })}
                        </View>
                    </ScrollView>
                    <View style={[containers.leaderboarditemContainer, { borderTopColor: colors.Outline, borderTopWidth: 1, borderStyle: 'solid' }]}>
                        <View style={containers.leaderboardPositionConatiner}>
                            <Text style={styles.leaderboardPostionText}>{LeaderboardData.userdata.position}</Text>
                            <View style={containers.leaderboardImageContainer}>
                                <Image source={{ uri: LeaderboardData.userdata.image }} style={styles.leaderboardImage} />
                                <Text style={styles.leaderboardPersonText}>{LeaderboardData.userdata.name}</Text>

                            </View>

                        </View>
                        <View>
                            <Text style={styles.leaderboardPointText}>{LeaderboardData.userdata.points}</Text>
                        </View>
                    </View>
                </View>)
                : (
                    <View>
                        <View style={[containers.historyTableContainer, { elevation: 10 }]}>
                            <Text style={styles.leaderboardTableText}>Date</Text>
                            <Text style={[styles.leaderboardTableText, { marginLeft: 20 }]}>Tiffin</Text>
                            <Text style={styles.leaderboardTableText}> Points</Text>

                        </View>
                        <ScrollView style={{ borderBottomColor: colors.Outline, borderBottomWidth: 1, borderStyle: 'solid' }}>
                            <View style={containers.donationHistoryContainer}>
                                {DonationData.data.map((item, index) => {
                                    return (
                                        <View key={index} style={containers.donationHistoryItemContainer}>
                                            <View style={containers.donationHistoryItemHeaderContainer}>
                                                <Text style={styles.donationHistoryItemHeaderText}>{item.date}</Text>
                                            </View>
                                            <View style={containers.donationHistoryItemBodyContainer}>
                                                <Text style={styles.donationHistoryItemBodyText}>{item.tiffinname.slice(0, 18)}...</Text>
                                            </View>

                                            <Text style={styles.donationHistoryItemHeaderText}>{item.points}</Text>

                                        </View>

                                    )
                                })}
                            </View>
                        </ScrollView>
                    </View>
                )}
        </View>
    )
}