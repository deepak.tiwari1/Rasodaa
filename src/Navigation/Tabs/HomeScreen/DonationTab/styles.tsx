import { StyleSheet } from 'react-native';
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager';

export const createContainers = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        paddingHorizontal: 20,
        paddingTop: 20,
        backgroundColor: colors.Background
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    headerbuttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        width: '50%',
        paddingVertical: 10,
        elevation: 5
    },
    activeTab: {
        backgroundColor: colors.PrimaryContainer,
    },
    inactiveTab: {
        backgroundColor: colors.Background,
    },
    leaderboardContainer: {
        width: '100%',
        paddingTop: 20,
    },
    leaderboarditemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 20,
    },
    leaderboardPositionConatiner: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    leaderboardImageContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 25,
    },
    index1: {
        backgroundColor: colors.PrimaryContainer,
        marginBottom: 24,
        borderRadius: 10
    },
    index2: {
        backgroundColor: colors.SecondryContainer,
        marginBottom: 12,
        borderRadius: 10
    },
    index3: {
        backgroundColor: colors.TertiaryContainer,
        marginBottom: 8,
        borderRadius: 10
    },
    leaderboardTableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 20,
        paddingHorizontal: 10,
    },
    donationHistoryContainer: {
        width: '100%',
        paddingTop: 10,

    },
    donationHistoryItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 10,
    },
    donationHistoryItemHeaderContainer: {
        flexDirection: 'row',
    },
    donationHistoryItemBodyContainer: {

    },
    historyTableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 10,
        paddingHorizontal: 10,
    }

})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    headerButtonText: {
        fontSize: 16,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnPrimaryContainer
    },
    leaderboardPostionText: {
        fontSize: 20,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground
    },
    leaderboardImage: {
        height: 25,
        width: 25,
        borderRadius: 10,
        marginRight: 10
    },
    leaderboardPersonText: {
        fontSize: 15,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground
    },
    leaderboardPointText: {
        fontSize: 15,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground
    },
    leaderboardTableText: {
        fontSize: 15,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground,
        fontWeight: 'bold'
    },
    donationHistoryItemHeaderText: {
        fontSize: 15,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground,
    },
    donationHistoryItemBodyText: {
        fontSize: 15,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground,
    },
    headlingText: {
        fontSize: 20,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground,
        fontWeight: 'bold',
        alignSelf: 'center',
        marginVertical: 20,
    }
})