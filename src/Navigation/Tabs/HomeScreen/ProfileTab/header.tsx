import { Text, View, Pressable, Image, ImageBackground, StyleSheet, Platform } from 'react-native';
import React, { useEffect, useState, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../../Management';
import { getGoogleAccessToken, getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import * as ImagePicker from 'expo-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { BASE_URL, SLIDER_CHEF } from '../../../../Constant/index';
import { createHeaderStyles, createContainers } from './styles'
import { setUserProfileData, setProfilePic, setUserPhone, defaultProfilePic, setUserProfileDatafromRasodaaAPI } from '../../../../Management/User/userProfileDataManager';
import styled from 'styled-components'

type userdata = {
	'email': string,
	'family_name': string,
	'given_name': string,
	'id': string,
	'locale': string,
	'name': string,
	'picture': string,
	"verified_email": boolean,
}

type Rasodaauserdata = {
	"customer": {
		"id": string,
		"user_id": string,
		"unique_id": string,
		"name": string,
		"email": string,
		"phone": string,
		"dob": string,
		"image": string,
		"gender": string,
		"food_preference": string,
		"spicy_level": string,
		"country": string,
		"state": string,
		"city": string,
		"locality": string,
		"pincode": string,
		"fast_days": string,
		"deleted_at": string,
		"created_at": string,
		"updated_at": string,
		"country_name": string,
		"state_name": string,
		"city_name": string,
		"locality_name": string
	}
}

export default function AccountTabHeader({ data }) {
	const userData = useSelector((state: RootState) => state.userprofiledata);
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
	const dispatch = useDispatch();

	const headerStyles = useMemo(() => createHeaderStyles(colors, font), [colors, font])
	const containers = useMemo(() => createContainers(colors), [colors])
	const [accessToken, setAccessToken] = useState('');
	// console.log(data)
	async function getRasodaaUserInfo(accessToken: string | undefined) {
		if (accessToken != '') {
			setAccessToken(accessToken);
			await fetch(BASE_URL + 'customer/profile', {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${accessToken}`
				}
			}).then(res => res.json()).then((data: Rasodaauserdata) => {

				dispatch(setUserProfileDatafromRasodaaAPI({
					username: data.customer.name,
					useremail: data.customer.email,
					id: data.customer.id,
					user_id: data.customer.user_id,
					unique_id: data.customer.unique_id,
					dob: data.customer.dob,
					gender: data.customer.gender,
					food_preference: data.customer.food_preference,
					spicy_level: data.customer.spicy_level,
					fast_days: data.customer.fast_days
				}));

				dispatch(setProfilePic({
					userprofilePic: (data.customer.image != null ? data.customer.image : defaultProfilePic)
				}));
				dispatch(setUserPhone({
					userphone: data.customer.phone
				}));
			})
		}
	}

	getRasodaaAccessToken(getRasodaaUserInfo);

	const ImageType = (uri: string) => {
		const spliturl = uri.split('.');
		if (spliturl[spliturl.length - 1] == 'png') {
			return 'image/png'
		}
		else if (spliturl[spliturl.length - 1] == 'jpg') {
			return 'image/jpg'
		}
		else if (spliturl[spliturl.length - 1] == 'jpeg') {
			return 'image/jpeg'
		}
	}

	const pickImage = async () => {
		// No permissions request is necessary for launching the image library
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			allowsEditing: true,
			aspect: [1, 1],
			quality: 1,
		});

		if (!result.cancelled) {
			let photo = {
				uri: result.uri,
				type: ImageType(result.uri),
				name: 'photo.jpg',
			};
			let form = new FormData();
			form.append("profile_pic", photo);
			// setImage(result.uri);

			fetch("http://admin.rasodaa.com/api/customer/updateprofilepic", {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					'Authorization': 'Bearer ' + accessToken
				},
				body: form
			})
				.then(response => response.json())
				.then(response => {
					if (response.status == 200) {
						data.navigate('Home');
					}
					else {
						alert(response.status);
					}
				})
				.catch(error => console.log('error', error));
		}
	}

	return (
		<View style={containers.header_Container}>
			<View style={containers.profile_Container}>
				<View style={containers.profile_LeftContainer}>
					<Text style={headerStyles.usename}>{userData.username}</Text>
					<Text style={headerStyles.useremail}>{userData.useremail}</Text>
					<Text style={headerStyles.userphone}>{userData.userphone}</Text>

				</View>
				<View style={containers.profile_RightContainer}>
					<Image style={headerStyles.profileImage}
						source={{ uri: SLIDER_CHEF + userData.userprofilePic }} />
					<Pressable
						style={headerStyles.editbutton}
						onPress={() => { pickImage() }}>
						<Text style={headerStyles.editbuttontext}>Edit</Text>
					</Pressable>
				</View>
			</View>
			<View style={containers.shortcut_buttonContainer}>
				<Pressable style={headerStyles.shortcut_button}
					onPress={() => {
						data.navigate('UserFavouriteCheffScreen', {
							userID: 1
						})
					}}>
					<FontAwesome name="heart-o" size={22} color={colors.OnBackground} />
					<Text style={headerStyles.shortcut_buttontext}>My Cheffes</Text>
				</Pressable>
				<Pressable style={headerStyles.shortcut_button}
					onPress={() => {
						data.navigate('UserNotificationScreen', {
							userID: 1
						})
					}}>
					<Ionicons name="md-notifications-outline" size={22} color={colors.OnBackground} />
					<Text style={headerStyles.shortcut_buttontext}>Notification</Text>
				</Pressable>
				<Pressable style={headerStyles.shortcut_button}
					onPress={() => {
						data.navigate('UserSettingsScreen', {
							userID: 1
						})
					}}>
					<Ionicons name="settings-outline" size={22} color={colors.OnBackground} />
					<Text style={headerStyles.shortcut_buttontext}>Settings</Text>
				</Pressable>

			</View>

		</View>
	);
}




