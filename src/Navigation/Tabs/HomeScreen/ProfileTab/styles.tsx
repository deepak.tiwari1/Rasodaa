import { StyleSheet } from 'react-native'
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager';

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    shortcut_button: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 17,
        borderStyle: 'solid',
        borderColor: 'rgba(51,51,51,.2)',
        borderBottomWidth: 1
    },
    shortcutbutton_LeftSection: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    shortcut_buttontext: {
        fontFamily: font.BodyFont.Medium.fontFamily,
        fontSize: font.BodyFont.Medium.fontSize,
        color: colors.OnBackground,
        marginLeft: 15
    },
    iconBox: {
        padding: 10,
        backgroundColor: (colors.modeCode == 'dark' ? colors.BackgroundColorShades.BC_200 : 'white'),
        elevation: 2,
        borderRadius: 30
    }
})

export const createContainers = (colors: Colors) => StyleSheet.create({
    main: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        flex: 1,
        backgroundColor: colors.Background,
    },
    useroption_Container: {
        backgroundColor: colors.Background,
        borderRadius: 20,
        paddingHorizontal: 20,
        paddingVertical: 20,
        // elevation: 2
    },
    header_Container: {
        // backgroundColor: (colors.modeCode == 'light' ? 'rgba(243,243,243,1)' : 'rgba(21,21,21,1)'),
        backgroundColor: colors.Background,
        paddingHorizontal: 20,
        paddingTop: 35,
        justifyContent: 'flex-start'
    },
    shortcut_buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 25,
        paddingVertical: 10,
        // borderTopWidth: 3,
        // borderBottomWidth: 3,
        borderColor: colors.Outline,
        borderStyle: 'solid'
    },
    profile_Container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    profile_LeftContainer: {
        width: '60%',
        paddingHorizontal: 20
    },
    profile_RightContainer: {
        width: 100,
        height: 100,
        borderRadius: 50,
        overflow: 'hidden',
        elevation: 15,
        position: 'relative'
    }
})

export const createHeaderStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    usename: {
        fontSize: 24,
        fontFamily: font.TitleFont.Small.fontFamily,
        color: colors.OnBackground
    },
    useremail: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground
    },
    userphone: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground
    },
    shortcut_button: {
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.Background,
        borderRadius: 10,
        padding: 10,
        width: '30%',
        elevation: 3
    },
    shortcut_buttontext: {
        fontSize: font.BodyFont.Small.fontSize,
        fontFamily: font.BodyFont.Small.fontFamily,
        color: colors.OnBackground,
        marginTop: 5
    },
    profileImage: {
        width: '100%',
        height: '100%',
        borderRadius: 50,
    },
    editbutton: {
        position: 'absolute',
        bottom: 0,
        zIndex: 5,
        backgroundColor: 'rgba(51,51,51,.8)', alignItems: 'center',
        width: '100%',
        height: 30,
        justifyContent: 'center'
    },
    editbuttontext: {
        color: 'white'
    }
})