import { View, Text, Image, Pressable, StyleSheet, ScrollView } from 'react-native';
import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setappLogindata } from '../../../../Management/App/appNavigationManager';
import { setUserProfileData, setProfiletoDefault } from '../../../../Management/User/userProfileDataManager';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { saveGoogleAccessToken, saveRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import { createStyles, createContainers } from './styles'
import type { RootState } from '../../../../Management'


export default function Account(props) {
	let dispatch = useDispatch();
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
	const styles = useMemo(() => createStyles(colors, font), [colors, font])
	const containers = useMemo(() => createContainers(colors), [colors])
	const signOut = async () => {
		try {
			await GoogleSignin.signOut();
			dispatch(setProfiletoDefault());
			saveGoogleAccessToken('');
			saveRasodaaAccessToken('');

		} catch (error) {
			console.error(error);
		}
	};
	return (
		<ScrollView style={containers.main}>
			<View style={containers.useroption_Container}>
				{/* <Pressable style={styles.shortcut_button}
					onPress={() => {
						props.navigation.navigate('UserCartScreen', {
							userID: 1
						})
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<AntDesign name="shoppingcart" size={20} color={colors.OnBackground} />
						</View>
						<Text style={styles.shortcut_buttontext}>Your Cart</Text>
					</View>
					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable> */}
				<Pressable style={styles.shortcut_button}
					onPress={() => {
						props.navigation.navigate('UserCartScreen', {
							userID: 1
						})
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<AntDesign name="shoppingcart" size={20} color={colors.OnBackground} />

						</View>
						<Text style={styles.shortcut_buttontext}>Order history</Text>
					</View>

					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable>
				<Pressable style={styles.shortcut_button}
					onPress={() => {
						props.navigation.navigate('UserSwapHistoryScreen', {
							userID: 1
						})
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<MaterialIcons name="swap-calls" color={colors.OnBackground} size={20} />

						</View>
						<Text style={styles.shortcut_buttontext}>Swap tiffins history</Text>
					</View>

					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable>
				<Pressable style={styles.shortcut_button}
					onPress={() => {
						props.navigation.navigate('UserSkipHistoryScreen', {
							userID: 1
						})
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<Ionicons name="play-skip-forward-outline" color={colors.OnBackground} size={20} />

						</View>
						<Text style={styles.shortcut_buttontext}>Skip tiffins history</Text>
					</View>
					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable>
				<Pressable style={styles.shortcut_button}
					onPress={() => {
						props.navigation.navigate('UserAddressManagementScreen', {
							userID: 1
						})
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<FontAwesome5 name="address-card" size={20} color={colors.OnBackground} />
						</View>
						<Text style={styles.shortcut_buttontext}>Manage Address</Text>
					</View>
					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable>

				<Pressable style={styles.shortcut_button}
					onPress={() => {
						props.navigation.navigate('UserHelpAndSupportScreen')
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<Ionicons name="help" size={20} color={colors.OnBackground} />
						</View>
						<Text style={styles.shortcut_buttontext}>Chat with Us</Text>
					</View>
					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable>
				<Pressable style={styles.shortcut_button}
					onPress={() => {
						signOut()
						dispatch(setappLogindata(false));
						props.navigation.navigate('AppLoginScreen');
					}}>
					<View style={styles.shortcutbutton_LeftSection}>
						<View style={styles.iconBox}>
							<MaterialCommunityIcons name="logout" size={20} color={colors.OnBackground} />
						</View>
						<Text style={styles.shortcut_buttontext}>Logout</Text>
					</View>
					<MaterialIcons name="arrow-forward-ios" color={colors.OnBackground} size={20} />
				</Pressable>
			</View>
		</ScrollView>
	);
}

