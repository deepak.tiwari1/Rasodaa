import { View, StyleSheet, Dimensions, StatusBar, SafeAreaView, FlatList } from 'react-native';
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setLocationGranted, setLocation } from '../../../../Management/App/appLocationManager';
/* ----------a-------------------- importing components ----------------------------- */
import OfferSilder from '../../../../Component/Carousels/offersCarousels';
import FoodCatagory from '../../../../Component/cards/TiffinCatagoryCard';
import FoodCuisines from '../../../../Component/cards/TiffinCuisinesCard';
import VerticalCheffSection from '../../../../Component/section/VerticalCheffSection';
import { HorizontalCheffHeaderIconVarient, HorizontalCheffBackgroundHaveSliderVarient } from '../../../../Component/section/HorizontalCheffSection';
import LocationDisplay from '../../../../Component/bottomSheets/CustomLocationForm';
import type { UserHomeScreenProps } from '../../../Screens/UserScreensPropTypes';
import { RootState } from '../../../../Management';
import * as Location from 'expo-location';
import { HeadlineLarge, LableMedium } from '../../../../Component/Utility/TextComponent';



export default function UserHomeScreen(props: UserHomeScreenProps) {
  const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
  const location = useSelector((state: RootState) => state.locationaccess);
  const navigationmanger = useSelector((state: RootState) => state.navigationmanager);
  const dispatch = useDispatch();

  useEffect(() => {
    if (location.is_custom_location == false && navigationmanger.isGuestMode == false) {
      (async () => {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
          props.navigation.navigate('UtilityLocationScreen');
          dispatch(setLocationGranted(false));
          if (!location.location_Granted) {

          }
        }
        else {
          let location = await Location.getCurrentPositionAsync({});
          let locationName = await Location.reverseGeocodeAsync(
            { latitude: location.coords.latitude, longitude: location.coords.longitude });
          let payload = {
            currentLocationName: locationName[0].district,
            currentLocationAddress: locationName[0].district + ', ' + locationName[0].city + ', ' + locationName[0].region + ' ' + locationName[0].postalCode
          }

          dispatch(setLocation(payload));
          dispatch(setLocationGranted(true));
        }
      })();
    }
    // async function requestLocationPermission() {
    //   if (Platform.OS === 'ios') {
    //     Geolocation.setRNConfiguration({
    //       authorizationLevel: 'whenInUse'
    //     })

    //     Geolocation.requestAuthorization()
    //     // IOS permission request does not offer a callback :/
    //     return null
    //   }
    //   else if (Platform.OS === 'android') {
    //     try {
    //       const granted = await PermissionsAndroid.request(
    //         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION

    //       )
    //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //         Geolocation.getCurrentPosition(
    //           (position) => {
    //             //this.setState({ location: position, loading: false });
    //             let payload = {
    //                  currentLocationName: locationName[0].district,currentLocationAddress: locationName[0].district + ', ' + locationName[0].city + ', ' + locationName[0].region + ' ' + locationName[0].postalCode
    //                     }

    //                     dispatch(setLocation(payload));
    //                     dispatch(setLocationGranted(true));
    //             console.log(position); coords latitude longitude
    //           },
    //           (error) => {
    //             //this.setState({ location: error, loading: false });
    //             console.log(error);
    //           },
    //           { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
    //         )
    //       } 
    //       else {
    //         props.navigation.navigate('UtilityLocationScreen');
    //         dispatch(setLocationGranted(false));
    //       }
    //     } catch (err) {
    //       console.warn(err.message)
    //       return false
    //     }
    //   }
    // }
    // requestLocationPermission();
  }, []);



  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar backgroundColor={colors.Background} barStyle={colors.modeCode == 'light' ? "dark-content" : "light-content"} />
      <FlatList
        data={[1]}
        renderItem={() => {
          return (
            <View style={[styles.container, { backgroundColor: colors.Background }]}>
              <LocationDisplay {...props} />
              <OfferSilder />
              <FoodCatagory {...props} />
              <FoodCuisines {...props} />


              <HorizontalCheffHeaderIconVarient
                props={{
                  props: props,
                  title: 'Top rated home cheffs',
                  iconimage: 'https://cdn-icons-png.flaticon.com/512/1486/1486474.png'
                }} />
              <HorizontalCheffHeaderIconVarient
                props={{
                  props: props,
                  title: 'Value for monay',
                  iconimage: 'https://cdn-icons-png.flaticon.com/512/744/744414.png'
                }} />
              <HorizontalCheffBackgroundHaveSliderVarient
                props={{
                  props: props,
                  title: '3 Days tiffins'
                }} />
              <HorizontalCheffBackgroundHaveSliderVarient
                props={{
                  props: props,
                  title: '7 Days tiffins'
                }} />
              <HorizontalCheffBackgroundHaveSliderVarient
                props={{
                  props: props,
                  title: '30 Days tiffins'
                }} />

              <VerticalCheffSection props={props} title={'Home cheffs around you'} />

              <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
                <HeadlineLarge text={'Rasodaa'} clr={colors.BackgroundColorShades.BC_300} align={'left'} />
                <LableMedium text={'The Cheff Shown Above include personal, local and sponsored recommendations.'} clr={colors.BackgroundColorShades.BC_300} align={'left'} />
              </View>

            </View>

          )
        }}
      />

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  item_images: {
    height: 30,
    width: 30,
  },
  food_timing_container: {
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: 'rgba(243,243,243,1)'

  },
  food_timing_button_container: {
    flexDirection: 'row',
  },
  food_timing_button: {
    width: Dimensions.get('window').width / 2 - 30,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'solid',
    borderBottomWidth: 3,
    marginHorizontal: 5,

  },
  food_timing_details_container: {
    paddingHorizontal: 5,
  },
  food_timing_details: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
    paddingVertical: 18,
    paddingHorizontal: 15,
    elevation: 15,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 6,

  },
  view_more_button: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 6,
    elevation: 5,
    height: 45,
  }
});
