import { View, Text, TextInput, StyleSheet, Dimensions, Pressable } from 'react-native'
import React, { useEffect, useState, useMemo } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import { BASE_URL } from '../../../../Constant'
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager'
import { useSelector } from 'react-redux'
import type { RootState } from '../../../../Management'
import { createHeaderstyles } from './styles'

export default function SearchTab_Header(props) {

	const [searchText, setSearchText] = useState('')
	const [is_delete_clicked, set_is_delete_clicked] = useState(false)
	const [chefdata, setchefdata] = useState([]);
	const [tiffindata, settiffindata] = useState([]);
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
	const styles = useMemo(() => createHeaderstyles(colors, font), [colors])

	const fetchSearchData = (accessToken) => {
		fetch(BASE_URL + 'searchall?q' + '=' + searchText, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${accessToken}`
			}
		}).then(response => response.json()).then(responseJson => {
			if (responseJson.status == 200) {
				// console.log(responseJson.search.chefs)
				props.sendcheffeData[1](responseJson.search.chefs);
				props.sendtiffinData[1](responseJson.search.menus);
			}
			else {
				alert(responseJson.status)
			}
		}).catch(error => {
			console.log(error);
		})
	}

	useEffect(() => {
		getRasodaaAccessToken(fetchSearchData);
		// console.log('Search API called')
	}, [searchText])


	return (
		<View style={[styles.search]}>
			<View style={[styles.searchbar]}>
				<AntDesign name="search1" size={25} color={colors.OnBackground}></AntDesign>
				<TextInput
					style={[styles.searchbar_input]}
					defaultValue={searchText}
					maxLength={30}
					autoCorrect={true}
					placeholder="Search Tiffins And Chiffes"
					onFocus={() => setSearchText('')}
					onChange={
						(newtext) => {
							setSearchText(newtext.nativeEvent.text);

						}} />
				<Pressable onPress={() => { setSearchText(''); }}>
					<Entypo name="cross" size={24} color={'black'}></Entypo>
				</Pressable>

			</View>
		</View>
	)
}


