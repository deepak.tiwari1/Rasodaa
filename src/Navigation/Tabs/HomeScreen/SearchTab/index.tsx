import { View, Text, Image, TextInput, Dimensions, StyleSheet, Pressable, ScrollView } from 'react-native';
import React, { useState, useMemo, FC, useEffect } from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SearchTabHeader from './header'
import { createStyles } from './styles';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../Management';
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import { BASE_URL, SLIDER_CHEF } from '../../../../Constant';

type cheffDataType = {
	"id": string,
	"unique_id": string,
	"kitchen_name": string,
	"chef_name": string,
	"last_name": string,
	"cuisines": string,
	"image": string,
	"food_preference": string,
	"reviews": {
		"ratings": number,
		"total_reviews": number
	},
	"cuisine_title": string,
	"cuisine_slug": string
}

export default function SearchTabScreen(props) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const styles = useMemo(() => createStyles(colors, font), [colors])
	const [tabStatus, setTabStatus] = useState({
		is_Chiffes_active: true,
		is_Tiffins_active: false,
	})

	const [availableTiffinList, setavailableTiffinList] = useState([])
	const [availableCheffList, setavailableCheffList] = useState<cheffDataType[]>([])


	return (

		<ScrollView style={styles.searchPage}>
			<SearchTabHeader
				sendcheffeData={[availableCheffList, setavailableCheffList]}
				sendtiffinData={[availableTiffinList, setavailableTiffinList]} >
			</SearchTabHeader>
			<View style={styles.filter}>
				<Pressable
					onPress={() => { setTabStatus({ is_Chiffes_active: true, is_Tiffins_active: false }) }}
					style={[styles.filter_Tab, (tabStatus.is_Chiffes_active ? styles.filter_Tab__active : styles.filter_Tab__inactive)]}>
					<Text
						style={[styles.filter_Tab_text, (tabStatus.is_Chiffes_active ? styles.filter_Tab__active_text : styles.filter_Tab__inactive_text)]}>
						Chiffes
					</Text>
				</Pressable>
				<Pressable
					onPress={() => { setTabStatus({ is_Chiffes_active: false, is_Tiffins_active: true }) }}
					style={[styles.filter_Tab, (tabStatus.is_Tiffins_active ? styles.filter_Tab__active : styles.filter_Tab__inactive)]}>
					<Text
						style={[styles.filter_Tab_text, (tabStatus.is_Tiffins_active ? styles.filter_Tab__active_text : styles.filter_Tab__inactive_text)]}>
						Tiffins
					</Text>
				</Pressable>
			</View>
			<View style={[styles.results]}>
				{tabStatus.is_Chiffes_active ? availableCheffList.map((cheff, index) => {
					return (
						<Pressable key={index} style={styles.result} onPress={() => {
							props.navigation.navigate('UserCheffProfileScreen', {
								cheffId: availableCheffList[index].id,
								cheffName: availableCheffList[index].chef_name + ' ' + availableCheffList[index].last_name,
								cheffImage: availableCheffList[index].image,
								cheffCuisine: availableCheffList[index].cuisines,
								cheffFoodPreference: availableCheffList[index].food_preference,
								cheffReviews: availableCheffList[index].reviews,
								cheffCuisineTitle: availableCheffList[index].cuisine_title,

							})
						}}
						>
							<View style={[styles.result_image_container]}>
								<Image style={[styles.result_image]} source={{ uri: SLIDER_CHEF + cheff.image }} />
								{(cheff.food_preference == 'veg_only') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkey.com/png/full/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png' }} />
									</View> : []
								}
								{(cheff.food_preference == 'nonveg_only') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkit.com/png/full/257-2579552_non-veg-symbol-non-veg-symbol-png.png' }} />
									</View> : []
								}
								{(cheff.food_preference == 'both') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkey.com/png/full/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png' }} />
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkit.com/png/full/257-2579552_non-veg-symbol-non-veg-symbol-png.png' }} />
									</View> : []
								}
								{(cheff.food_preference == 'eggetarian') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://foofyfood.tamss.biz/assets/img/slider/1632896487ojp8VXLng4.jpg' }} />
									</View> : []
								}
							</View>
							<View style={[styles.result_details_container]}>
								<Text style={[styles.result_heading]}>{cheff.chef_name} {cheff.last_name}</Text>
								<View style={{ flexDirection: 'row' }}>
									{(cheff.cuisine_slug != null) ?
										(cheff.cuisine_title.split(',').splice(0, 2).map((cuisine, index) => {
											return <Text key={index} style={[styles.result_subheading]}>{cuisine} </Text>
										}
										)) : <Text style={[styles.result_subheading]}>No Cuisine</Text>
									}
								</View>
								<View style={[styles.result_rating_container]}>
									<AntDesign name="star" color={'rgb(255,164,28)'} size={15} />
									<Text style={[styles.result_rating_text]}>5</Text>
								</View>
								<View style={[styles.result_foottime]}>
									<Text style={[styles.result_foodtime_text]}>Lunch</Text>
									<Text style={[styles.result_foodtime_text]}>Dinner</Text>
								</View>
							</View>

						</Pressable>
					)
				}) : []}
				{tabStatus.is_Tiffins_active ? availableTiffinList.map((Tiffins, index) => {
					return (
						<Pressable key={index} style={[styles.result]} onPress={() => {
							props.navigation.navigate('CheffProfileScreen', {
								cheffId: 1,
								cheffName: 'Sanjiv Kumar'
							})
						}}>
							<View style={[styles.result_image_container]}>
								<Image style={[styles.result_image]} source={{ uri: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg' }} />
								<View style={[styles.tiffin]}>
									<Image style={[styles.tiffin_image]} source={{ uri: 'https://i.dlpng.com/static/png/6785192_preview.png' }} />
								</View>
							</View>
							<View style={[styles.result_details_container]}>
								<Text style={[styles.result_heading]}>{Tiffins}</Text>
								<Text style={[styles.result_subheading]}>Cheff. Sanjiv Kumar</Text>
								<View style={[styles.result_rating_container]}>
									<AntDesign name="star" color={'rgb(255,164,28)'} size={15} />
									<Text style={[styles.result_rating_text]}>5</Text>
								</View>
								<View style={[styles.result_foottime]}>
									<Text style={[styles.result_foodtime_text]}>Lunch</Text>
									<Text style={[styles.result_foodtime_text]}>Dinner</Text>
								</View>
							</View>

						</Pressable>
					)
				}) : []}
			</View>
		</ScrollView>
	);
}


