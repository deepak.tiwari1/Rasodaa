import { StyleSheet, Dimensions, ImageBackground } from 'react-native';
import type { Colors, initialFontdata } from "../../../../Management/App/appApperienceManager"

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    searchPage: {
        flex: 1,
        backgroundColor: colors.Background
    },

    filter: {
        width: Dimensions.get('window').width - 30,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        marginHorizontal: 15,
    },

    filter_Tab: {
        height: '100%',
        width: '50%',
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5
    },

    filter_Tab__active: {
        backgroundColor: colors.OnSurface
    },

    filter_Tab__inactive: {
        backgroundColor: colors.Surface
    },

    filter_Tab_text: {
        fontSize: font.BodyFont.Large.fontSize,
    },

    filter_Tab__active_text: {
        color: colors.Surface,
    },

    filter_Tab__inactive_text: {
        color: colors.OnSurface,
    },

    results: {
        marginVertical: 20,
        paddingHorizontal: 20,
        backgroundColor: colors.Background
    },
    result: {
        borderRadius: 8,
        // borderColor: colors.Outline,
        // borderStyle: 'solid',
        // borderWidth: 1,
        backgroundColor: colors.Surface,
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 120,
        elevation: 5

    },
    result_image_container: {
        width: '30%',
        height: '100%',
        position: 'relative'
    },
    result_image: {
        width: '100%',
        height: '100%',
        borderRadius: 8
    },
    result_foodtype: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        height: 20,
        // width: 40,
        flexDirection: 'row'
    },
    result_foodtype_image: {
        width: 20,
        height: 20,
    },
    result_details_container: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        width: '65%',
        height: '100%',
        justifyContent: 'flex-start'
    },
    result_heading: {
        fontSize: 18,
        lineHeight: 20,
        color: colors.OnSurface,
    },
    result_subheading: {
        fontSize: 14,
        color: colors.OnSurface,
    },
    result_rating_container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: 40
    },
    result_rating_image: {
        width: 20,
        height: 20
    },
    result_rating_text: {
        color: colors.OnSurface,
        marginLeft: 2
    },
    result_foottime: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 35,
    },
    result_foodtime_text: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: colors.Primary,
        paddingVertical: 2,
        paddingHorizontal: 10,
        marginRight: 5,
        borderRadius: 4,
        fontSize: 12,
        color: colors.OnSurface,
    },
    tiffin: {
        position: 'absolute',
        right: -13,
        bottom: -4.5,
        height: 50,
        width: 40,
        elevation: 5
    },
    tiffin_image: {
        width: '100%',
        height: '100%',

    }
})


export const createHeaderstyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({

    search: {

        width: Dimensions.get('window').width,
        height: 60,
        paddingHorizontal: 15,
        marginBottom: 10,
        paddingTop: 10
    },
    searchbar: {
        height: '100%',
        width: '100%',
        borderRadius: 8,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: colors.Outline,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 15,
    },
    searchbar_input: {
        marginHorizontal: 12,
        width: '80%',
        height: '100%',
        fontSize: 18,
        color: 'black'

    }


})