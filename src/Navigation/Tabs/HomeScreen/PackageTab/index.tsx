import { View, Text, ScrollView, StyleSheet, Dimensions, Pressable } from 'react-native';
import React, { useState, useMemo } from 'react';
import TiffineSheduled from '../../../../Component/section/CheffTiffineShedule';
import { HeadlineLarge, HeadlineMedium, HeadlineSmall, TitleLarge } from '../../../../Component/Utility/TextComponent';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../Management';
import PresentdaySchedual from '../../../../Component/Timeline/PresentdaySchedual';
import WeeklySchedual from '../../../../Component/Timeline/WeeklySchedual';
import { P500, P400, P600 } from '../../../../Management/Utility/GlobalStyles'
import { SegmentedArc } from '@shipt/segmented-arc-for-react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import i18n from 'i18n-js'
import { HorizontalOneTimeTiffinVarient } from '../../../../Component/section/HorizontalCheffSection';
import { createStyles, createContainers } from './styles';

const tiffins = {
  lunch: [
    {
      tiffinImagePath: 'https://www.secondrecipe.com/wp-content/uploads/2020/11/dal-bati-churma.jpg',
      tiffinName: 'Dal Bati Churma',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Lunch',
      tiffinDetails: ['150ml Daal', '4 Pcs Ghee Bati', '2 Churma Ladu', 'Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,

    },
    {
      tiffinImagePath: 'https://www.geetakiduniya.com/wp-content/uploads/2021/02/aloo-gobi.jpg',
      tiffinName: 'Gobhi Aloo or Roti',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Lunch',
      tiffinDetails: ['150ml Gobhi Sabji', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://www.dinedelicious.in/wp-content/uploads/2019/07/bhindi-ki-sabzi-e1587646044790.jpg',
      tiffinName: 'Bhindi Ki Sabji Roti',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Lunch',
      tiffinDetails: ['150ml Bhindi Sabji', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://s3.envato.com/files/289816475/DSC_3457.jpg',
      tiffinName: 'Dudh Sev or Roti',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Lunch',
      tiffinDetails: ['150ml Dush Sev Sabji ', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://allwaysdelicious.com/wp-content/uploads/2019/10/palak-paneer-oh-sq-scaled.jpg',
      tiffinName: 'Palak Panner',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Lunch',
      tiffinDetails: ['150ml Palak Panner', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://vismaifood.com/storage/app/uploads/public/1b8/afa/a77/thumb__700_0_0_0_auto.jpg',
      tiffinName: 'Matter Aloo ki sabji',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Lunch',
      tiffinDetails: ['150ml Matter Aloo', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },

  ],

  dinner: [
    {
      tiffinImagePath: 'https://www.archanaskitchen.com/images/archanaskitchen/0-Archanas-Kitchen-Recipes/Articles/Punjabi_Toor_Dal_Tadka_Recipe_Video_Tuvar_dal_recipe_2.jpg',
      tiffinName: 'Toor ki Dal or Roti',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Dinner',
      tiffinDetails: ['150ml Toor Daal', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://www.cubesnjuliennes.com/wp-content/uploads/2020/09/Instant-Pot-Whole-Masoor-Dal.jpg',
      tiffinName: 'Masoor ki Dal',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Dinner',
      tiffinDetails: ['150ml Masoor Daal', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://www.sailusfood.com/wp-content/uploads/2016/01/urad-dal-recipe.jpg',
      tiffinName: 'Urad ki Dal or Roti',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Dinner',
      tiffinDetails: ['150ml Urad Daal', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://1.bp.blogspot.com/-eTCh-J-J0ic/XwPm0tgt9fI/AAAAAAAAM8s/oT21wsxURYUacwbROrAaxnHcmdpHiQU1ACK4BGAsYHg/s3456/IMG_3818.JPG',
      tiffinName: 'Simla Mirch ki Sabji',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Dinner',
      tiffinDetails: ['150ml Sabji', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://www.teaforturmeric.com/wp-content/uploads/2021/10/Aloo-Baingan-Eggplant-and-Potato-Curry-8.jpg',
      tiffinName: 'Baigan ki sabji ',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Dinner',
      tiffinDetails: ['150ml Baigan Sabji', '6 Pcs Ghee Roti', '2 Churma Roti', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },
    {
      tiffinImagePath: 'https://www.chefatlarge.in/wp-content/uploads/2018/08/40209999_881656468707482_5983292221066051584_n.jpg',
      tiffinName: 'Besan ki Sabji',
      tiffinCheffName: 'Cheff. Sanjiv Kumar',
      tiffinRating: 5,
      tiffinType: 'Dinner',
      tiffinDetails: ['150ml Besan', '6 Pcs Ghee Roti', '2 Papad', '1 Salad'],
      tiffinSeats: [true, true, true, false, false],
      tiffinNotAvailableNo: 3,
    },

  ]
}
const timeline = [2, 3, 4, 5, 6, 7]

export default function ActivePackageTab(props) {
  const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
  const styles = useMemo(() => createStyles(colors, font), [colors, font]);
  const containers = useMemo(() => createContainers(colors), [colors]);
  const navigationmanger = useSelector((state: RootState) => state.navigationmanager);
  const [showArcRanges, setShowArcRanges] = useState(false);

  const segments = [
    {
      scale: 1,
      filledColor: colors.Primary,
      emptyColor: '#F2F3F5',
      data: { label: 'Red' }
    }

  ];

  const ranges = ['90'];

  const _handlePress = () => {
    setShowArcRanges(!showArcRanges);
  };
  const [foodtime, setfoodtime] = useState({ is_Lunch: true, is_Dinner: false });
  return (
    <ScrollView style={containers.scrollContainer}>
      {true ? (
        <View>
          <View style={containers.PackageHeaderContainer}>
            <Text style={styles.PackageHeaderText}>Current Plan</Text>
            <SegmentedArc
              segments={segments}
              fillValue={(100 / 60) * 54}
              isAnimated={true}
              animationDelay={1000}
              showArcRanges={showArcRanges}
              ranges={ranges}
            >
              {() => (<View style={containers.activePackageHeaderCenterContainer}>
                <Text style={styles.activePackageHeaderCenterText}>54 Tiffins</Text>
              </View>)}
            </SegmentedArc>
          </View>
          <View style={containers.activePackageBodyContainer}>
            <View>
              <PresentdaySchedual props={{ dateoftime: 1, lunch: tiffins.lunch[0], dinner: tiffins.dinner[0] }} />
            </View>
            <View>
              <View style={containers.WeeklyShedualContainer}>
                {timeline.map((timeline_item, index) => {
                  return (
                    <View key={index} style={{ marginBottom: 15 }}>

                      <WeeklySchedual
                        cardhavefunction={true}
                        props={{ dateoftime: timeline_item, lunch: tiffins.lunch[index], dinner: tiffins.dinner[index] }} />
                    </View>
                  )
                })}
              </View>

            </View>
          </View>
        </View>
      ) : (
        <View>
          <View style={containers.PackageHeaderContainer}>
            <Text style={styles.PackageHeaderText}>No Active Package</Text>
            <SegmentedArc
              segments={segments}
              fillValue={0}
              isAnimated={true}
              animationDelay={1000}
              showArcRanges={showArcRanges}
              ranges={ranges}
            >

            </SegmentedArc>
          </View>
          <View
            style={[styles.food_timing_container, { backgroundColor: colors.Background }]}>
            <View
              style={styles.food_timing_button_container}>
              <Pressable
                style={[styles.food_timing_button, (foodtime.is_Lunch) ? { borderBottomColor: colors.Primary } : { borderBottomColor: colors.PrimaryColorShades.PC_400 }]} onPress={() => { setfoodtime({ is_Lunch: true, is_Dinner: false }) }}>
                <Text style={{ fontFamily: P600, fontSize: 18, color: colors.OnBackground }}>{i18n.t('lunch')}</Text>
              </Pressable>
              <Pressable style={[styles.food_timing_button, (foodtime.is_Dinner) ? { borderBottomColor: colors.Primary } : { borderBottomColor: colors.PrimaryColorShades.PC_400 }]} onPress={() => { setfoodtime({ is_Lunch: false, is_Dinner: true }) }}>
                <Text style={{ fontFamily: P600, fontSize: 18, color: colors.OnBackground }}>{i18n.t('dinner')}</Text>
              </Pressable>
            </View>

            {(foodtime.is_Lunch) ? <View style={styles.food_timing_details_container}>
              <Pressable style={[styles.food_timing_details, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.Primary, elevation: 5 }]}>

                <View>
                  <Text style={{ fontFamily: P400, fontSize: 12, color: colors.OnSurface }}>{i18n.t('Order_now_to_get')}</Text>
                  <Text style={{ fontFamily: P500, fontSize: 18, color: colors.OnSurface }}>{i18n.t('Tomorrows_Lunch')}</Text>
                </View>
                <View>
                  <Ionicons name="ios-time-outline" size={40} color={colors.Primary} />
                </View>

              </Pressable>
              <Pressable onPress={() => { props.navigation.navigate('Search') }}
                style={[styles.view_more_button, { backgroundColor: colors.Primary, borderColor: colors.Primary }]}>
                <Text
                  style={{ fontFamily: P500, fontSize: 16, color: colors.OnPrimary }}>{navigationmanger.isGuestMode ? "LOGIN NOW" : "BOOK NOW"}</Text>
              </Pressable>
            </View> : []}
            {(foodtime.is_Dinner) ? <View style={styles.food_timing_details_container}>
              <Pressable style={[styles.food_timing_details, { backgroundColor: colors.Surface, borderColor: colors.Outline, shadowColor: colors.Primary, elevation: 5 }]}>
                <View>
                  <Text style={{ fontFamily: P400, fontSize: 12, color: colors.OnSurface }}>Order now to get </Text>
                  <Text style={{ fontFamily: P500, fontSize: 18, color: colors.OnSurface }}>Dinner</Text>
                </View>
                <View>
                  <Ionicons name="ios-time-outline" size={40} color={colors.Primary} />
                </View>

              </Pressable>
              <Pressable onPress={() => { props.navigation.navigate('Search') }}
                style={[styles.view_more_button, { backgroundColor: colors.Primary, borderColor: colors.Outline }]}>
                <Text style={{ fontFamily: P500, fontSize: 16, color: colors.OnPrimary }}>BOOK NOW</Text>
              </Pressable>
            </View> : []}
          </View>
          <HorizontalOneTimeTiffinVarient props={{
            props: props,
            title: 'One Time Tiffins'
          }} />
        </View>
      )}

    </ScrollView>
  );
}

