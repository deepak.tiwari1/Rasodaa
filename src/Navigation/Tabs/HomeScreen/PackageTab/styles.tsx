
import { Dimensions, StyleSheet } from "react-native";
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager';

const { height, width } = Dimensions.get("window");

export const createContainers = (colors: Colors) => StyleSheet.create({
    scrollContainer: {
        flex: 1, backgroundColor: colors.Background, paddingVertical: 20, paddingHorizontal: 10
    },
    PackageHeaderContainer: {
        flex: 1, alignItems: 'center', justifyContent: 'center', marginVertical: 20, borderRadius: 20, elevation: 1, paddingVertical: 20, backgroundColor: colors.Background, marginHorizontal: 20
    },
    activePackageHeaderCenterContainer: {
        alignItems: 'center'
    },
    activePackageBodyContainer: {
        marginTop: 15, marginHorizontal: 10
    },
    WeeklyShedualContainer: {
        borderTopWidth: 1,
        borderTopColor: 'rgba(51,51,51,.2)',
        borderStyle: 'solid',
        paddingVertical: 20,
    }
})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    PackageHeaderText: {
        fontSize: 20, fontWeight: '600', fontFamily: font.LableFont.Large.fontFamily, color: colors.OnBackground
    },
    activePackageHeaderCenterText: {
        fontSize: 20, fontWeight: '600', fontFamily: font.LableFont.Large.fontFamily, paddingTop: 30, color: colors.OnBackground
    },
    item_images: {
        height: 30,
        width: 30,
    },
    food_timing_container: {
        flex: 1,
        paddingVertical: 30,
        paddingHorizontal: 20,
        backgroundColor: 'rgba(243,243,243,1)'

    },
    food_timing_button_container: {
        flexDirection: 'row',
    },
    food_timing_button: {
        width: width / 2 - 30,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderStyle: 'solid',
        borderBottomWidth: 3,
        marginHorizontal: 5,

    },
    food_timing_details_container: {
        paddingHorizontal: 5,
    },
    food_timing_details: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
        paddingVertical: 18,
        paddingHorizontal: 15,
        elevation: 15,
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 6,

    },
    view_more_button: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 6,
        elevation: 5,
        height: 45,
    }
})