import { View, Text, Pressable } from 'react-native'
import React, { useEffect } from 'react'
import { NavigationContainer, useNavigation } from '@react-navigation/native';

//import { AppStack } from './Screens/rootAppStack';
//import { UserStack } from './Screens/rootUserStack';
//import { UtilityStack } from './Screens/rootUtilityStack';

import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../Management';

import { createNativeStackNavigator } from '@react-navigation/native-stack';

import UserLoginScreen from './Screens/AppStackScreens/LoginScreen/index';
import UserSignupScreen from './Screens/AppStackScreens/SignupScreen';
import UserOnBordingScreen from './Screens/AppStackScreens/OnboardingScreen/index';
import UserForgotPasswordScreen from './Screens/AppStackScreens/ForgotPasswordScreen';
import AppOtpScreen from './Screens/AppStackScreens/OtpScreen';
import SuccessfulRegisterationScreen from './Screens/AppStackScreens/RegisterationSuccessfulScreen';


import UtilityLocationScreen from './Screens/UtilityStackScreens/UtilityLocationScreen';
import UtilityNetworkScreen from './Screens/UtilityStackScreens/UtilityNetworkScreen';
import UtilityCitySelectionScreen from './Screens/UtilityStackScreens/UtilityCitySelectionScreen';
import UtilityPinSubmitionScreen from './Screens/UtilityStackScreens/UtilityPinSubmitionScreen';
import UtilityPinCreationScreen from './Screens/UtilityStackScreens/UtilityPinCreationScreen';


import UserChangePasswordScreen from './Screens/UserStackScreens/UserChangePasswordScreen';
import UserTiffinsScreen from './Screens/UserStackScreens/UserTiffinsScreen';
import UserCheffProfileScreen from './Screens/UserStackScreens/UserCheffProfileScreen';
import UserCartScreen from './Screens/UserStackScreens/UserCartScreen';
import UserAddressManagementScreen from './Screens/UserStackScreens/UserAddressManagmentScreen';
import UserHelpAndSupportScreen from './Screens/UserStackScreens/HelpAndSupportScreen';
import UserPaymentDetailsScreen from './Screens/UserStackScreens/UserPaymentCardScreen';
import UserSettingsScreen from './Screens/UserStackScreens/UserSettingsScreen';
import UserNotificationScreen from './Screens/UserStackScreens/UserNotificationScreen';
import UserFavouriteCheffScrren from './Screens/UserStackScreens/UserFavouriteCheffScreen';
import UserCheckoutScreen from './Screens/UserStackScreens/UserCheckoutScreen';
import UserHomeScreen from './Screens/UserStackScreens/UserHomeScreen';
import UserSwapHistoryScreen from './Screens/UserStackScreens/UserSwapHistoryScreen';
import UserSkipHistoryScreen from './Screens/UserStackScreens/UserSkipHistoryScreen';

import UtilityFingerprintEnableScreen from './Screens/UtilityStackScreens/UtilityFingerprintEnableScreen';

import UserTiffinsScreenHeader from './Screens/UserStackScreens/UserTiffinsScreen/header'
import ChefProfileHeader from './Screens/UserStackScreens/UserCheffProfileScreen/header'

const Stack = createNativeStackNavigator();

export default function Navigation() {
    const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
    const navigationState = useSelector((state: RootState) => state.navigationmanager);
    const dispatch = useDispatch();

    return (
        <NavigationContainer >
            <Stack.Navigator
                screenOptions={{ headerStyle: { backgroundColor: colors.Background } }}
                initialRouteName={!navigationState.isOldUser ? "AppOnBordingScreen" : "AppLoginScreen"}>
                {!navigationState.isOldUser ?
                    <Stack.Screen name="AppOnBordingScreen" component={UserOnBordingScreen} options={{ headerShown: false }} /> : []
                }
                {navigationState.isApp ?
                    <Stack.Group>
                        <Stack.Screen name="AppLoginScreen" component={UserLoginScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="AppSignupScreen" component={UserSignupScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="AppForgotPasswordScreen" component={UserForgotPasswordScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="AppOtpScreen" component={AppOtpScreen} options={{ headerShown: false }} />

                        <Stack.Screen name="AppSuccessfulRegisterationScreen" component={SuccessfulRegisterationScreen} options={{ headerShown: false }} />
                    </Stack.Group> : null}

                {navigationState.isUtility ?
                    <Stack.Group>
                        <Stack.Screen name="UtilityLocationScreen" component={UtilityLocationScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UtilityNetworkScreen" component={UtilityNetworkScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UtilityCitySelectionScreen" component={UtilityCitySelectionScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UtilityPinCreationScreen" component={UtilityPinCreationScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UtilityPinSubmitionScreen" component={UtilityPinSubmitionScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UtilityFingerprintEnableScreen" component={UtilityFingerprintEnableScreen} options={{ headerShown: false }} />
                    </Stack.Group> : null}

                {navigationState.isUser ?
                    <Stack.Group>
                        <Stack.Screen name="UserHomeScreen" component={UserHomeScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserTiffinsScreen" component={UserTiffinsScreen} options={{ header: ({ navigation, route }) => (<UserTiffinsScreenHeader navigation={navigation} route={route} />) }} />
                        <Stack.Screen name="UserCheffProfileScreen" component={UserCheffProfileScreen} options={{ header: ({ navigation, route }) => (<ChefProfileHeader navigation={navigation} route={route} />) }} />
                        {/* <Stack.Screen name="UserCheffPackageScreen" component={UserCheffPackageScreen} options={{ headerShown: false }} /> */}
                        <Stack.Screen name="UserCartScreen" component={UserCartScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserAddressManagementScreen" component={UserAddressManagementScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserHelpAndSupportScreen" component={UserHelpAndSupportScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserPaymentDetailsScreen" component={UserPaymentDetailsScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserSettingsScreen" component={UserSettingsScreen}
                            options={{ headerShown: false }} />
                        <Stack.Screen name="UserNotificationScreen" component={UserNotificationScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserFavouriteCheffScreen" component={UserFavouriteCheffScrren} options={{ headerShown: false }} />
                        <Stack.Screen name="UserCheckoutScreen" component={UserCheckoutScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserChangePasswordScreen" component={UserChangePasswordScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserSkipHistoryScreen" component={UserSkipHistoryScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="UserSwapHistoryScreen" component={UserSwapHistoryScreen} options={{ headerShown: false }} />
                    </Stack.Group> : null}

            </Stack.Navigator>

        </NavigationContainer>
    )
}


export type StackScreenList = {
    AppOnBordingScreen: undefined;
    AppLoginScreen: {} | undefined;
    AppSignupScreen: {} | undefined;
    AppForgotPasswordScreen: {} | undefined;
    AppOtpScreen: {
        confirm: string | null,
        phonenumber: string;
    } | undefined;
    AppSuccessfulRegisterationScreen: {} | undefined;

    UtilityLocationScreen: {} | undefined;
    UtilityNetworkScreen: {} | undefined;
    UtilityCitySelectionScreen: {} | undefined;
    UtilityPinCreationScreen: {} | undefined;
    UtilityPinSubmitionScreen: {} | undefined;
    UtilityFingerprintEnableScreen: {} | undefined;

    UserChangePasswordScreen: {} | undefined;
    UserHomeScreen: {} | undefined;
    UserTiffinsScreen: { is_old_user: boolean } | undefined;
    UserCheffProfileScreen: { cheffID: number } | undefined;
    //UserCheffPackageScreen: {} | undefined;
    UserCartScreen: {} | undefined;
    UserAddressManagementScreen: {} | undefined;
    UserHelpAndSupportScreen: {} | undefined;
    UserPaymentDetailsScreen: {} | undefined;
    UserSettingsScreen: {} | undefined;
    UserNotificationScreen: {} | undefined;
    UserFavouriteCheffScreen: {} | undefined;
    UserCheckoutScreen: {} | undefined;
    UserSkipHistoryScreen: {} | undefined;
    UserSwapHistoryScreen: {} | undefined;

}





