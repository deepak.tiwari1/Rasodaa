import React from 'react';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { StackScreenList } from '..';

export type LocationScreenProps = NativeStackScreenProps<StackScreenList, 'UtilityLocationScreen'>;
export type LocationScreenRoute = LocationScreenProps['route'];
export type LocationScreenNavigation = LocationScreenProps['navigation'];

export type NetworkScreenProps = NativeStackScreenProps<StackScreenList, 'UtilityNetworkScreen'>;
export type NetworkScreenRoute = NetworkScreenProps['route'];
export type NetworkScreenNavigation = NetworkScreenProps['navigation'];

export type CitySelectionScreenProps = NativeStackScreenProps<StackScreenList, 'UtilityCitySelectionScreen'>;
export type CitySelectionScreenRoute = CitySelectionScreenProps['route'];
export type CitySelectionScreenNavigation = CitySelectionScreenProps['navigation'];

export type PinCreationScreenProps = NativeStackScreenProps<StackScreenList, 'UtilityPinCreationScreen'>;
export type PinCreationScreenRoute = PinCreationScreenProps['route'];
export type PinCreationScreenNavigation = PinCreationScreenProps['navigation'];

export type PinSubmitionScreenProps = NativeStackScreenProps<StackScreenList, 'UtilityPinSubmitionScreen'>;
export type PinSubmitionScreenRoute = PinSubmitionScreenProps['route'];
export type PinSubmitionScreenNavigation = PinSubmitionScreenProps['navigation'];

export type FingerprintSubmitionScreenProps = NativeStackScreenProps<StackScreenList, 'UtilityFingerprintEnableScreen'>;
export type FingerprintScreenRoute = FingerprintSubmitionScreenProps['route'];
export type FingerprintScreenNavigation = FingerprintSubmitionScreenProps['navigation'];