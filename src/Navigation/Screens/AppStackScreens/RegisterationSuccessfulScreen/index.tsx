import { View, Text } from 'react-native'
import React, { useMemo, useEffect, useState } from 'react'
import LottieView from 'lottie-react-native';
import { Createstyles, Button } from './styles'
import { REGISTERATION } from '../../../../assets';
import { HeadlineMedium, HeadlineSmall, TitleMedium } from '../../../../Component/Utility/TextComponent';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../Management';

export default function SuccessfulRegisterationScreen(props) {
    const styles = useMemo(() => Createstyles(), [])
    const { colors } = useSelector((state: RootState) => state.appapperiencemanager)

    return (
        <View style={styles.container}>
            <View>

                <LottieView style={styles.animationContainer} source={REGISTERATION} autoPlay={true} loop={false} />
                <HeadlineSmall
                    text={"Successfull Registeration"}
                    align={'center'}
                    clr={colors.OnBackground}
                />
                <Button bclr={colors.Primary}
                    onPress={() => {
                        props.navigation.navigate('AppLoginScreen')
                    }}
                >
                    <TitleMedium text={'Login Now'} align={'center'} clr={colors.OnPrimary} />
                </Button>
            </View>
        </View>
    )
}