import { StyleSheet, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

export const Createstyles = () => StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    animationContainer: {
        width: 300,
        height: 300,
    }
})

export const Button = styled.TouchableOpacity`
    paddingVertical: 10px;
    backgroundColor: ${props => props.bclr};
    borderRadius: 30px;
    marginTop: 20px;
`