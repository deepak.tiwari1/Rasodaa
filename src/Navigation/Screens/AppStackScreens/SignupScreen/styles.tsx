import { StyleSheet, Pressable } from 'react-native';
import { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createContainer = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 30,
    },
    signup_Conatiner: {
        paddingHorizontal: 40
    },
    form_Container: {
        marginVertical: 20
    },
    nonsignup_Container: {

    }
})

export const createStyle = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    header_text: {
        fontSize: font.DisplayFont.Medium.fontSize,
        color: colors.OnBackground,
        alignSelf: 'center'
    },
    form_input: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: colors.Outline,
        borderRadius: 30,
        paddingHorizontal: 20,
        marginVertical: 8,
        color: colors.OnBackground,
        height: 50
    },
    form_submitButtom: {
        backgroundColor: colors.Primary,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    form_submitButtonText: {
        color: colors.OnPrimary,
        fontFamily: font.TitleFont.Medium.fontFamily,
        fontSize: font.TitleFont.Medium.fontSize
    },
    nonsignup_buttontext: {
        fontFamily: font.LableFont.Large.fontFamily,
        fontSize: font.LableFont.Large.fontSize,
        color: colors.OnBackground,
        alignSelf: 'center'
    }
})

