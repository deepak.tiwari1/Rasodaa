import { View, Text, TextInput, StyleSheet, Image, Pressable, TouchableOpacity, } from 'react-native'
import React, { useMemo, useState, useEffect, useRef } from 'react'
import { AppSignupScreenProps } from '../../AppScreensPropTypes'
import { HeadlineLarge, DisplayMedium, LableLarge, TitleLarge, TitleMedium } from '../../../../Component/Utility/TextComponent';
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../../../Management'
import { createStyle, createContainer } from './styles'
import { saveGuestMode_TO_LOCALDB, setappLogindata } from '../../../../Management/App/appNavigationManager'

import { googleKeys } from '../../../../Constant'
import { saveGoogleAccessToken, getGoogleAccessToken } from '../../../../Management/services/AccessTokenManager';
import { UnEncryptedStoreKeys } from '../../../../Management/LocalDBStoreKeys';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import { setUsername, setEmail, setPassword, setPhoneNumber } from '../../../../Management/App/userRegisterationManager';

export default function UserSignupScreen({ navigation, route }: AppSignupScreenProps) {

    const { colors, font, fontSize } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyle(colors, font), []);
    const containers = useMemo(() => createContainer(colors), []);

    const dispatch = useDispatch();

    const [username, setusername] = useState('');
    const [email, setemail] = useState('');
    const [mobile, setmobile] = useState('');
    const [password, setpassword] = useState('');

    async function signInWithPhoneNumber(phoneNumber: string) {
        const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
        navigation.navigate('AppOtpScreen', {
            'confirm': confirmation.verificationId,
            'phonenumber': mobile,
        })
    }

    return (
        <View style={containers.main}>
            <View style={containers.signup_Conatiner}>
                <Text style={styles.header_text}>Rasodaa</Text>
                <View style={containers.form_Container}>
                    <TextInput
                        style={styles.form_input}
                        placeholder="Enter Full Name"
                        placeholderTextColor={colors.BackgroundColorShades.BC_300}
                        maxLength={20}
                        onChangeText={(text) => setusername(text)}
                    />
                    <TextInput
                        style={styles.form_input}
                        placeholder="Enter Mobile Number"
                        placeholderTextColor={colors.BackgroundColorShades.BC_300}
                        autoComplete="tel"
                        keyboardType="phone-pad"
                        textContentType="telephoneNumber"
                        maxLength={10}
                        onChangeText={(text) => setmobile(text)}
                    >
                    </TextInput>
                    <TextInput
                        style={styles.form_input}
                        placeholder="Enter Gmail Address"
                        placeholderTextColor={colors.BackgroundColorShades.BC_300}
                        autoComplete="email"
                        keyboardType="email-address"
                        onChangeText={(text) => setemail(text)}
                    >
                    </TextInput>
                    <TextInput
                        style={styles.form_input}
                        placeholder="Password"
                        placeholderTextColor={colors.BackgroundColorShades.BC_300}
                        secureTextEntry={true}
                        maxLength={8}
                        onChangeText={(text) => setpassword(text)}
                    >
                    </TextInput>
                    <TouchableOpacity
                        style={styles.form_submitButtom}
                        onPress={() => {
                            if (username.length > 4 && email.includes('@gmail.com') && mobile.length == 10 && password.length == 8) {
                                dispatch(setUsername(username));
                                dispatch(setEmail(email));
                                dispatch(setPassword(password));
                                dispatch(setPhoneNumber(mobile));
                                try {
                                    signInWithPhoneNumber("+91" + mobile);
                                }
                                catch (err) {
                                    alert("text:" + `Error: ${err.message}`);
                                }
                            }
                            else (
                                alert('Please fill all the details Correctly')
                            )
                        }
                        }
                    >
                        <Text style={styles.form_submitButtonText}>Register</Text>
                    </TouchableOpacity>

                </View>
                <View style={containers.nonsignup_Container}>
                    <TouchableOpacity
                        style={{ paddingTop: 10 }}
                        onPress={() => { navigation.navigate('AppLoginScreen') }}>
                        <Text style={styles.nonsignup_buttontext}>Already have an account?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}