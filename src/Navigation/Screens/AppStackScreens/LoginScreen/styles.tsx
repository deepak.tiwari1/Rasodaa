import { StyleSheet } from 'react-native'
import type { Colors } from '@management/App/appApperienceManager'
import { initialFontdata } from '../../../../Management/App/appApperienceManager';

export const createContainer = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20,
        backgroundColor: colors.Background,
    },
    login_container: {
        width: '100%',
        paddingHorizontal: 40,
        paddingVertical: 20,
        borderRadius: 30,
        justifyContent: 'space-between',
        backgroundColor: colors.Background,

    },
    login_formContainer: {
        marginVertical: 10

    },
    login_optionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between', marginVertical: 10
    },
    nonlogin_optionContainer: {
        marginVertical: 15
    },

})

export const createStyle = (colors: Colors, font: initialFontdata) => StyleSheet.create({

    form_input: {

        width: '100%',
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: colors.Outline,
        borderStyle: 'solid',
        paddingHorizontal: 20,
        marginTop: 7,
        fontSize: 16,
        color: colors.OnSurface,
        marginVertical: 8
    },
    form_submitButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 13,
        borderRadius: 30,
        backgroundColor: colors.Primary,
        marginTop: 5
    },
    login_optionButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        height: 52,
        marginTop: 5,
        borderRadius: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: colors.Outline,
        backgroundColor: colors.Surface,
        width: '49%',

    },

    nonlogin_optionButton: {
        marginVertical: 1
    },
    nonlogin_optionButtonText: {
        fontSize: font.LableFont.Large.fontSize,
        fontFamily: font.LableFont.Large.fontFamily,
        color: colors.BackgroundColorShades.BC_600,
        alignSelf: 'center'
    },

})
