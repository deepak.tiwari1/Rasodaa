import { View, Text, SafeAreaView, TouchableOpacity, TextInput, Button } from 'react-native';
import React, { useEffect, useRef, useState, useMemo } from 'react'
import { AppLoginScreenProps } from '../../AppScreensPropTypes'
import Zocial from 'react-native-vector-icons/Zocial'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../../../Management'
import { setappLogindata, saveGuestMode_TO_LOCALDB } from '../../../../Management/App/appNavigationManager'
import { DisplayLarge, TitleLarge, LableLarge, TitleMedium, LableMedium, DisplayMedium } from '../../../../Component/Utility/TextComponent';
import { createStyle, createContainer } from './styles'
import {
	GoogleSignin,
	GoogleSigninButton,
	statusCodes,
} from '@react-native-google-signin/google-signin';

import { UnEncryptedStoreKeys } from '../../../../Management/LocalDBStoreKeys';
import { getGoogleAccessToken, saveGoogleAccessToken, saveRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { googleKeys } from '../../../../Constant';
import Users from '../../../../Management/services/api'
import RazorpayCheckout from 'react-native-razorpay';


GoogleSignin.configure({
	scopes: [], // [Android] what API you want to access on behalf of the user, default is email and profile
	webClientId: '743279779049-jtv089il1m53dlu2ij09st0ak8oba65p.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
	offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
	// hostedDomain: '', // specifies a hosted domain restriction
	// forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
	// accountName: '', // [Android] specifies an account name on the device that should be used
	// iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
	// googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
	// openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
	// profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
});

export default function UserLoginScreen({ navigation, route }: AppLoginScreenProps) {
	const { colors, font, fontSize } = useSelector((state: RootState) => state.appapperiencemanager);

	const styles = useMemo(() => createStyle(colors, font), [colors, font])
	const containers = useMemo(() => createContainer(colors), [colors])

	const navigationmanager = useSelector((state: RootState) => state.navigationmanager);
	const pin = useSelector((state: RootState) => state.apppinandfingerprintmanager);
	const dispatch = useDispatch();

	const [phoneNumber, setPhoneNumber] = useState('');
	const [userPassword, setuserPassword] = useState('');

	useEffect(() => {
		//console.log(navigationmanager.isLogined)
		if (navigationmanager.isOldUser && navigationmanager.isLogined) {
			dispatch(saveGuestMode_TO_LOCALDB(false));
			if (pin.appPinLogin.length != 6) {
				navigation.navigate('UtilityPinCreationScreen');
			}
			else if (pin.appPinLogin.length == 6) {
				navigation.navigate('UtilityPinSubmitionScreen');
			}

		}
		else if (navigationmanager.isOldUser && navigationmanager.isGuestMode) {
			navigation.navigate('UserHomeScreen');

		}
		else if (navigationmanager.isOldUser) {
			navigation.navigate('AppLoginScreen');
		}
	}, [])

	const signIn = async () => {
		try {
			await GoogleSignin.hasPlayServices();
			const userInfo = await GoogleSignin.signIn();
			// setuserinfo({ userInfo });
			const token = await GoogleSignin.getTokens();
			saveGoogleAccessToken(token.accessToken);
			const issignin = await GoogleSignin.isSignedIn();
			console.log(issignin)
			if (issignin) {
				navigation.navigate('UtilityPinCreationScreen');
			}
		} catch (error) {
			if (error.code === statusCodes.SIGN_IN_CANCELLED) {
				// user cancelled the login flow
			} else if (error.code === statusCodes.IN_PROGRESS) {
				// operation (e.g. sign in) is in progress already
			} else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
				// play services not available or outdated
			} else {
				// some other error happened
			}
		}
	};



	return (
		<View style={containers.main} >
			<View style={containers.login_container}>
				<DisplayMedium text={'Rasodaa'} align={'center'} clr={colors.OnBackground} />
				<View style={containers.login_formContainer}>
					<TextInput
						style={styles.form_input}
						placeholder="Email or Phone Number"
						placeholderTextColor={colors.BackgroundColorShades.BC_300}
						maxLength={10}
						onChangeText={(text) => setPhoneNumber(text)}
					>
					</TextInput>
					<TextInput

						style={styles.form_input}
						placeholder="Enter Password"
						placeholderTextColor={colors.BackgroundColorShades.BC_300}
						secureTextEntry={true}
						keyboardType="number-pad"
						maxLength={8}
						onChangeText={(text) => setuserPassword(text)}
					>
					</TextInput>
					<TouchableOpacity
						style={styles.form_submitButton}
						onPress={async () => {
							if (phoneNumber != '' && userPassword != '') {
								fetch("http://admin.rasodaa.com/api/login", {
									method: 'post',
									headers: {
										'Content-Type': 'application/json; charset=utf-8',
									},
									body: JSON.stringify({
										email: phoneNumber,
										password: userPassword,
									})
								}).then(res => res.json())
									.then((response) => {

										if (response.status == 200) {
											saveRasodaaAccessToken(response.user.token);
											console.log(response.user.token)
											dispatch(setappLogindata(true));
											navigation.navigate('UtilityPinCreationScreen');
										}
										else if (response.status == 400) {
											alert('Email Verification needed');
										}
										else {
											alert('Invalid Email or Password')
										}
									})
							}
						}}

					>
						<TitleMedium text={'Login'} clr={colors.OnPrimary} align={'center'} />
					</TouchableOpacity>
				</View>

				<View style={containers.login_optionContainer}>
					<TouchableOpacity
						style={styles.login_optionButton}
						onPress={() => signIn()}>
						<AntDesign name="google" size={22} color={colors.OnSurface} style={{ marginHorizontal: 5 }} />
						<TitleMedium
							text={'Google'}
							clr={colors.OnSurface}
							align={'center'} />
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.login_optionButton}
						onPress={() => {
							dispatch(setappLogindata(false));
							navigation.navigate('UtilityCitySelectionScreen');
						}}>
						<Zocial name="guest" size={22} color={colors.OnSurface} style={{ marginHorizontal: 5 }} />
						<TitleMedium
							text={'Guest'}
							clr={colors.OnSurface}
							align={'center'} />
					</TouchableOpacity>
				</View>
				<View style={containers.nonlogin_optionContainer}>
					<TouchableOpacity
						style={styles.nonlogin_optionButton}
						onPress={() => { navigation.navigate('AppForgotPasswordScreen') }}>
						<Text style={styles.nonlogin_optionButtonText}>Forgot Password? Reset</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.nonlogin_optionButton}
						onPress={() => {
							dispatch(setappLogindata(false));
							navigation.navigate('AppSignupScreen')
						}}>
						<Text style={styles.nonlogin_optionButtonText}>New User? Register</Text>
					</TouchableOpacity>
				</View>
			</View>
		</View>
	)
}


