import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import React, { useMemo } from 'react'
import { createStyle, createContainer } from './styles'
import { useSelector } from 'react-redux'
import type { RootState } from '../../../../Management'
import LottieView from 'lottie-react-native'
import { FORGOTPASSWORD } from '../../../../assets'

export default function UserForgotPasswordScreen() {
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
    const styles = useMemo(() => createStyle(colors, font), [])
    const containers = useMemo(() => createContainer(colors), [])

    return (
        <View style={containers.main}>
            <Text style={styles.header_text}>Forgot Password</Text>
            <View style={containers.forgot_Container}>
                <View style={containers.animation_Container}>
                    <LottieView source={FORGOTPASSWORD} autoPlay={true} style={styles.animation} />
                </View>
                <TextInput
                    style={styles.forgotInput}
                    placeholder='Email or Phone number'
                    placeholderTextColor={colors.BackgroundColorShades.BC_300}
                />
                <TouchableOpacity style={styles.forgotButton}>
                    <Text style={styles.forgotButton_text}>Find My Identity</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.forgot_noticeText}>If we find your Phone or Email ID then you got an OTP to verify. if Not then Kindly Contect on +91XXXXXXXXX for Account Recovery Process </Text>
        </View>
    )
}