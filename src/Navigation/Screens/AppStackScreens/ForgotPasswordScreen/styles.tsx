import { StyleSheet } from "react-native";
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager';

export const createContainer = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 40,
    },
    forgot_Container: {
        paddingHorizontal: 40,
        marginVertical: 10,
        width: '100%'
    },
    animation_Container: {
        alignItems: 'center'
    }
})

export const createStyle = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    header_text: {
        fontFamily: font.HeadlineFont.Medium.fontFamily,
        fontSize: font.HeadlineFont.Medium.fontSize,
        color: colors.OnBackground
    },
    forgotInput: {
        borderRadius: 30,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: colors.Outline,
        paddingHorizontal: 30,
        textAlign: 'center',
        width: '100%',
        color: colors.OnBackground,
        fontSize: font.TitleFont.Small.fontSize,
        fontFamily: font.TitleFont.Small.fontFamily,
    },
    forgotButton: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.Primary,
        borderRadius: 30,
        height: 50,
        marginVertical: 10
    },
    forgotButton_text: {
        fontSize: font.TitleFont.Medium.fontSize,
        fontFamily: font.TitleFont.Medium.fontFamily,
        color: colors.OnPrimary
    },
    forgot_noticeText: {
        fontSize: font.LableFont.Small.fontSize,
        fontFamily: font.LableFont.Small.fontFamily,
        color: colors.BackgroundColorShades.BC_300,
        alignSelf: 'center',
        paddingHorizontal: 20,
    },
    animation: {
        height: 250
    }
})