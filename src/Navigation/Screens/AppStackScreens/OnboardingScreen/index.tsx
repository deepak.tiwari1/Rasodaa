import { View, Text, StyleSheet, Image, Dimensions, StatusBar, TouchableOpacity, Button } from 'react-native';
import React, { useEffect, useState } from 'react';
import Onboarding from 'react-native-onboarding-swiper';
import type { OnBordingScreenProps } from '../../AppScreensPropTypes';
import Feather from 'react-native-vector-icons/Feather';
import { useSelector, useDispatch } from 'react-redux';
import { setolduserdata } from '../../../../Management/App/appNavigationManager';
import { RootState } from '../../../../Management';
import LottieView from 'lottie-react-native';
import { BodyLarge, TitleLarge, LableLarge, BodySmall, LableMedium, TitleSmall } from '../../../../Component/Utility/TextComponent';
import { ONBOARDING_ANIMATION } from '../../../../assets'

//import {  } from './styles'

export default function UserOnBordingScreen(props: OnBordingScreenProps) {
  const dispatch = useDispatch();
  const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
  const navigationmanager = useSelector((state: RootState) => state.navigationmanager);
  const [pageIndex, setpageIndex] = useState<number>(0);


  useEffect(() => {


  }, []);


  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={colors.Background} barStyle="light-content" />
      <Onboarding
        onDone={() => {
          dispatch(setolduserdata(true));
          props.navigation.navigate('AppLoginScreen')
        }}
        showSkip={false}
        showNext={false}
        bottomBarColor={colors.Background}
        transitionAnimationDuration={300}

        DoneButtonComponent={({ ...props }) => {
          return (
            <TouchableOpacity {...props}>
              <Feather name="arrow-right" size={24} color="black" style={{ marginRight: 15 }} />
              {/* <TitleSmall text={'Next'} align={'center'} clr={colors.OnBackground} /> */}
            </TouchableOpacity>
          )
        }}
        pages={[
          {
            backgroundColor: colors.Background,
            image: <LottieView source={ONBOARDING_ANIMATION.Screen1} autoPlay={true} style={{ height: 400 }} />,
            title: <TitleLarge text={'Do You Want'} clr={colors.OnBackground} align={'center'} />,
            subtitle: (
              <View style={{ width: '80%' }}>
                <LableLarge text={'Ghar ka Bana Khana for Your Lunch and Dinner ??'} clr={colors.BackgroundColorShades.BC_500} align={'center'} />
              </View>
            ),
          },
          {
            backgroundColor: colors.Background,
            image: <LottieView source={ONBOARDING_ANIMATION.Screen2} autoPlay={true} style={{ height: 400 }} />,
            title: <TitleLarge text={'But You'} clr={colors.OnBackground} align={'center'} />,
            subtitle: (
              <View style={{ width: '80%' }}>
                <LableLarge text={'Did Not Want Restaurant Food ?'} clr={colors.BackgroundColorShades.BC_500} align={'center'} />
              </View>
            ),
          },
          {
            backgroundColor: colors.Background,
            image: <LottieView source={ONBOARDING_ANIMATION.Screen3} autoPlay={true} loop={false} style={{ height: 400 }} />,
            title: <TitleLarge text={'Then'} clr={colors.OnBackground} align={'center'} />,
            subtitle: (
              <View style={{ width: '80%' }}>
                <LableLarge text={'Welcome To Rasodaa'} clr={colors.BackgroundColorShades.BC_500} align={'center'} />
              </View>
            ),
          },
          {
            backgroundColor: colors.Background,
            image: <LottieView source={ONBOARDING_ANIMATION.Screen4} autoPlay={true} style={{ height: 400 }} />,
            title: <TitleLarge text={'Rasodaa'} clr={colors.OnBackground} align={'center'} />,
            subtitle: (
              <View style={{ width: '80%' }}>
                <LableLarge text={'Presenting You Our Rasodaa Cheffs'} clr={colors.BackgroundColorShades.BC_500} align={'center'} />
              </View>
            ),
          },
          {
            backgroundColor: colors.Background,
            image: <LottieView source={ONBOARDING_ANIMATION.Screen5} autoPlay={true} style={{ height: 400 }} />,
            title: <TitleLarge text={'Book Your Cheff'} clr={colors.OnBackground} align={'center'} />,
            subtitle: (
              <View style={{ width: '80%' }}>
                <LableLarge text={'By Finding Cheff Schedule and Choice Which Cheff Package Most Suitable For You.'} clr={colors.BackgroundColorShades.BC_500} align={'center'} />
              </View>
            ),
          },
          {
            backgroundColor: colors.Background,
            image: <LottieView source={ONBOARDING_ANIMATION.Screen6} autoPlay={true} style={{ height: 400 }} />,
            title: <TitleLarge text={'Knock Knock!'} clr={colors.OnBackground} align={'center'} />,
            subtitle: (
              <View style={{ width: '80%' }}>
                <LableLarge text={'Your Tiffin is Ready. So What you Waiting For Lets Book Your Ghar Ka Khana Today.'} clr={colors.BackgroundColorShades.BC_500} align={'center'} />
              </View>
            ),
          },

        ]} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})