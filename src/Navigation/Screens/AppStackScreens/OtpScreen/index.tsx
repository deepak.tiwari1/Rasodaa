import { View, Text, Pressable, Dimensions, TextInput, TouchableOpacity } from 'react-native'
import React, { useEffect, useRef, useState, useMemo } from 'react'
import { HeadlineLarge, LableLarge, TitleMedium, HeadlineSmall } from '../../../../Component/Utility/TextComponent';
import styled from 'styled-components/native';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../../Management';
//import { PhoneAuthProvider, getAuth, signInWithCredential } from 'firebase/auth';
import Users from '../../../../Management/services/api'
import type { AppOtpScreenProps } from '../../AppScreensPropTypes';
import { getGoogleAccessToken, saveFirebaseAccessToken, getFirebaseAccessToken } from '../../../../Management/services/AccessTokenManager';
import { EncryptedStoreKeys } from '../../../../Management/LocalDBStoreKeys';
import auth from '@react-native-firebase/auth';
import OTPInputView from '@twotalltotems/react-native-otp-input'
// import { Countdown } from 'react-native-element-timer';
import { createStyle, createContainer } from './styles'
import LottieView from 'lottie-react-native'
import { VERIFYCODE } from '../../../../assets';

export default function AppOtpScreen(props: AppOtpScreenProps) {
    const { colors, fontSize, font } = useSelector((state: RootState) => state.appapperiencemanager);

    const styles = useMemo(() => createStyle(colors, font), []);
    const containers = useMemo(() => createContainer(colors), []);

    const localuserdata = useSelector((state: RootState) => state.userregisterationmanager);
    const dispatch = useDispatch();

    const [verificationCode, setVerificationCode] = useState('');
    //const auth = getAuth();

    const userobject = new Users({});

    const [firebaseAccessToken, setfirebaseAccessToken] = useState<string>('');

    const [isResendActive, setisResendActive] = useState(false)


    async function confirmCode() {
        let number = verificationCode;
        // console.log('number ' + number)
        try {
            //let data = await props.route.params?.confirm.confirm(number);
            const credential = auth.PhoneAuthProvider.credential(
                props.route.params?.confirm,
                number);
            if (credential) {
                saveFirebaseAccessToken(credential.token);
                fetch("http://admin.rasodaa.com/api/register", {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    body: JSON.stringify({
                        name: localuserdata.username,
                        email: localuserdata.email,
                        password: localuserdata.password,
                        phone: localuserdata.phoneNumber,
                        role: localuserdata.role
                    })
                }).then(res => res.json())
                    .then((response) => {
                        if (response.status == 200) {
                            props.navigation.navigate('AppSuccessfulRegisterationScreen');
                        }
                        else {
                            alert('Status Code is: ' + response.status);
                        }
                    }).catch(err => {
                        alert(err);
                    })
            }

        } catch (error) {
            console.log(error);
            alert('Invalid code.');
        }
    }

    return (
        <View style={containers.main}>
            <View>
                <Text style={styles.header_text}>Verify Phone</Text>
                <Text style={styles.subheader_text}>Code is Send to {props.route.params?.phonenumber}</Text>
                <OTPInputView
                    style={containers.otp_Container}
                    pinCount={6}
                    // autoFocusOnLoad={true}
                    onCodeChanged={(code) => { setVerificationCode(code) }}
                    codeInputFieldStyle={styles.otp_Input}
                />
            </View>
            <View style={containers.animation_Container}>
                <LottieView source={VERIFYCODE} style={styles.verification_Animation} autoPlay={true} />
            </View>
            <View >
                <View style={containers.resend_Container}>
                    <Text style={styles.resend_text}>{(!isResendActive ? "Don't recive code ?" : 'OTP Resended to your Number')}</Text>
                    {!isResendActive ?
                        <Pressable style={styles.resend_Button}
                            onPress={() => setisResendActive(true)}
                        >
                            <Text style={styles.resend_text}> Request again.</Text>
                        </Pressable> : []
                    }
                </View>
                <TouchableOpacity style={styles.verify_Button}
                    onPress={() => { confirmCode() }}>
                    <TitleMedium text={'Verify OTP and Continue'} clr={colors.OnPrimary} align={'center'} />
                </TouchableOpacity>
            </View>
        </View>
    )
}
