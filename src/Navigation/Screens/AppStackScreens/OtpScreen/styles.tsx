import { StyleSheet } from "react-native";
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager';

export const createContainer = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        paddingVertical: 40, paddingHorizontal: 40,
        justifyContent: 'space-between'
    },
    otp_Container: {
        height: 100,
        marginVertical: 10
    },
    animation_Container: {
        alignItems: 'center'
    },
    resend_Container: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export const createStyle = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    header_text: {
        fontSize: font.HeadlineFont.Medium.fontSize,
        fontFamily: font.HeadlineFont.Medium.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center'
    },
    subheader_text: {
        fontSize: font.LableFont.Large.fontSize,
        fontFamily: font.LableFont.Large.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center'
    },
    otp_Input: {
        color: 'black',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5,
        width: 50,
        height: 50
    },
    resend_text: {
        fontFamily: font.LableFont.Large.fontFamily,
        fontSize: font.LableFont.Large.fontSize,
        color: colors.OnBackground,
        alignSelf: 'center'
    },
    resend_Button: {

    },
    verification_Animation: {
        height: 200
    },
    verify_Button: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 10,
        backgroundColor: colors.Primary,
        marginTop: 5,
        width: '100%'
    }
})