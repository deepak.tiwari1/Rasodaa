import { View, Text, TouchableOpacity, Dimensions } from 'react-native'
import React, { useState, useEffect, useMemo } from 'react'
import type { CitySelectionScreenProps } from '../../UtilityScreensPropTypes';
import { FlatGrid } from 'react-native-super-grid';
//import * as Districts from '../../../../assets/JSONFiles/IndianDistrict.json';
import { TitleMedium, HeadlineMedium } from '../../../../Component/Utility/TextComponent';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../../Management';
import { setLocation } from '../../../../Management/App/appLocationManager';
import { saveGuestMode_TO_LOCALDB } from '../../../../Management/App/appNavigationManager';
import { createStyles, createContainers } from './styles';


export default function UtilityCitySelectionScreen(props: CitySelectionScreenProps) {
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyles(colors, font), [colors])
    const containers = useMemo(() => createContainers(colors), [colors])
    const dispatch = useDispatch();
    const [selectedCity, setSelectedCity] = useState('');
    const [districList, setDistricList] = useState([]);

    useEffect(() => {
        fetch('http://admin.rasodaa.com/api/getcities/33?start=0&length=all&search=a', {
            method: 'GET'
        }).then(res => res.json()).then(res => {
            if (res.status == 200) {
                setDistricList(res.cities.cities);
                //console.log(res)
            }
        }).catch(err => {
            console.log(err)
        })

    }, [])

    return (
        <View style={containers.main}>
            <Text style={styles.header}>Select City</Text>
            <FlatGrid
                itemDimension={130}
                data={districList}
                renderItem={({ item }) => (
                    <TouchableOpacity style={styles.cities_button}
                        onPress={() => {

                            dispatch(setLocation({
                                currentLocationName: item.name,
                                currentLocationAddress: 'Rajasthan, India'
                            }));
                            dispatch(saveGuestMode_TO_LOCALDB(true));
                            props.navigation.navigate('UserHomeScreen');

                        }}>
                        <TitleMedium text={item.name} clr={colors.OnSurface} align={'center'} />
                    </TouchableOpacity>
                )}
            />

        </View>
    )
}
