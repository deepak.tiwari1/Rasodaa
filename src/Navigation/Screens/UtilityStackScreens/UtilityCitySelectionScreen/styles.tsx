import { StyleSheet } from 'react-native'
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createContainers = (colors: Colors) => StyleSheet.create({
    main: {
        backgroundColor: colors.Background,
        flex: 1,
        paddingTop: 40
    }
})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    header: {
        fontSize: font.HeadlineFont.Medium.fontSize,
        fontFamily: font.HeadlineFont.Medium.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center',
        marginVertical: 10
    },
    cities_button: {
        paddingVertical: 15,
        backgroundColor: colors.Surface,
        borderRadius: 10,
        borderColor: colors.Ouline,
        borderStyle: 'solid',
        borderWidth: 1,
        elevation: 5
    }
})