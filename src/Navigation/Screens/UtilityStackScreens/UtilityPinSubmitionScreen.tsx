import { View, Text, Alert, ToastAndroid, Keyboard, TextInput, InteractionManager } from 'react-native';
import React, { useEffect, useRef, useState } from 'react'
import { PinSubmitionScreenProps } from '../UtilityScreensPropTypes'
import styled from 'styled-components/native'
import { useSelector } from 'react-redux';
import { RootState } from '../../../Management';
import { TitleLarge, HeadlineMedium, LableLarge, TitleSmall, LableMedium, TitleMedium } from '../../../Component/Utility/TextComponent';
import * as LocalAuthentication from 'expo-local-authentication';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
//import { FontAwesome5 } from '@expo/vector-icons';

export default function UtilityPinSubmitionScreen(props: PinSubmitionScreenProps) {
    const Pin = useSelector((state: RootState) => state.apppinandfingerprintmanager);
    const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
    const PinInputRef = useRef(null)
    const biometricsAuth = async () => {
        const compatible = await LocalAuthentication.hasHardwareAsync()
        if (!compatible) throw 'This device is not compatible for biometric authentication'
        const enrolled = await LocalAuthentication.isEnrolledAsync()
        if (!enrolled) throw `This device doesn't have biometric authentication enabled`
        try {
            const result = await LocalAuthentication.authenticateAsync({
                promptMessage: 'Verify your identity',
                fallbackLabel: 'Enter PIN',

            })
            if (result.success) {
                props.navigation.navigate('UserHomeScreen');
            }
            else if (!result.success) {
                PinInputRef.current.focus()
            }
            return
        }
        catch (error) {
            console.log(error);
        }
    }

    // useEffect(() => {
    //     //alert(Pin.appPinLogin)
    //     if (Pin.appFingerprintLogin) {
    //         biometricsAuth();
    //     }

    // }, [])
    return (
        <Container bclr={colors.Background}>
            <TopContainer>
                <View style={{ marginBottom: 30 }}>
                    <TitleMedium text={'Forgot PIN'} clr={colors.Primary} align={'right'} />
                </View>
                <View style={{ marginBottom: 30 }}>
                    <HeadlineMedium text={'Hii User'} clr={colors.OnBackground} align={'left'} />
                    <LableLarge text={'Welcome Back'} clr={colors.OnBackground} align={'left'} />
                </View>
                <PinBox>
                    <TitleMedium text={'Enter your 6-digit PIN'} clr={colors.OnBackground} align={'left'} />
                    <View style={{ flexDirection: 'row', borderBottomWidth: 3, borderBottomColor: colors.Primary, marginTop: 10 }}>
                        <PinInput
                            ref={PinInputRef}
                            clr={colors.OnBackground}
                            boclr={colors.Primary}
                            keyboardType="number-pad"
                            maxLength={6}
                            secureTextEntry={true}
                            //autoFocus={true}
                            onChange={(e) => {

                                if (e.nativeEvent.text.length == 6) {
                                    //setPin(e.nativeEvent.text)

                                    if (e.nativeEvent.text == Pin.appPinLogin) {
                                        props.navigation.navigate('UserHomeScreen')
                                    }
                                    else {
                                        ToastAndroid.show('Wrong Pin', ToastAndroid.LONG)
                                    }
                                }
                            }}
                            onSubmitEditing={() => {
                                props.navigation.navigate('UserHomeScreen');
                            }}
                        />

                        {Pin.appFingerprintLogin ?
                            <PinButton onPress={() => { biometricsAuth() }}>
                                <FontAwesome5 name="fingerprint" size={24} color="black" />
                            </PinButton> : []
                        }
                    </View>
                </PinBox>
            </TopContainer>
            <BottomContainer>
                <LableMedium text={'Signing in as user@email.com'} clr={colors.OnBackground} align={'left'} />
                <SwitchAccount onPress={() => {
                    props.navigation.navigate('AppLoginScreen');
                }}>
                    <LableLarge text={'Switch Account?'} clr={colors.Secondry} align={'left'} />
                </SwitchAccount>
            </BottomContainer>
        </Container>
    )
}

const Container = styled.View`
    flex: 1;
    backgroundColor: ${props => props.bclr};
    paddingVertical: 20px;
    paddingHorizontal: 20px;
    justifyContent: space-between;
`
const TopContainer = styled.View`
    //paddingVertical: 20px;
    paddingHorizontal: 20px;
`
const BottomContainer = styled.View`
    //flex-Direction: row;
    judtifyContent: flex-start;
    alignItems: flex-end;
    paddingVertical: 20px;
    paddingHorizontal: 20px;
`

const PinBox = styled.View`
    
`
const PinInput = styled.TextInput`
    fontSize: 25px;
    width: 87%;
    color:${props => props.clr};

`
const PinButton = styled.TouchableOpacity`
    paddingHorizontal: 20px;
    paddingVertical: 10px;
`

const SwitchAccount = styled.TouchableOpacity`
    marginVeritcal: 20px;
`