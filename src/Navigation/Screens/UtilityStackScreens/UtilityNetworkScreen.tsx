import { View, Text, StyleSheet, Image } from 'react-native'
import React from 'react'
import * as GS from '../../../Management/Utility/GlobalStyles'
import type { NetworkScreenProps } from '../UtilityScreensPropTypes'
import { TitleLarge } from '../../../Component/Utility/TextComponent';

export default function UtilityNetworkScreen({ navigation, route }: NetworkScreenProps) {
  return (
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'flex-end' }}>
      <View style={styles.container}>
        <Image source={{ uri: 'https://media.istockphoto.com/vectors/signal-forbidden-symbol-dark-sticker-vector-id1310560704?k=20&m=1310560704&s=612x612&w=0&h=KprftoCCuriYxQFqq3Ht161ciUWJ8zatzeJ6M72eHlg=' }} style={styles.image} />
        <TitleLarge text={'No Internet'} align="center" clr={'black'} />
        <TitleLarge text={'Please check your internet connection'} align="center" clr={'black'} />
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {

    backgroundColor: 'white',
    width: '100%', height: '100%',
    paddingHorizontal: 20,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100, height: 100,
  }
})