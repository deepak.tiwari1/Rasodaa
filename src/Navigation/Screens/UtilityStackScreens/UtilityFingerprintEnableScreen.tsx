import { View, Text, Pressable } from 'react-native'
import React, { useEffect } from 'react'
import styled from 'styled-components/native'
import { TitleMedium, TitleSmall, TitleLarge } from '../../../Component/Utility/TextComponent';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../Management';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import { EnableFingerprint } from '../../../Management/App/appPinandFingerprintManager';
import type { FingerprintSubmitionScreenProps } from '../UtilityScreensPropTypes';
import LottieView from 'lottie-react-native';
import { FINGERPRINTSUCCESS, FINGERPRINTREQUEST } from '../../../assets';
import * as LocalAuthentication from 'expo-local-authentication';

export default function UtilityFingerprintEnableScreen(props: FingerprintSubmitionScreenProps) {
    const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
    const dispatch = useDispatch();
    const [isfingerprintVerify, setIsFingerprintVerify] = React.useState(false);
    const AnimaRef = React.useRef<LottieView>(null);

    const handlefingerprint = async () => {
        const hasHardware = await LocalAuthentication.hasHardwareAsync();
        if (hasHardware) {
            const isEnrolled = await LocalAuthentication.getEnrolledLevelAsync();
            if (isEnrolled) {
                const result = await LocalAuthentication.authenticateAsync();
                if (result.success) {
                    dispatch(EnableFingerprint());
                    setIsFingerprintVerify(true);
                    AnimaRef.current?.play();
                }
                else if (!result.success) {
                    alert('Fingerprint Authentication Failed');
                }

            }
        }
    }

    return (
        <Container>
            {isfingerprintVerify ?
                <View>
                    <LottieView ref={AnimaRef} source={FINGERPRINTSUCCESS} loop={false} style={{
                        width: 300,
                        height: 300,
                    }} />
                    <TitleLarge text={'Fingerprint Enabled'} align={'center'} clr={'black'} />
                </View>
                :
                <LottieView source={FINGERPRINTREQUEST} autoPlay={true} loop={true} style={{
                    width: 300,
                    height: 300,
                }} />}
            <EnableFingerprintButton
                bclr={colors.Primary}
                onPress={() => {
                    if (isfingerprintVerify == false) { handlefingerprint() }
                    else if (isfingerprintVerify == true) {
                        props.navigation.navigate('UserHomeScreen');
                    }
                }}
            >
                {isfingerprintVerify == false ?
                    <TitleSmall text={'Enable Fingerprint'} clr={colors.OnPrimary} align={'center'} /> : <TitleSmall text={'Go to Home'} clr={colors.OnPrimary} align={'center'} />}
            </EnableFingerprintButton>
        </Container>
    )
}

const Container = styled.View`
    flex: 1;
    justify-content: space-between;
    align-items: center;
    padding-vertical: 40px;
`

const EnableFingerprintButton = styled.TouchableOpacity`
    paddingVertical: 10px;
    paddingHorizontal: 50px;
    backgroundColor: ${props => props.bclr};
    border-radius: 10px;
`