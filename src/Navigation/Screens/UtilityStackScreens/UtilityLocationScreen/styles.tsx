import { StyleSheet } from 'react-native'
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createContainers = (colors: Colors) => StyleSheet.create({
    addresssheet_Container: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.Background
    },
    adresssheet_addressTypeContainer: {
        width: '100%',
        paddingVertical: 5,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    adresssheet_addressTypeButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: 10,
    },
    adresssheet_addressFormContainer: {
        width: '100%',
        paddingHorizontal: 10
    }
})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    addresssheet_Header: {
        fontSize: font.TitleFont.Large.fontSize,
        fontFamily: font.TitleFont.Large.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center',
        marginVertical: 10,
        // borderBottomColor: colors.Outline,
        // borderStyle: "solid",
        // borderBottomWidth: 1

    },
    adresssheet_addressTypesubheader: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground,

    },
    adresssheet_addressTypeButton_active: {
        borderColor: colors.Secondry,
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 2,
        paddingHorizontal: 10,
        paddingVertical: 2,
        marginLeft: 8,
        elevation: 5,
        backgroundColor: colors.Surface

    },
    adresssheet_addressTypeButton_inactive: {
        borderColor: colors.SecondryColorShades.SC_500,
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 2,
        marginLeft: 8,
        elevation: 5,
        backgroundColor: colors.Surface

    },
    adresssheet_addressTypeButtonText: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center'
    },
    adresssheet_addressFormInput: {
        borderColor: colors.Outline,
        borderStyle: 'solid',
        borderWidth: 1,
        height: 50,
        width: '100%',
        paddingHorizontal: 20,
        borderRadius: 20,
        marginVertical: 10,
        color: colors.OnBackground

    },
    adresssheet_addressFormsubmitButton: {
        backgroundColor: colors.Primary,
        borderRadius: 20,
        marginVertical: 10,
        paddingVertical: 13,
    },
    adresssheet_addressFormsubmitButtonText: {
        alignSelf: 'center',
        fontFamily: font.BodyFont.Large.fontFamily,
        fontSize: font.BodyFont.Large.fontSize,
        color: colors.OnPrimary
    }
})