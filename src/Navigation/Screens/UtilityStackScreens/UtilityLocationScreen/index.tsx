import { Dimensions, ScrollView, TextInput, View, Text, StyleSheet, TouchableOpacity, Pressable } from 'react-native'
import React, { useEffect, useState, useMemo } from 'react'
//import { MaterialIcons, FontAwesome } from '@expo/vector-icons';
import * as GS from '../../../../Management/Utility/GlobalStyles';
import Geolocation from 'react-native-geolocation-service';
import { RootState } from '../../../../Management';
import { useDispatch, useSelector } from 'react-redux';
import { setLocationGranted, setLocation, setCustomLocationstatus } from '../../../../Management/App/appLocationManager';
import styled from 'styled-components/native';
import { addAddress } from '../../../../Management/User/userAddressManager';
import type { LocationScreenProps } from '../../UtilityScreensPropTypes';
import { TitleMedium, HeadlineMedium } from '../../../../Component/Utility/TextComponent';
import { createContainers, createStyles } from './styles';

export default function UtilityLocationScreen(props: LocationScreenProps) {
  const dispatch = useDispatch();
  const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
  const containers = useMemo(() => createContainers(colors), [colors])
  const styles = useMemo(() => createStyles(colors, font), [colors, font])

  const addlist = useSelector((state: RootState) => state.useraddressprovider);
  const navigation = useSelector((state: RootState) => state.navigationmanager);

  // async function getlocation() {
  //   let { status } = await Geolocation.requestForegroundPermissionsAsync();
  //   if (status !== 'granted') {
  //     props.navigation.navigate('UtilityLocationScreen');
  //     dispatch(setLocationGranted(false));

  //   }
  //   else {
  //     let location = await Location.getCurrentPositionAsync({});
  //     let locationName = await Location.reverseGeocodeAsync(
  //       { latitude: location.coords.latitude, longitude: location.coords.longitude });
  //     let address = {
  //       currentLocationName: locationName[0].district,
  //       currentLocationAddress: locationName[0].district + ', ' + locationName[0].city + ', ' + locationName[0].region + ' ' + locationName[0].postalCode
  //     }

  //     dispatch(setLocation(address));
  //     dispatch(setLocationGranted(true));

  //     props.navigation.navigate('UserHomeScreen');

  //   }

  // }

  let [addressType, setaddressType] = useState('Home');
  let [firstLine, setfirstLine] = useState("");
  let [secondLine, setsecondLine] = useState("");
  let [city, setcity] = useState("");
  let [state, setstate] = useState("");
  let [zip, setzip] = useState("");

  return (
    <View style={containers.addresssheet_Container}>
      <Text style={styles.addresssheet_Header}>Enter complete address</Text>
      <View style={containers.adresssheet_addressTypeContainer}>
        <Text style={styles.adresssheet_addressTypesubheader}>Save address as</Text>
        <View style={containers.adresssheet_addressTypeButtonsContainer}>
          <Pressable
            style={(addressType == 'Home' ? styles.adresssheet_addressTypeButton_active : styles.adresssheet_addressTypeButton_inactive)}
            onPress={() => setaddressType('Home')}
          >
            <Text style={styles.adresssheet_addressTypeButtonText}>Home</Text>
          </Pressable>
          <Pressable
            onPress={() => setaddressType('Work')}
            style={(addressType == 'Work' ? styles.adresssheet_addressTypeButton_active : styles.adresssheet_addressTypeButton_inactive)}>
            <Text style={styles.adresssheet_addressTypeButtonText}>Work</Text>
          </Pressable>
        </View>
      </View>
      <View style={containers.adresssheet_addressFormContainer}>
        <TextInput
          placeholder='Enter House no, floor no'
          maxLength={40}
          autoComplete="street-address"
          placeholderTextColor={colors.BackgroundColorShades.BC_400}
          onChangeText={(text) => { setfirstLine(text) }}
          style={styles.adresssheet_addressFormInput} />
        <TextInput
          placeholder='Enter locality'
          maxLength={40}
          autoComplete="postal-address-locality"
          placeholderTextColor={colors.BackgroundColorShades.BC_400}
          onChangeText={(text) => { setsecondLine(text) }}
          style={styles.adresssheet_addressFormInput} />
        <TextInput
          placeholder='Enter city'
          maxLength={20}
          autoComplete="postal-address-region"
          placeholderTextColor={colors.BackgroundColorShades.BC_400}
          onChangeText={(text) => { setcity(text) }}
          style={styles.adresssheet_addressFormInput} />
        <TextInput
          placeholder='Enter state'
          maxLength={20}
          autoComplete="postal-address-country"
          placeholderTextColor={colors.BackgroundColorShades.BC_400}
          onChangeText={(text) => { setstate(text) }}
          style={styles.adresssheet_addressFormInput} />
        <TextInput
          placeholder='Enter zip code'
          maxLength={6}
          autoComplete="postal-code"
          keyboardType="numeric"
          placeholderTextColor={colors.BackgroundColorShades.BC_400}
          onChangeText={(text) => { setzip(text) }}
          style={styles.adresssheet_addressFormInput} />
        <TouchableOpacity
          style={styles.adresssheet_addressFormsubmitButton}
          onPress={() => {
            dispatch(addAddress({
              data: {
                addressId: addlist.length + 1,
                is_default_address: true,
                firstLine: firstLine,
                secondLine: secondLine,
                city: city,
                state: state,
                zip: zip,
                addresstype: addressType
              },
              is_default_address: true,
              ui_data: {
                is_edit_address: false,

              }
            }));
            dispatch(setLocation({
              currentLocationName: addressType,
              currentLocationAddress: firstLine + ", " + secondLine + ', ' + city + ', ' + zip,

            }));
            dispatch(setCustomLocationstatus(true)),
              props.navigation.navigate('UserHomeScreen')
          }}
        >
          <Text style={styles.adresssheet_addressFormsubmitButtonText}>Save address</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

