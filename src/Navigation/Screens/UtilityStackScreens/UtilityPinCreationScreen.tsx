import { View, Text, Alert, ToastAndroid, Keyboard, TextInput, TouchableOpacity } from 'react-native';
import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components/native'
//import FingerprintScanner from 'react-native-fingerprint-scanner';
import * as LocalAuthentication from 'expo-local-authentication';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../Management';
import { TitleLarge, HeadlineMedium, LableLarge, TitleSmall, LableMedium, TitleMedium } from '../../../Component/Utility/TextComponent';
//import { FontAwesome5 } from '@expo/vector-icons';
import { PinCreationScreenProps } from '../UtilityScreensPropTypes'
import { setappfingerprintdata, setappPindata } from '../../../Management/App/appPinandFingerprintManager';

export default function UtilityPinCreationScreen(props: PinCreationScreenProps) {
    const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
    const Pin = useSelector((state: RootState) => state.apppinandfingerprintmanager);
    const dispatch = useDispatch();
    const [enterPin, setEnterPin] = useState('');
    const [reEnterPin, setReEnterPin] = useState('');

    const handlefingerprint = async () => {
        const compatible = await LocalAuthentication.hasHardwareAsync();
        if (compatible) {
            dispatch(setappfingerprintdata(true));
        }
        else {
            dispatch(setappfingerprintdata(false));
        }
    }

    useEffect(() => {
        handlefingerprint();
    }, []);

    return (
        <Container >
            <TopContainer>

                <View style={{ marginBottom: 30 }}>
                    <HeadlineMedium text={'Hii User'} clr={'black'} align={'left'} />
                    <LableLarge text={'Welcome Pin Creation'} clr={'black'} align={'left'} />
                </View>
                <PinBox>
                    <TitleMedium text={'Enter your 6-digit PIN'} clr={colors.BackgroundColorShades.BC_400} align={'left'} />
                    <View style={{ flexDirection: 'row', borderBottomWidth: 3, borderBottomColor: colors.PrimaryColorShades.PC_400, marginTop: 10 }}>
                        <PinInput
                            clr={colors.OnSurface}
                            keyboardType="number-pad"
                            maxLength={6}
                            autoFocus={true}
                            secureTextEntry={true}
                            placeholder={'------'}
                            placeholderTextColor={colors.BackgroundColorShades.BC_400}
                            onChange={(e) => {
                                if (e.nativeEvent.text.length == 6) {
                                    setEnterPin(e.nativeEvent.text);
                                }
                            }}
                        />

                    </View>
                </PinBox>
                <PinBox>
                    <TitleMedium text={'Re-Enter your 6-digit PIN'} clr={colors.BackgroundColorShades.BC_400} align={'left'} />
                    <View style={{ flexDirection: 'row', borderBottomWidth: 3, borderBottomColor: colors.PrimaryColorShades.PC_400, marginTop: 10 }}>
                        <PinInput
                            clr={colors.OnSurface}
                            keyboardType="number-pad"
                            maxLength={6}
                            secureTextEntry={true}
                            placeholder={'------'}
                            placeholderTextColor={colors.BackgroundColorShades.BC_400}
                            onChange={(e) => {
                                if (e.nativeEvent.text.length == 6) {
                                    setReEnterPin(e.nativeEvent.text);
                                }
                            }}
                        />
                    </View>
                </PinBox>

            </TopContainer>
            <BottomContainer>
                <SubmitButton
                    bclr={colors.Primary}
                    onPress={() => {
                        if (enterPin == reEnterPin) {
                            dispatch(setappPindata(enterPin));
                            if (Pin.appFingerprintLogin) {
                                props.navigation.navigate('UtilityFingerprintEnableScreen');
                            }
                            else {
                                props.navigation.navigate('UtilityPinSubmitionScreen');

                            }
                        }
                        else (
                            ToastAndroid.show('Pin Not Matching', ToastAndroid.SHORT)
                        )
                    }}>
                    <TitleSmall text={'Create PIN'} clr={colors.OnPrimary} align={'center'} />
                </SubmitButton>
            </BottomContainer>
        </Container>
    )
}


const Container = styled.View`
    flex: 1;
    paddingVertical: 20px;
    paddingHorizontal: 20px;
    justifyContent: space-between;
`
const TopContainer = styled.View`
    paddingVertical: 30px;
    paddingHorizontal: 20px;
`
const BottomContainer = styled.View`
    //flex-Direction: row;
    judtifyContent: space-between;
    alignItems: center;
    paddingVertical: 20px;
    paddingHorizontal: 20px;
`

const PinBox = styled.View`
    marginVertical: 10px;
`
const PinInput = styled.TextInput`
    fontSize: 25px;
    width: 87%;
    color: ${props => props.clr};

`
const PinButton = styled.TouchableOpacity`
    paddingHorizontal: 20px;
    paddingVertical: 10px;
`


const SubmitButton = styled.TouchableOpacity`
    marginVertical: 20px;
    paddingVertical: 8px;
    paddingHorizontal: 20px;
    backgroundColor: ${props => props.bclr};
    borderRadius: 8px;
`