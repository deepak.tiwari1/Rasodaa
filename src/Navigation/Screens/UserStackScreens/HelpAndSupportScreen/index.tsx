import React from 'react';
import { View, Text } from 'react-native';
import type { UserHelpAndSupportScreenProps } from '../../UserScreensPropTypes';
import { Createstyles } from './styles';
import { TitleLarge, TitleSmall } from '../../../../Component/Utility/TextComponent';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../Management';

export default function UserHelpAndSupportScreen(props: UserHelpAndSupportScreenProps) {
	const styles = React.useMemo(() => Createstyles(), []);
	const { colors } = useSelector((state: RootState) => state.appapperiencemanager);
	return (
		<View style={styles.container}>
			<TitleLarge text={'Contect US'} align={'center'} clr={colors.OnBackground} />
			<TitleSmall text={'+91 XXXXXXXXXX'} align={'center'} clr={colors.BackgroundColorShades.BC_400} />
		</View>
	)
}

