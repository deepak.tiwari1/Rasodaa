import { View, Text, Alert } from 'react-native'
import React, { useState, useMemo, useRef, useEffect } from 'react'
import styled from 'styled-components/native'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../../Management';
import { setappLogindata, saveGuestMode_TO_LOCALDB } from '../../../../Management/App/appNavigationManager'
import { DisplayLarge, TitleLarge, LableLarge, TitleMedium, LableMedium, DisplayMedium, DisplaySmall, HeadlineLarge } from '../../../../Component/Utility/TextComponent';
//import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import type { MobileNumberEntryScreenProps } from '../../AppScreensPropTypes';
import { CreateStyle, Button } from './styles'
import { setPassword, setPhoneNumber } from '../../../../Management/App/userRegisterationManager';
import { BASE_URL } from '../../../../Constant';
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import { GoogleSignin } from '@react-native-google-signin/google-signin';


export default function MobileNumberEntryScreen({ navigation, route }: MobileNumberEntryScreenProps) {
    const { colors, font, fontSize } = useSelector((state: RootState) => state.appapperiencemanager);
    const dispatch = useDispatch();
    const styles = useMemo(() => CreateStyle(), []);
    const [OldPassword, setOldPassword] = useState('');
    const [EnterPassword, setEnterPassword] = useState('');
    const [ReEnterPassword, setReEnterPassword] = useState('');
    const [RasodaaACCESSTOKEN, setRasodaaACCESSTOKEN] = useState('');
    useEffect(() => {
        getRasodaaAccessToken(setRasodaaACCESSTOKEN)
    }, [])
    return (
        <View style={styles.container}>

            <HeadlineLarge text={'Change Your Login Password'} align={'center'} clr={colors.OnBackground} />
            <View style={styles.form}>
                <LableLarge text={'Enter Old password'} clr={colors.OnSurface} align={'center'} />
                <Input
                    bclr={colors.Outline}
                    clr={colors.OnSurface}
                    fz={font.LableFont.Large.fontSize + fontSize}
                    fzz={font.LableFont.Large}
                    style={{ marginVertical: 10, fontSize: 17 }}
                    placeholder="+91 ----------"
                    maxLength={8}
                    onChangeText={(text) => { setOldPassword(text); }}
                >
                </Input>
                <LableLarge text={'Enter New password'} clr={colors.OnSurface} align={'center'} />
                <Input
                    bclr={colors.Outline}
                    clr={colors.OnSurface}
                    fz={font.LableFont.Large.fontSize + fontSize}
                    fzz={font.LableFont.Large}
                    style={{ marginVertical: 10, fontSize: 17 }}
                    placeholder="P@ssw0rd"
                    maxLength={8}
                    secureTextEntry={true}
                    onChangeText={(text) => { setEnterPassword(text); }}
                >
                </Input>
                <LableLarge text={'Re-enter New password'} clr={colors.OnSurface} align={'center'} />
                <Input
                    bclr={colors.Outline}
                    clr={colors.OnSurface}
                    fz={font.LableFont.Large.fontSize + fontSize}
                    fzz={font.LableFont.Large}
                    style={{ marginVertical: 10, fontSize: 17 }}
                    placeholder="P@ssw0rd"
                    maxLength={8}
                    secureTextEntry={true}
                    onChangeText={(text) => { setReEnterPassword(text); }}
                >
                </Input>
                <Button bclr={colors.Primary}
                    onPress={async () => {
                        if (OldPassword.length === 8 && EnterPassword.length === 8 && ReEnterPassword.length === 8 && EnterPassword === ReEnterPassword) {

                            try {
                                // console.log('OldPassword ' + OldPassword);
                                // console.log('EnterPassword ' + EnterPassword);
                                // console.log('RasodaaACCESSTOKEN ' + RasodaaACCESSTOKEN);
                                fetch(BASE_URL + 'customer/changepassword', {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'Authorization': 'Bearer ' + RasodaaACCESSTOKEN,
                                    },
                                    body: JSON.stringify({
                                        old_password: OldPassword,
                                        password: EnterPassword,
                                    })
                                }).then(res => res.json()).then((response) => {

                                    if (response.status == 200) {
                                        const signOut = async () => {
                                            try {
                                                await GoogleSignin.signOut();

                                            } catch (error) {
                                                console.error(error);
                                            }
                                        };
                                        dispatch(setappLogindata(false));
                                        navigation.navigate('AppLoginScreen');
                                    }
                                    else {
                                        console.log('error:' + response.message);
                                    }
                                }).catch(error =>
                                    alert(error)
                                )
                            }
                            catch (err) {
                                alert("text:" + `Error: ${err.message}`);
                            }
                        }
                    }}
                >
                    <TitleMedium text={'Change Password'} align={'center'} clr={colors.OnPrimary} />
                </Button>
            </View>
        </View>
    )
}


const Page = styled.View`
	flex: 1;
    justify-content: center;
    paddingHorizontal: 20px;
	backgroundColor: ${props => props.clr};

`
const LoginSurface = styled.View`
	
    width:100%;
    backgroundColor:${props => props.clr};
	//elevation: 3;
    border-radius:10px;
	//border-width:1px;
	border-color:${props => props.bclr};
	border-style:solid;
	paddingHorizontal:40px;
	paddingVertical:40px;
	justifyContent:space-between;
	

`

const Label = styled.Text`
	color:${props => props.clr};
	font-size:${props => props.fz}px;
	font-family:${props => props.fzz.fontFamily};

`

const Input = styled.TextInput`
	width:100%;
	height:50px;
	border-radius:30px;
	border-width:1px;
	border-color:${props => props.bclr};
	border-style:solid;
	paddingHorizontal:20px;
	marginTop:7px;
	font-size:${props => props.fz}px;
	font-family:${props => props.fzz.fontFamily};
	color:${props => props.clr};
`


const ModeButton = styled.Pressable`
    flex-direction:row;
    justify-content:center;
    align-items:center;
    paddingHorizontal:10px;
    paddingVertical:10px;
    border-radius:30px;
    backgroundColor:${props => props.clr};
	marginTop:5px;
    
`
const ModeButtonText = styled.Text`
	color:${props => props.clr};
	font-size:${props => props.fz}px;
	text-align:center;
`

const LoginText = styled.Text`
	font-size:${props => props.fz}px;
	font-weight:${props => props.fzz.fontWeight};
	fontFamily:${props => props.fzz.fontFamily};
	color:${props => props.clr};
	text-align:center;
	width:100%;

`
const LoginOptionButton = styled.TouchableOpacity`
    flex-direction:row;
    justify-content:center;
    align-items:center;
    paddingHorizontal:10px;
    paddingVertical:10px;
	marginTop:5px;
	border-radius:30px;
	border-width:1px;
	border-color:${props => props.boclr};
	border-style:solid;
	width:49%;
    
`