import { StyleSheet } from "react-native";
import styled from 'styled-components/native';


export const CreateStyle = () => StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 30,
    },
    form: {
        marginVertical: 30,
        paddingHorizontal: 20,
    }
})

export const Button = styled.TouchableOpacity`
    backgroundColor: ${props => props.bclr};
    borderRadius: 30px;
    paddingVertical: 10px;
`

