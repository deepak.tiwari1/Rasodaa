import { StyleSheet, Dimensions } from "react-native";
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager';

const { width, height } = Dimensions.get('window');

export const createContainers = (colors: Colors) => StyleSheet.create({

})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    headlingText: {
        fontSize: 18,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground,
        // fontWeight: 'bold',
        alignSelf: 'center',
        marginVertical: 20,
    }
})