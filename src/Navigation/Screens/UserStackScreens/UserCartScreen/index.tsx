import { View, Text, SafeAreaView, StyleSheet, Pressable, Image, ScrollView, Dimensions } from 'react-native';
import React, { useMemo } from 'react';
import { RootState } from '../../../../Management';
import { setDefaultAddress, setEditAddress } from '../../../../Management/User/userAddressManager';
import { useDispatch, useSelector } from 'react-redux';
import { add } from 'date-fns';
import { saveActivePackageData_TO_LOCALDB, type activepackageData } from '../../../../Management/User/userActivePackageManager';
import { UserCartScreenProps } from '../../UserScreensPropTypes';
import { createStyles, createContainers } from './styles'

export default function UserCartScreen({ navigation, route }: UserCartScreenProps) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
	const styles = useMemo(() => createStyles(colors, font), [colors, font])
	const containers = useMemo(() => createContainers(colors), [colors])
	return (
		<ScrollView style={containers.main}>
			<Text style={styles.headerText}>My Cart</Text>
			<View>

			</View>
		</ScrollView>
	)

}
