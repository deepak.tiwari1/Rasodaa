import React, { useEffect, useState, useMemo } from 'react';
import { ScrollView, View, Text, Pressable, Image } from 'react-native';
import { BASE_URL, SLIDER_CHEF } from '../../../../Constant';
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import { createContainers, createStyles } from './styles'
import type { UserFavouriteCheffScreenProps } from '../../UserScreensPropTypes';
import { useSelector } from 'react-redux';
import { StyleSheet, Dimensions, ImageBackground } from 'react-native';
import { RootState } from '../../../../Management';
import { HeadlineMedium } from '../../../../Component/Utility/TextComponent';
import AntDesign from 'react-native-vector-icons/AntDesign';
type FavCheffDataType = {
	"chef_id": number,
	"customer_id": number,
	"id": number,
	"kitchen_name": string,
	"name": string,
	"last_name": string,
	"unique_id": string,
	"image": string,
	"gender": string,
	"food_preference": string,
	"spicy_level": string,
	"speciality": string,
	"description": string,
	"active": string,
	"max_deliveries": string,
	"lunch_preparation_time": string,
	"dinner_preparation_time": string,
	"verified": number,
	"certified": string
}

export default function UserFavouriteCheffScreen(props: UserFavouriteCheffScreenProps) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const [FavCheffList, setFavCheffList] = useState<FavCheffDataType[]>([]);
	const styles = useMemo(() => createStyles(colors, font), [colors])
	const containers = useMemo(() => createContainers(colors), [colors])

	const fetchdata = (accessToken: string) => {
		fetch(BASE_URL + 'customer/favourites', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + accessToken
			}
		}).then(res => res.json()).then(res => {
			if (res.status == 200) {

				setFavCheffList(res.favourites.favourites)
			}
			else {
				alert(res.status)
			}
		})
	}

	useEffect(() => {
		getRasodaaAccessToken(fetchdata)
	}, [])

	return (
		<ScrollView style={containers.main}>
			{/* <HeadlineMedium text={'My Cheffs'} clr={colors.OnBackground} align={'center'} /> */}
			<Text style={styles.headertext}>My Favourites</Text>
			<View style={[styles.results]}>
				{FavCheffList.map((cheff, index) => {
					return (
						<Pressable key={index} style={[styles.result]} onPress={() => {
							props.navigation.navigate('UserCheffProfileScreen', {
								cheffId: FavCheffList[index].chef_id,
								cheffName: FavCheffList[index].name + ' ' + FavCheffList[index].last_name,
								cheffImage: FavCheffList[index].image

							})
						}}
						>
							<View style={[styles.result_image_container]}>
								<Image style={[styles.result_image]} source={{ uri: SLIDER_CHEF + cheff.image }} />
								{(cheff.food_preference == 'veg_only') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkey.com/png/full/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png' }} />
									</View> : []
								}
								{(cheff.food_preference == 'nonveg_only') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkit.com/png/full/257-2579552_non-veg-symbol-non-veg-symbol-png.png' }} />
									</View> : []
								}
								{(cheff.food_preference == 'both') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkey.com/png/full/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png' }} />
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://www.pngkit.com/png/full/257-2579552_non-veg-symbol-non-veg-symbol-png.png' }} />
									</View> : []
								}
								{(cheff.food_preference == 'eggetarian') ?

									<View style={[styles.result_foodtype]}>
										<Image style={[styles.result_foodtype_image]} source={{ uri: 'https://foofyfood.tamss.biz/assets/img/slider/1632896487ojp8VXLng4.jpg' }} />
									</View> : []
								}
							</View>
							<View style={[styles.result_details_container]}>
								<Text style={[styles.result_heading]}>{cheff.name} {cheff.last_name}</Text>
								{/* <View style={{ flexDirection: 'row' }}>
									{(cheff.cuisine_slug != null) ?
										(cheff.cuisine_title.split(',').splice(0, 2).map((cuisine, index) => {
											return <Text key={index} style={[styles.result_subheading]}>{cuisine} </Text>
										}
										)) : <Text style={[styles.result_subheading]}>No Cuisine</Text>
									}
								</View> */}
								<View style={[styles.result_rating_container]}>
									<AntDesign name={'star'} color={'rgb(250,156,28)'} size={16} />
									<Text style={[styles.result_rating_text]}>5</Text>
								</View>
								<View style={[styles.result_foottime]}>
									<Text style={[styles.result_foodtime_text]}>Lunch</Text>
									<Text style={[styles.result_foodtime_text]}>Dinner</Text>
								</View>
							</View>

						</Pressable>
					)
				})}
			</View>

		</ScrollView>
	)
}



