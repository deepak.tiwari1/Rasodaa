import React from 'react';
import { HomeBottomTabManager } from '../../Tabs';
import type { UserHomeScreenProps } from '../UserScreensPropTypes';

export default function UserHomeScreen(props: UserHomeScreenProps) {

    return (
        <HomeBottomTabManager {...props} />
    )
}
