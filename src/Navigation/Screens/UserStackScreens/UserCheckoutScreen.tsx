import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { UserCheckoutScreenProps } from '../UserScreensPropTypes';
import RazorpayCheckout from 'react-native-razorpay';
import { TitleMedium } from '../../../Component/Utility/TextComponent';
//import { initializePayment } from 'react-native-upi-payment';

export default function UserCheckoutScreen({ navigation, route }: UserCheckoutScreenProps) {
	return (
		<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
			<TitleMedium text={'App Checkout'} clr={'black'} align={'center'} />
			<TouchableOpacity
				style={{ backgroundColor: '#00bcd4', padding: 10, borderRadius: 10 }}
				onPress={() => {
					var options = {
						description: 'Credits towards consultation',
						image: 'https://i.imgur.com/3g7nmJC.png',
						currency: 'INR',
						key: 'rzp_test_k6aKyOtPaQB439', // Your api key
						amount: '100',
						name: 'foo',
						prefill: {
							email: 'void@razorpay.com',
							contact: '9191919191',
							name: 'Razorpay Software'
						},
						theme: { color: '#F37254' }
					}
					RazorpayCheckout.open(options).then((data) => {
						// handle success
						alert(`Success: ${data.razorpay_payment_id}`);
					}).catch((error) => {
						// handle failure
						alert(`Error: ${error.code} | ${error.description}`);
					});
				}}
			>
				<TitleMedium text={'Pay'} clr={'black'} align={'center'} />
			</TouchableOpacity>
		</View>
	)
}
