import React, { useEffect, useState, useMemo } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Pressable, Image } from 'react-native';
import type { UserSettingsScreenProps } from '../../UserScreensPropTypes';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../../../Management'
import { saveAppLanguageData_TO_LOCALDB, language_Code, fontCode, saveFontSizeData_TO_LOCALDB, saveAppThemeData_TO_LOCALDB } from '../../../../Management/App/appApperienceManager';
import { HeadlineMedium, LableLarge, TitleLarge, TitleMedium, LableMedium } from '../../../../Component/Utility/TextComponent';
import SelectDropdown from 'react-native-select-dropdown'
import DatePicker from 'react-native-date-picker'
import Slider from '@react-native-community/slider';

import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';

import { createStyles, createContainers } from './styles'

let languages = {
	'en-IN': 'English',
	'hi-IN': 'Hindi',
	'mr-IN': 'Marwadi',
}

export default function UserSettingsScreen({ navigation, route }: UserSettingsScreenProps) {
	const { colors, fontSize, font, languageMode } = useSelector((state: RootState) => state.appapperiencemanager);
	const styles = useMemo(() => createStyles(colors, font), [colors, font])
	const containers = useMemo(() => createContainers(colors), [colors])

	const userData = useSelector((state: RootState) => state.userprofiledata);
	const dispatch = useDispatch();
	const [date, setDate] = useState(new Date(userData.dob))
	const [open, setOpen] = useState(false)
	const [openspicy, setOpenspicy] = useState(false)
	const [spicy_level_value, setSpicy_level_value] = useState((parseInt(userData.spicy_level).toString() == 'NaN' ? 1 : parseInt(userData.spicy_level)))
	const [foodpreference, setfoodpreference] = useState(userData.food_preference)
	let SpicyLevel = new Array(spicy_level_value).fill('')

	const [accessToken, setaccessToken] = useState('')
	useEffect(() => {
		getRasodaaAccessToken(setaccessToken)
	}, [])

	return (
		<View style={{ flex: 1, paddingVertical: 20, backgroundColor: colors.Background }}>
			{/* heading */}
			<View
				style={{ flexDirection: 'row', paddingBottom: 30, alignItems: 'center', paddingHorizontal: 20 }}>
				<Pressable onPress={navigation.goBack}>
					<AntDesign name='arrowleft' size={22} color={colors.OnBackground} style={{ marginRight: 28 }} />
				</Pressable>
				<TitleLarge text={'App settings'} align={'left'} clr={colors.OnBackground} />
			</View>
			{/* Top Settings */}
			<View style={{ borderTopColor: colors.BackgroundColorShades.BC_100, borderStyle: 'solid', borderTopWidth: 2 }}>
				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<Ionicons name="ios-sunny" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Change Theme'} align={'left'} clr={colors.OnBackground} />
							<View style={{ flexDirection: 'row', alignItems: 'center' }}>
								<View style={{ marginRight: 5 }}>
									<LableMedium text={'Light'} align={'left'} clr={colors.BackgroundColorShades.BC_500} />
								</View>
								<View style={{ marginRight: 5 }}>
									<LableMedium text={'Dark'} align={'left'} clr={colors.BackgroundColorShades.BC_500} />
								</View>
							</View>
						</View>
					</View>
					<View>
						<TouchableOpacity>
							<SelectDropdown
								data={['Light Mode', 'Dark Mode']}
								rowStyle={{ backgroundColor: colors.BackgroundColorShades.BC_100 }}
								rowTextStyle={{ fontSize: font.LableFont.Large.fontSize, color: colors.OnSurface }}
								dropdownStyle={{ backgroundColor: colors.BackgroundColorShades.BC_100, borderRadius: 8 }}
								defaultValueByIndex={0}
								buttonStyle={{ width: 120, backgroundColor: colors.Background }}
								buttonTextStyle={{ fontSize: font.LableFont.Large.fontSize, color: colors.OnBackground }}
								onSelect={(selectedItem, index) => {
									if (index == 0) {
										dispatch(saveAppThemeData_TO_LOCALDB('light'))
									}
									else if (index == 1) {
										dispatch(saveAppThemeData_TO_LOCALDB('dark'))
									}
									else (
										dispatch(saveAppThemeData_TO_LOCALDB('light'))
									)
								}}
								buttonTextAfterSelection={(selectedItem, index) => {
									// text represented after item is selected
									// if data array is an array of objects then return selectedItem.property to render after item is selected
									return selectedItem
								}}
								rowTextForSelection={(item, index) => {
									// text represented for each item in dropdown
									// if data array is an array of objects then return item.property to represent item in dropdown
									return item
								}}
							/>
						</TouchableOpacity>
					</View>
				</View>
				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<Entypo name="language" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Change Language'} align={'left'} clr={colors.OnBackground} />
							<View style={{ flexDirection: 'row', alignItems: 'center' }}>
								<View style={{ marginRight: 5 }}>
									<LableMedium text={'English'} align={'left'} clr={colors.BackgroundColorShades.BC_500} />
								</View>
								<View style={{ marginRight: 5 }}>
									<LableMedium text={'Hindi'} align={'left'} clr={colors.BackgroundColorShades.BC_500} />
								</View>
								<View style={{ marginRight: 5 }}>
									<LableMedium text={'Marwadi'} align={'left'} clr={colors.BackgroundColorShades.BC_500} />
								</View>
							</View>
						</View>
					</View>
					<View >
						<SelectDropdown
							data={['English', 'Hindi', 'Marwadi']}
							rowStyle={{ backgroundColor: colors.BackgroundColorShades.BC_100 }}
							rowTextStyle={{ fontSize: font.LableFont.Large.fontSize, color: colors.OnSurface }}
							dropdownStyle={{ backgroundColor: colors.BackgroundColorShades.BC_100, borderRadius: 8 }}
							defaultValue={languages[languageMode]}
							buttonStyle={{ width: 90, backgroundColor: colors.Background, }}
							buttonTextStyle={{ fontSize: font.LableFont.Large.fontSize, color: colors.OnBackground }}
							onSelect={(selectedItem, index) => {
								if (index == 0) {
									dispatch(saveAppLanguageData_TO_LOCALDB(language_Code.En))
									navigation.navigate('Home');
								}
								else if (index == 1) {
									dispatch(saveAppLanguageData_TO_LOCALDB(language_Code.Hi))
									navigation.navigate('Home')
								}
								else if (index == 2) {
									dispatch(saveAppLanguageData_TO_LOCALDB(language_Code.Mr))
									navigation.navigate('Home')
								}
								else (
									dispatch(saveAppLanguageData_TO_LOCALDB(language_Code.En))
								)
							}}
							buttonTextAfterSelection={(selectedItem, index) => {
								// text represented after item is selected
								// if data array is an array of objects then return selectedItem.property to render after item is selected
								return selectedItem
							}}
							rowTextForSelection={(item, index) => {
								// text represented for each item in dropdown
								// if data array is an array of objects then return item.property to represent item in dropdown
								return item
							}}
						/>
					</View>
				</View>
			</View>
			{/* Bottom Settings */}
			<View style={{ marginTop: 20, paddingTop: 20, borderTopColor: colors.BackgroundColorShades.BC_100, borderStyle: 'solid', borderTopWidth: 2 }}>

				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<Ionicons name="fast-food" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Food Preference'} align={'left'} clr={colors.OnBackground} />
							<View style={{ flexDirection: 'row', alignItems: 'center' }}>
								<View style={{ marginRight: 5 }}>
									<LableMedium text={foodpreference} align={'left'} clr={colors.BackgroundColorShades.BC_500} />
								</View>
							</View>
						</View>
					</View>
					<View >
						<SelectDropdown
							data={['Vegetarian', 'Non-Vegetarian']}
							rowStyle={{ backgroundColor: colors.BackgroundColorShades.BC_100 }}
							rowTextStyle={{ fontSize: font.LableFont.Large.fontSize, color: colors.OnSurface }}
							dropdownStyle={{ backgroundColor: colors.BackgroundColorShades.BC_100, borderRadius: 8 }}
							defaultValue={userData.food_preference}
							buttonStyle={{ width: 110, backgroundColor: colors.Background, }}
							buttonTextStyle={{ fontSize: font.LableFont.Large.fontSize, color: colors.Primary }}
							onSelect={(selectedItem, index) => {
								setfoodpreference(selectedItem)

								fetch("http://admin.rasodaa.com/api/customer/profile", {
									method: 'POST',
									headers: {
										'Content-Type': 'application/json',
										'Authorization': 'Bearer ' + accessToken,
									},
									body: JSON.stringify({
										name: userData.username,
										dob: userData.dob,
										gender: userData.gender,
										food_preference: selectedItem,
										spicy_level: userData.spicy_level,
										country: '',
										fast_days: userData.fast_days,
										state: "",
										city: "",
										locality: ""
									})

								}).then(response => response.json())
									.then(response => {
										if (response.status == 200) {
											navigation.navigate('Account');
										}
										else {
											alert(response.message)
										}
									})
									.catch(error => console.log('error', error));

							}}
							buttonTextAfterSelection={(selectedItem, index) => {
								// text represented after item is selected
								// if data array is an array of objects then return selectedItem.property to render after item is selected
								return selectedItem
							}}
							rowTextForSelection={(item, index) => {
								// text represented for each item in dropdown
								// if data array is an array of objects then return item.property to represent item in dropdown
								return item
							}}
						/>
					</View>
				</View>

				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 15, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<MaterialCommunityIcons name="car-brake-fluid-level" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Change Spicy Level'} align={'left'} clr={colors.OnBackground} />
							<View style={{ flexDirection: 'row', marginVertical: 5 }}>
								{SpicyLevel.map((item, index) => {
									return <Image key={index} source={{ uri: 'https://cdn-icons-png.flaticon.com/128/1598/1598080.png' }} style={{ width: 15, height: 15, marginHorizontal: 5 }} />
								})}

							</View>
						</View>
					</View>
					<View style={{ paddingRight: 18 }}>
						<TouchableOpacity onPress={() => {
							setOpenspicy(!openspicy)
							if (openspicy == true) {
								fetch("http://admin.rasodaa.com/api/customer/profile", {
									method: 'POST',
									headers: {
										'Content-Type': 'application/json',
										'Authorization': 'Bearer ' + accessToken,
									},
									body: JSON.stringify({
										name: userData.username,
										dob: userData.dob,
										gender: userData.gender,
										food_preference: userData.food_preference,
										spicy_level: spicy_level_value,
										country: '',
										fast_days: userData.fast_days,
										state: "",
										city: "",
										locality: ""
									})

								}).then(response => response.json())
									.then(response => {
										if (response.status == 200) {
											navigation.navigate('Account');
										}
										else {
											alert(response.message)
										}
									})
									.catch(error => console.log('error', error));
							}
						}}
						>
							{openspicy ? <LableLarge text={'Done'} clr={colors.Primary} align={'left'} /> : <LableLarge text={'Change'} clr={colors.Primary} align={'left'} />}
						</TouchableOpacity>
					</View>

				</View>
				{openspicy ? <View style={{ alignItems: 'center', marginVertical: 20 }}>
					<Slider
						style={{ width: Dimensions.get('window').width / 1.2, height: 40 }}
						minimumValue={1}
						maximumValue={5}
						minimumTrackTintColor={colors.OnBackground}
						maximumTrackTintColor={colors.BackgroundColorShades.BC_400}
						step={1}
						value={spicy_level_value}
						thumbImage={{ uri: 'https://cdn-icons-png.flaticon.com/128/1598/1598080.png' }}
						onSlidingComplete={(value) => {
							setSpicy_level_value(value)

						}}
					/>
				</View> : []}

				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 12, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<Fontisto name="date" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Change DOB'} align={'left'} clr={colors.OnBackground} />
							<LableLarge text={date.toDateString()} clr={colors.OnBackground} align={'left'} />
						</View>
					</View>
					<View style={{ paddingRight: 18 }}>
						<TouchableOpacity onPress={() => {
							setOpen(!open)
							if (open == true) {
								fetch("http://admin.rasodaa.com/api/customer/profile", {
									method: 'POST',
									headers: {
										'Content-Type': 'application/json',
										'Authorization': 'Bearer ' + accessToken,
									},
									body: JSON.stringify({
										name: userData.username,
										dob: date,
										gender: userData.gender,
										food_preference: userData.food_preference,
										spicy_level: spicy_level_value,
										country: '',
										fast_days: userData.fast_days,
										state: "",
										city: "",
										locality: ""
									})

								}).then(response => response.json())
									.then(response => {
										if (response.status == 200) {
											navigation.navigate('Account');
										}
										else {
											alert(response.message)
										}
									})
									.catch(error => console.log('error', error));
							}
						}}>
							{open ? <LableLarge text={'Done'} clr={colors.Primary} align={'left'} /> : <LableLarge text={'Change'} clr={colors.Primary} align={'left'} />}
						</TouchableOpacity>
					</View>
				</View>
				{open ? <View style={{ alignItems: 'center', marginVertical: 10 }}>
					<DatePicker mode='date' date={date} onDateChange={setDate} />
				</View> : []}

				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 15, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<MaterialCommunityIcons name="lock-reset" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Change Pin lock'} align={'left'} clr={colors.OnBackground} />
							<LableLarge text={'......'} clr={colors.OnBackground} align={'left'} />
						</View>
					</View>
					<View style={{ paddingRight: 18 }}>
						<TouchableOpacity onPress={() => { navigation.navigate('UtilityPinCreationScreen') }}>
							<LableLarge text={'Change'} clr={colors.Primary} align={'left'} />
						</TouchableOpacity>
					</View>
				</View>

				<View
					style={{ flexDirection: 'row', width: Dimensions.get('window').width, justifyContent: 'space-between', paddingVertical: 15, paddingHorizontal: 20, alignItems: 'center', marginTop: 8 }}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<MaterialCommunityIcons name="form-textbox-password" size={font.TitleFont.Large.fontSize} color={colors.Primary} style={{ marginRight: 28 }} />
						<View>
							<TitleMedium text={'Change Password'} align={'left'} clr={colors.OnBackground} />
							<LableLarge text={'......'} clr={colors.OnBackground} align={'left'} />
						</View>
					</View>
					<View style={{ paddingRight: 18 }}>
						<TouchableOpacity onPress={() => { navigation.navigate('UserChangePasswordScreen') }}>
							<LableLarge text={'Change'} clr={colors.Primary} align={'left'} />
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</View>
	)
}


const ModeWrapper = styled.View`
    border-width:1px;
	border-color:${props => props.bclr};
	border-style:solid;
    backgroundColor:${props => props.clr};
	marginTop:${props => props.mv[0]}px;
	marginBottom:${props => props.mv[1]}px;
    width:100%;
	elevation:5;
	paddingVertical:20px;
	border-radius:10px;
`
const ModeSwitchText = styled.Text`
	color:${props => props.clr};
	font-size:${props => props.fz}px;
	text-align:center;
	marginBottom:20px;
	font-weight:bold;
`
const ModeSwitchWrapper = styled.View`
	flex-direction:row;
	justify-content:space-around;
	width:100%;
	backgroundColor:${props => props.clr};
`

const ModeButton = styled.Pressable`
    flex-direction:row;
    justify-content:center;
    align-items:center;
    paddingHorizontal:10px;
    paddingVertical:10px;
    border-radius:10px;
    backgroundColor:${props => props.clr};
    
`
const ModeButtonText = styled.Text`
	color:${props => props.clr};
	font-size:${props => props.fz}px;
	text-align:center;
`