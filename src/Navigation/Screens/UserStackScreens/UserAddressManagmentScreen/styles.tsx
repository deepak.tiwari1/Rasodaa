import { StyleSheet } from 'react-native'
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createContainers = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.Background,
        paddingHorizontal: 20
    },
    add_addressContainer: {
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    view_addressContainer: {
        paddingVertical: 25,
        marginHorizontal: 5,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: (colors.modeCode == 'dark' ? 'rgba(243,243,243,.4)' : 'rgba(51,51,51,.4)')
    },
    single_addressContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    singleaddress_LeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '80%'
    },
    addressdetailsContainer: {
        marginLeft: 15,
    },
    addresssheet_Container: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    adresssheet_addressTypeContainer: {
        width: '100%',
        paddingVertical: 5,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    adresssheet_addressTypeButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: 10,
    },
    adresssheet_addressFormContainer: {
        width: '100%',
        paddingHorizontal: 10
    }
})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    headerText: {
        color: colors.OnBackground,
        fontSize: font.HeadlineFont.Small.fontSize,
        fontFamily: font.HeadlineFont.Small.fontFamily,
        alignSelf: 'center',
        marginVertical: 30
    },
    add_addressButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
        paddingVertical: 5,
        backgroundColor: colors.Primary,
        borderRadius: 10,
        height: 40,
        // width: 100
    },
    add_addressButtonText: {
        color: colors.OnPrimary,
        fontSize: font.BodyFont.Large.fontSize,
        fontFamily: font.BodyFont.Large.fontFamily,
    },
    address_heading: {
        fontSize: font.BodyFont.Large.fontSize,
        fontFamily: font.BodyFont.Large.fontFamily,
        color: colors.OnBackground
    },
    address_subheading: {
        fontSize: font.BodyFont.Small.fontSize,
        fontFamily: font.BodyFont.Small.fontFamily,
        color: colors.OnBackground
    },
    addresssheet_Header: {
        fontSize: font.TitleFont.Medium.fontSize,
        fontFamily: font.TitleFont.Medium.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center',
        marginVertical: 5,
        // borderBottomColor: colors.Outline,
        // borderStyle: "solid",
        // borderBottomWidth: 1

    },
    adresssheet_addressTypesubheader: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground,

    },
    adresssheet_addressTypeButton_active: {
        borderColor: colors.Secondry,
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 2,
        paddingHorizontal: 10,
        paddingVertical: 2,
        marginLeft: 8,
        elevation: 5,
        backgroundColor: colors.Surface

    },
    adresssheet_addressTypeButton_inactive: {
        borderColor: colors.SecondryColorShades.SC_500,
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 2,
        marginLeft: 8,
        elevation: 5,
        backgroundColor: colors.Surface

    },
    adresssheet_addressTypeButtonText: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground,
        alignSelf: 'center'
    },
    adresssheet_addressFormInput: {
        borderColor: colors.Outline,
        borderStyle: 'solid',
        borderWidth: 1,
        height: 50,
        width: '100%',
        paddingHorizontal: 20,
        borderRadius: 20,
        marginVertical: 10,

    },
    adresssheet_addressFormsubmitButton: {
        backgroundColor: colors.Primary,
        borderRadius: 20,
        marginVertical: 10,
        paddingVertical: 13,
    },
    adresssheet_addressFormsubmitButtonText: {
        alignSelf: 'center',
        fontFamily: font.BodyFont.Large.fontFamily,
        fontSize: font.BodyFont.Large.fontSize,
        color: colors.OnPrimary
    }

})
