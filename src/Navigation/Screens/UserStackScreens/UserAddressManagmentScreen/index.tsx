import React, { useEffect, useState, useMemo, useRef } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Pressable, Image, ScrollView, Dimensions, TouchableOpacity, TextInput, Button } from 'react-native';
import { RootState } from '../../../../Management';
import { useSelector, useDispatch } from 'react-redux';
import { setDefaultAddress, setEditAddress, addAddress, removeAddress, updateAddress } from '../../../../Management/User/userAddressManager';
import { BASE_URL } from '../../../../Constant';
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import { createStyles, createContainers } from './styles'
import RBSheet, { RBSheetProps } from "react-native-raw-bottom-sheet";
import { differenceInMinutesWithOptions } from 'date-fns/fp';

type address = {
	"id": string,
	"customer_id": string,
	"address_title": string,
	"address": string,
	"country": string,
	"state": string,
	"city": string,
	"locality": string,
	"pincode": string,
	"latitude": string,
	"longitude": string,
	"is_default": string,
	"deleted_at": string,
	"created_at": string,
	"updated_at": string,
	"customer_name": string,
	"country_name": string,
	"state_name": string,
	"city_name": string,
	"locality_name": string
}


export default function UserAddressManagement(props) {
	let addresslist = useSelector((state: RootState) => state.useraddressprovider);
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
	const styles = useMemo(() => createStyles(colors, font), [colors, font])
	const containers = useMemo(() => createContainers(colors), [colors])
	let dispatch = useDispatch();
	let [address, setAddress] = useState<address[]>([]);

	const BSheet = useRef()

	const fetchAddress = async (accessToken: string) => {
		// console.log(accessToken)
		await fetch(BASE_URL + 'customer/addresses', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + accessToken
			}
		}).then(response => response.json()).then(res => {
			if (res.status == 200) {
				// console.log(res.addresses);
				setAddress(res.addresses);
			}
			else {
				alert(res.message);
			}
		})
	}

	useEffect(() => {
		getRasodaaAccessToken(fetchAddress);
	}, [])
	let [addressType, setaddressType] = useState('Home');
	let [firstLine, setfirstLine] = useState("");
	let [secondLine, setsecondLine] = useState("");
	let [city, setcity] = useState("");
	let [state, setstate] = useState("");
	let [zip, setzip] = useState("");

	return (
		<ScrollView style={containers.main}>
			<Text style={styles.headerText}>My Addresses</Text>
			<View style={containers.add_addressContainer}>
				<TouchableOpacity style={styles.add_addressButton} onPress={() => BSheet.current?.open()}>
					<Ionicons name="add" color={colors.OnPrimary} size={24} />
					<Text style={styles.add_addressButtonText}>Add new</Text>
				</TouchableOpacity>
			</View>
			<View style={containers.view_addressContainer}>
				{address.map((item, index) => {
					return (
						<View key={index} style={containers.single_addressContainer}>
							<View style={containers.singleaddress_LeftContainer}>
								<Feather name="home" size={20} color={colors.OnBackground} />
								<View style={containers.addressdetailsContainer}>
									<Text style={styles.address_heading}>Home</Text>
									<Text style={styles.address_subheading}>{item.address} {item.address_title}</Text>
									<Text style={styles.address_subheading}>{item.city_name} {item.country_name}, {item.pincode}</Text>
								</View>
							</View>
							<TouchableOpacity>
								<SimpleLineIcons name="options" color={colors.Primary} size={16} />
							</TouchableOpacity>
						</View>
					)
				})}
			</View>
			<RBSheet
				ref={BSheet}
				height={Dimensions.get('window').height / 1.4}
				animationType={'slide'}
				closeOnDragDown={true}
				closeOnPressMask={false}
				customStyles={{ container: { backgroundColor: colors.Background } }}
			>
				<View style={containers.addresssheet_Container}>
					{/* <Text style={styles.addresssheet_Header}>Enter complete address</Text> */}
					<View style={containers.adresssheet_addressTypeContainer}>
						<Text style={styles.adresssheet_addressTypesubheader}>Save address as</Text>
						<View style={containers.adresssheet_addressTypeButtonsContainer}>
							<Pressable
								style={(addressType == 'Home' ? styles.adresssheet_addressTypeButton_active : styles.adresssheet_addressTypeButton_inactive)}
								onPress={() => setaddressType('Home')}
							>
								<Text style={styles.adresssheet_addressTypeButtonText}>Home</Text>
							</Pressable>
							<Pressable
								onPress={() => setaddressType('Work')}
								style={(addressType == 'Work' ? styles.adresssheet_addressTypeButton_active : styles.adresssheet_addressTypeButton_inactive)}>
								<Text style={styles.adresssheet_addressTypeButtonText}>Work</Text>
							</Pressable>
						</View>
					</View>
					<View style={containers.adresssheet_addressFormContainer}>
						<TextInput
							placeholder='Enter House no, floor no'
							placeholderTextColor={colors.BackgroundColorShades.BC_400}
							style={styles.adresssheet_addressFormInput} />
						<TextInput
							placeholder='Enter locality'
							placeholderTextColor={colors.BackgroundColorShades.BC_400}
							style={styles.adresssheet_addressFormInput} />
						<TextInput
							placeholder='Enter city'
							placeholderTextColor={colors.BackgroundColorShades.BC_400}
							style={styles.adresssheet_addressFormInput} />
						<TextInput
							placeholder='Enter state'
							placeholderTextColor={colors.BackgroundColorShades.BC_400}
							style={styles.adresssheet_addressFormInput} />
						<TextInput
							placeholder='Enter zip code'
							placeholderTextColor={colors.BackgroundColorShades.BC_400}
							style={styles.adresssheet_addressFormInput} />
						<TouchableOpacity style={styles.adresssheet_addressFormsubmitButton}>
							<Text style={styles.adresssheet_addressFormsubmitButtonText}>Save address</Text>
						</TouchableOpacity>
					</View>
				</View>
			</RBSheet>
		</ScrollView >
	)
}
