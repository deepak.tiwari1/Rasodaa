import { View, Text } from 'react-native';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import type { RootState } from '../../../../Management'
import { createStyles, createContainers } from './styles'

export default function UserSwapHistoryScreen() {
    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
    const styles = useMemo(() => createStyles(colors, font), [colors, font]);
    const containers = useMemo(() => createContainers(colors), [colors]);
    return (
        <View>
            <Text style={styles.headlingText}>All swap tiffins</Text>
        </View>
    );
}