import React, { useState, useEffect, useMemo } from 'react'
import { View, Text, StatusBar, TouchableOpacity } from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { BASE_URL } from '../../../../Constant'
import { useSelector, useDispatch } from 'react-redux'
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager'
import { createHeaderStyles, createContainers } from './styles'
import { RootState } from '../../../../Management'

export default function ChefProfileHeader({ navigation, route }) {
    const [favorite_status, setfavorite_status] = useState(false);
    const { colors, font, fontSize } = useSelector((state: RootState) => state.appapperiencemanager);

    const styles = useMemo(() => createHeaderStyles(colors, font), [colors, font])
    const containers = useMemo(() => createContainers(colors), [colors])

    let dispatch = useDispatch();
    // const fetchdata = (accessToken) => {
    //     fetch(BASE_URL + 'customer/favourites', {
    //         method: 'GET',
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': 'Bearer ' + accessToken
    //         }
    //     }).then(res => res.json()).then(res => {
    //         if (res.status == 200) {
    //             // console.log(res.favourites.favourites)
    //             for (let i = 0; i < res.favourites.favourites.length; i++) {
    //                 if (res.favourites.favourites[i].chef_id == navigation.route.params?.cheffId) {
    //                     setfavorite_status(true)
    //                 }
    //                 else {
    //                     setfavorite_status(false)
    //                 }
    //             }
    //         }
    //         else {
    //             alert(res.status)
    //         }
    //     })
    // }
    // useEffect(() => {
    //     getRasodaaAccessToken(fetchdata)
    // }, [])

    return (
        <View>

        </View>
        // <View style={containers.headermain}>
        //     <StatusBar backgroundColor={colors.Background} barStyle={colors.modeCode == 'dark' ? 'dark-content' : 'light-content'} />
        //     <View style={containers.headerContainer}>
        //         <TouchableOpacity onPress={() => { navigation.goBack() }}>
        //             <Ionicons name="arrow-back" size={24} color={colors.OnBackground} />
        //         </TouchableOpacity>
        //         <TouchableOpacity onPress={() => { setfavorite_status(!favorite_status) }} >
        //             {(favorite_status) ? <MaterialIcons name="favorite" size={24} color={colors.Primary} /> : <MaterialIcons name="favorite-outline" size={24} color={colors.Primary} />}
        //         </TouchableOpacity>
        //     </View>
        // </View>
    )
}