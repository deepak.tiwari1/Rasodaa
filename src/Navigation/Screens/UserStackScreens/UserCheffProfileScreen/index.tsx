import React, { useEffect, useState, useMemo } from 'react'
import { Text, View, ScrollView, Dimensions, TouchableOpacity, Image, Animated, StyleSheet, StatusBar, Pressable } from 'react-native'
import { P600 } from '../../../../Management/Utility/GlobalStyles';
import TiffineSheduled from '../../../../Component/section/CheffTiffineShedule';
import TiffinSubsPlans from '../../../../Component/section/CheffPackages';
import ReviewsTab from '../../../../Component/section/CheffProfileReviews';
import CheffDetails from '../../../../Component/section/CheffProfileDetails';
import OnDemandtiffins from '../../../../Component/section/CheffSpecialTiffins';
import { useSelector, useDispatch } from 'react-redux';
import { UserCheffProfileScreenProps } from '../../UserScreensPropTypes';
import { RootState } from '../../../../Management';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { BASE_URL, SLIDER_CHEF } from '../../../../Constant';
import { getRasodaaAccessToken } from '../../../../Management/services/AccessTokenManager';
import { createStyles, createContainers } from './styles'
import HorizontalCheffSection from '../../../../Component/section/HorizontalCheffSection'
import { HeadlineLarge, LableMedium } from '../../../../Component/Utility/TextComponent';

export default function UserCheffProfileScreen(props: UserCheffProfileScreenProps) {
  const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager);
  const styles = useMemo(() => createStyles(colors, font), [colors, font])
  const containers = useMemo(() => createContainers(colors), [colors])
  const [favorite_status, setfavorite_status] = useState(false);
  return (
    <ScrollView style={containers.main}>
      <View style={containers.headermain}>
        {/* <StatusBar backgroundColor={colors.Background} barStyle={colors.modeCode == 'dark' ? 'dark-content' : 'light-content'} /> */}
        <View style={containers.headerContainer}>
          <TouchableOpacity onPress={() => { props.navigation.goBack() }}>
            <Ionicons name="arrow-back" size={24} color={colors.OnBackground} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { setfavorite_status(!favorite_status) }} >
            {(favorite_status) ? <MaterialIcons name="favorite" size={24} color={colors.Primary} /> : <MaterialIcons name="favorite-outline" size={24} color={colors.Primary} />}
          </TouchableOpacity>
        </View>
      </View>
      <View style={containers.cheffProfile_headerContainer}>
        <View style={containers.cheffProfile_headerleftSection}>
          <Text style={styles.headingText}>{'Sanjiv Kumar'}</Text>
          <Text style={styles.subheadingText}>{'South Indian, North Indian'}</Text>
          <Text style={styles.locationText}>{'Suraj poll, Udaipur 323001'}</Text>
          <View style={containers.cheffProfile_headerRatingContainer}>
            <AntDesign name="star" size={16} color={'white'} />
            <Text style={styles.rating_text}>4.2</Text>
          </View>
        </View>
        <View style={containers.cheffProfile_headerrightSection}>
          <Image
            source={{ uri: 'https://images.squarespace-cdn.com/content/v1/55493f50e4b082cc498fdb53/1563982759292-IJ84W3GAWOS2ZY1FFU24/Chef+Sanjeev+Kapoor+%284+of+7%29.jpg?format=1000w' }}
            style={{ height: 100, width: 100, borderRadius: 50 }}
          />
        </View>
      </View>
      <CheffDetails {...props} />
      <TiffineSheduled {...props} />
      <OnDemandtiffins {...props} />
      <TiffinSubsPlans {...props} />

      <ReviewsTab {...props} />
      <HorizontalCheffSection props={props} title={'Similar Chefs'} />
      <HorizontalCheffSection props={props} title={'Recommendation'} />
      <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
        <HeadlineLarge text={'Rasodaa'} clr={colors.BackgroundColorShades.BC_300} align={'left'} />
        <LableMedium text={'The Cheff Shown Above include personal, local and sponsored recommendations.'} clr={colors.BackgroundColorShades.BC_300} align={'left'} />
      </View>
    </ScrollView>
  )
}


