import { StyleSheet } from 'react-native'
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createContainers = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.Background,
        // paddingHorizontal: 20
    },
    headermain: {
        width: '100%',
        paddingHorizontal: 25,
        paddingVertical: 15,
        // height: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: colors.Background
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    cheffProfile_headerContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
    },
    cheffProfile_headerleftSection: {
        paddingHorizontal: 10
    },
    cheffProfile_headerrightSection: {
        padding: 3,
        backgroundColor: colors.Background,
        borderRadius: 50,
        elevation: 5
    },
    cheffProfile_headerRatingContainer: {
        backgroundColor: 'green',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: 4,
        borderRadius: 10,
        flexDirection: 'row',
        alignSelf: 'flex-start'
    }
})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    headingText: {
        fontSize: 24,
        fontWeight: '800',
        color: colors.OnBackground
    },
    subheadingText: {
        fontSize: font.BodyFont.Medium.fontSize,
        fontFamily: font.BodyFont.Medium.fontFamily,
        color: colors.OnBackground
    },
    locationText: {
        fontSize: 12,
        color: colors.OnBackground
    },
    rating_text: {
        fontSize: 16,
        color: 'white',
        marginLeft: 4
    }
})

export const createHeaderStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({

})