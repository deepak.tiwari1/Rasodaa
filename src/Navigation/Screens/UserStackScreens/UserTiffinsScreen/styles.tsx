import { Dimensions, StyleSheet } from "react-native";
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createHeaderStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    search: {

        width: Dimensions.get('window').width,
        height: 60,
        paddingHorizontal: 15,
        // marginBottom: 10,
        paddingTop: 10,
        backgroundColor: colors.Background
    },
    searchbar: {
        height: '100%',
        width: '100%',
        borderRadius: 20,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: colors.Outline,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 15,
        backgroundColor: 'rgba(243,243,243,1)',
        elevation: 5
    },
    searchbar_input: {
        marginHorizontal: 12,
        width: '80%',
        height: '100%',
        fontSize: 18,
        color: 'black'

    }
})