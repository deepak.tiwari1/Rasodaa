import { View, Text, ScrollView, Pressable, ImageBackground, StyleSheet, Dimensions, Image, StatusBar } from 'react-native';
import React, { useEffect, useState } from 'react';
import * as GS from '../../../../Management/Utility/GlobalStyles';
import * as Server from '../../../../Management/Utility/GlobalURLs';
import CheffTiffinsCard from '../../../../Component/cards/FilterTiffinsCard';
import type { UserTiffinsScreenProps } from '../../UserScreensPropTypes';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../Management';
import VerticalCheffSection from '../../../../Component/section/VerticalCheffSection'
import { HeadlineLarge, LableMedium } from '../../../../Component/Utility/TextComponent';

export default function UserTiffinsScreen(props: UserTiffinsScreenProps) {
    let cheffData = [
        {
            cheffName: 'Sanjiv Kumar',
            cheffImage: 'https://uniquetimes.org/wp-content/uploads/2020/05/img_106501_chefsanjeevkapoor3_s.jpg'
        },
        {
            cheffName: 'Chandu Mehta',
            cheffImage: 'https://cdn.lifestyleasia.com/wp-content/uploads/sites/3/2021/06/01125429/129305145_378999043204329_6799470558229249305_n.jpg'
        },
        {
            cheffName: 'Goverdhan Purohit',
            cheffImage: 'https://taste.co.za/wp-content/uploads/2017/09/MPW.jpg'
        },
        {
            cheffName: 'Anupama Singh',
            cheffImage: 'https://media.architecturaldigest.com/photos/5e84dd4a82159b000817253e/4:3/w_3448,h_2586,c_limit/GettyImages-1211668452.jpg'
        },
        {
            cheffName: 'Rohit Singh',
            cheffImage: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYGZVZlgTOb0IKRcXLS_6uRuFNCDSccREZvPKyf2WisHb4vIXPc5xKDoDTlHYOvs-iC5c&usqp=CAU'
        }
    ]
    const [tiffins, SetTiffins] = useState(cheffData);
    useEffect(() => {
        // fetch(Server.API+'tiffins?type='+props.route.).then(res=>res.json()).then(data=>{SetTiffins(data.tiffins.tiffins)}).catch(err=>console.log(err))
    }, [])
    const { colors } = useSelector((state: RootState) => state.appapperiencemanager)
    const [sort, setsort] = useState(false);

    return (
        <ScrollView
            style={{ backgroundColor: colors.Background, flex: 1, paddingHorizontal: 10 }}
            showsVerticalScrollIndicator={false}
        >
            <VerticalCheffSection props={props} title={'Search results'} />
            <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
                <HeadlineLarge text={'Rasodaa'} clr={colors.BackgroundColorShades.BC_300} align={'left'} />
                <LableMedium text={'The Cheff Shown Above include personal, local and sponsored recommendations.'} clr={colors.BackgroundColorShades.BC_300} align={'left'} />
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1

    },
    card_Container: {
        paddingHorizontal: 5,
        marginVertical: 5,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',

    }

})


