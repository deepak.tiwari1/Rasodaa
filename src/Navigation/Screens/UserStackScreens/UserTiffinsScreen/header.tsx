import { View, Text, TextInput, Pressable } from 'react-native'
import React, { useMemo, useState } from 'react'
import { useSelector } from 'react-redux'
import { createHeaderStyles } from './styles'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import { RootState } from '../../../../Management'

export default function UserTiffinsScreenHeader({ navigation, route }) {

    const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
    const styles = useMemo(() => createHeaderStyles(colors, font), [colors])

    const [searchText, setSearchText] = useState('')

    return (
        <View style={styles.search}>
            <View style={styles.searchbar}>
                <Pressable onPress={() => { navigation.goBack() }}>
                    <SimpleLineIcons name="arrow-left" size={17} color={'black'} />
                </Pressable>
                <TextInput
                    style={styles.searchbar_input}
                    defaultValue={route.params.tiffinName}
                    maxLength={30}
                    autoCorrect={true}
                    onFocus={() => navigation.navigate('Search')}
                    onChange={
                        (newtext) => {
                            setSearchText(newtext.nativeEvent.text);

                        }} />
                <Pressable onPress={() => { navigation.navigate('Search') }}>
                    <Entypo name="cross" size={24} color={'black'} />
                </Pressable>

            </View>
        </View>
    )
}

