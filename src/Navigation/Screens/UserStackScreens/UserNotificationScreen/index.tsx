import React, { useMemo } from 'react';
import { View, Text } from 'react-native';
import type { UserNotificationScreenProps } from '../../UserScreensPropTypes';
import { createStyles, createContainers } from './styles'
import { useSelector } from 'react-redux'
import type { RootState } from '../../../../Management'

export default function UserNotificationScreen({ navigation, route }: UserNotificationScreenProps) {
	const { colors, font } = useSelector((state: RootState) => state.appapperiencemanager)
	const styles = useMemo(() => createStyles(colors, font), [colors, font])
	const containers = useMemo(() => createContainers(colors), [colors])
	return (
		<View style={containers.main}>
			<Text style={styles.headerText}>App Notification</Text>
		</View>
	)
}
