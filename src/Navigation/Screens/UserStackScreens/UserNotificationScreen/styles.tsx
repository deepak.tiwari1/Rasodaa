import { StyleSheet } from 'react-native'
import type { Colors, initialFontdata } from '../../../../Management/App/appApperienceManager'

export const createContainers = (colors: Colors) => StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.Background,
        paddingVertical: 20
    }
})

export const createStyles = (colors: Colors, font: initialFontdata) => StyleSheet.create({
    headerText: {
        color: colors.OnBackground,
        fontSize: font.HeadlineFont.Medium.fontSize,
        fontFamily: font.HeadlineFont.Medium.fontFamily,
        alignSelf: 'center'
    }
})

