import React from 'react';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { StackScreenList } from '..';

export type UserHomeScreenProps = NativeStackScreenProps<StackScreenList, 'UserHomeScreen'>;
export type UserHomeScreenRoute = UserHomeScreenProps['route'];
export type UserHomeScreenNavigation = UserHomeScreenProps['navigation'];

export type UserTiffinsScreenProps = NativeStackScreenProps<StackScreenList, 'UserTiffinsScreen'>;
export type UserTiffinsScreenRoute = UserTiffinsScreenProps['route'];
export type UserTiffinsScreenNavigation = UserTiffinsScreenProps['navigation'];

export type UserCheffProfileScreenProps = NativeStackScreenProps<StackScreenList, 'UserCheffProfileScreen'>;
export type UserCheffProfileScreenRoute = UserCheffProfileScreenProps['route'];
export type UserCheffProfileScreenNavigation = UserCheffProfileScreenProps['navigation'];

export type UserCheffPackageScreenProps = NativeStackScreenProps<StackScreenList, 'UserCheffPackageScreen'>;
export type UserCheffPackageScreenRoute = UserCheffPackageScreenProps['route'];
export type UserCheffPackageScreenNavigation = UserCheffPackageScreenProps['navigation'];

export type UserCheckoutScreenProps = NativeStackScreenProps<StackScreenList, 'UserCheckoutScreen'>;
export type UserCheckoutScreenRoute = UserCheckoutScreenProps['route'];
export type UserCheckoutScreenNavigation = UserCheckoutScreenProps['navigation'];

export type UserFavouriteCheffScreenProps = NativeStackScreenProps<StackScreenList, 'UserFavouriteCheffScreen'>;
export type UserFavouriteCheffScreenRoute = UserFavouriteCheffScreenProps['route'];
export type UserFavouriteCheffScreenNavigation = UserFavouriteCheffScreenProps['navigation'];

export type UserNotificationScreenProps = NativeStackScreenProps<StackScreenList, 'UserNotificationScreen'>;
export type UserNotificationScreenRoute = UserNotificationScreenProps['route'];
export type UserNotificationScreenNavigation = UserNotificationScreenProps['navigation'];

export type UserSettingsScreenProps = NativeStackScreenProps<StackScreenList, 'UserSettingsScreen'>;
export type UserSettingsScreenRoute = UserSettingsScreenProps['route'];
export type UserSettingsScreenNavigation = UserSettingsScreenProps['navigation'];

export type UserPaymentDetailsScreenProps = NativeStackScreenProps<StackScreenList, 'UserPaymentDetailsScreen'>;
export type UserPaymentDetailsScreenRoute = UserPaymentDetailsScreenProps['route'];
export type UserPaymentDetailsScreenNavigation = UserPaymentDetailsScreenProps['navigation'];

export type UserHelpAndSupportScreenProps = NativeStackScreenProps<StackScreenList, 'UserHelpAndSupportScreen'>;
export type UserHelpAndSupportScreenRoute = UserHelpAndSupportScreenProps['route'];
export type UserHelpAndSupportScreenNavigation = UserHelpAndSupportScreenProps['navigation'];

export type UserAddressManagementScreenProps = NativeStackScreenProps<StackScreenList, 'UserAddressManagementScreen'>;
export type UserAddressManagementScreenRoute = UserAddressManagementScreenProps['route'];
export type UserAddressManagementScreenNavigation = UserAddressManagementScreenProps['navigation'];

export type UserCartScreenProps = NativeStackScreenProps<StackScreenList, 'UserCartScreen'>;
export type UserCartScreenRoute = UserCartScreenProps['route'];
export type UserCartScreenNavigation = UserCartScreenProps['navigation'];

