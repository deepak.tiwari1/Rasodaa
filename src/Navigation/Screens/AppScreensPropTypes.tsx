import React from 'react';
import { StackScreenList } from '..';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

export type OnBordingScreenProps = NativeStackScreenProps<StackScreenList, 'AppOnBordingScreen'>;
export type OnBordingScreenRoute = OnBordingScreenProps['route'];
export type OnBordingScreenNavigation = OnBordingScreenProps['navigation'];

export type AppSignupScreenProps = NativeStackScreenProps<StackScreenList, 'AppSignupScreen'>;
export type AppSignupScreenRoute = AppSignupScreenProps['route'];
export type AppSignupScreenNavigation = AppSignupScreenProps['navigation'];

export type AppLoginScreenProps = NativeStackScreenProps<StackScreenList, 'AppLoginScreen'>;
export type AppLoginScreenRoute = AppLoginScreenProps['route'];
export type AppLoginScreenNavigation = AppLoginScreenProps['navigation'];

export type AppForgotPasswordScreenProps = NativeStackScreenProps<StackScreenList, 'AppForgotPasswordScreen'>;
export type AppForgotPasswordScreenRoute = AppForgotPasswordScreenProps['route'];
export type AppForgotPasswordScreenNavigation = AppForgotPasswordScreenProps['navigation'];

export type AppOtpScreenProps = NativeStackScreenProps<StackScreenList, 'AppOtpScreen'>;
export type AppOtpScreenRoute = AppOtpScreenProps['route'];
export type AppOtpScreenNavigation = AppOtpScreenProps['navigation'];

export type MobileNumberEntryScreenProps = NativeStackScreenProps<StackScreenList, 'MobileNumberEntryScreen'>;
export type MobileNumberEntryScreenRoute = MobileNumberEntryScreenProps['route'];
export type MobileNumberEntryScreenNavigation = MobileNumberEntryScreenProps['navigation'];

