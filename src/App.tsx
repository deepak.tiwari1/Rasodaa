/* ------------------------ importing Global Modules ------------------------ */
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import i18n from 'i18n-js';
import * as languages from './assets/JSONFiles/language.json';
import Navigation from "./Navigation";
import { UnEncryptedStoreKeys } from './Management/LocalDBStoreKeys';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import * as SplashScreen from 'expo-splash-screen';
import { AddFont } from './Management/Utility/GlobalStyles';
import { RootState } from './Management';

import { useSelector, useDispatch } from 'react-redux';
import { setolduserdata, setappLogindata, saveGuestMode_TO_LOCALDB } from './Management/App/appNavigationManager';
import { saveAppThemeData_TO_LOCALDB, saveAppLanguageData_TO_LOCALDB, saveFontSizeData_TO_LOCALDB } from './Management/App/appApperienceManager';
import { setappPindata, setappfingerprintdata } from './Management/App/appPinandFingerprintManager';
import { saveActivePackageData_TO_LOCALDB } from './Management/User/userActivePackageManager';

export default function App() {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  AddFont(setIsLoading);
  i18n.translations = languages;

  const dispatch = useDispatch();

  const getsavedPin = async () => {
    let savedPin = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppPin);
    if (savedPin) {
      dispatch(setappPindata(savedPin));
    }
  }
  const getsavedfingerprint = async () => {
    let savedfingerprint = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppFingerprint);
    if (savedfingerprint) {
      if (savedfingerprint == 'true') {
        dispatch(setappfingerprintdata(true));
      }
      else if (savedfingerprint == 'false') {
        dispatch(setappfingerprintdata(false));
      }
    }
  }
  const getsavedolduser = async () => {
    let savedolduser = await AsyncStorage.getItem(UnEncryptedStoreKeys.isOldUser);
    if (savedolduser) {
      if (savedolduser == 'true') {
        dispatch(setolduserdata(true));
      }
      else if (savedolduser == 'false') {
        dispatch(setolduserdata(false));
      }
    }
  }

  const getloginedstatus = async () => {
    let loginedstatus = await AsyncStorage.getItem(UnEncryptedStoreKeys.UserLoginedStatus);
    if (loginedstatus) {
      if (loginedstatus == 'true') {
        dispatch(setappLogindata(true));
      }
      else if (loginedstatus == 'false') {
        dispatch(setappLogindata(false));
      }
    }
  }

  const getappTheme = async () => {
    let savedappTheme = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppTheme);
    if (savedappTheme) {
      //console.log('savedappTheme' + savedappTheme)
      dispatch(saveAppThemeData_TO_LOCALDB(savedappTheme));
    }
  }

  const getappLanguage = async () => {
    let savedappLanguage = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppLanguage);
    if (savedappLanguage) {
      //console.log('savedappLanguage' + savedappLanguage)
      dispatch(saveAppLanguageData_TO_LOCALDB(savedappLanguage));
    }
    else {
      i18n.locale = 'en-IN';
    }
  }

  const getFontSize = async () => {
    let savedFontSize = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppFontSize);
    if (savedFontSize) {
      //console.log('savedFontSize' + savedFontSize)
      dispatch(saveFontSizeData_TO_LOCALDB(savedFontSize));
    }
  }

  const getGuestMode = async () => {
    let savedGuestMode = await AsyncStorage.getItem(UnEncryptedStoreKeys.AppGuestMode);
    if (savedGuestMode) {
      dispatch(saveGuestMode_TO_LOCALDB(savedGuestMode));
    }
  }

  const getActivePackage = async () => {
    let savedActivePackage = await AsyncStorage.getItem(UnEncryptedStoreKeys.ActivePackageData);
    if (savedActivePackage) {
      dispatch(saveActivePackageData_TO_LOCALDB(savedActivePackage));
    }
  }

  useEffect(() => {

    getloginedstatus();
    getsavedPin();
    getsavedfingerprint();
    getsavedolduser();
    getappTheme();
    getappLanguage();
    getFontSize();
    getGuestMode();
    getActivePackage();


  }, [])

  return (isLoading ? <Navigation /> : [])

}


