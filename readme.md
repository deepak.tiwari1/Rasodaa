
# Rasodaa

An Mobile application for all Tiffin and Home Food Lovers.


## Current Features

- Light/dark mode Management
- Multi language Management
- Geolocation Management
- Network Management
- Guest and User Management
- Pinlock and Fingerprint Management
- local storage Management
- Google and Firebase support enable
- Razerpay support enable

## Roadmap

- Chatbot integration for Help and Support.

- Remaining Backend API integration for User.

- Google accessToken Verification Api integration in Backend.

- Update in Current Supported Languages vocabulary File. 

- Add More Languages support.

- Updated Location File for Indian States.


## Run Locally
Pre-requirement

Must Have `git cli` and `npm cli` install

Clone the project

```bash
  git clone https://gitlab.com/deepak.tiwari1/Rasodaa.git
```

Go to the project directory

```bash
  cd Rasodaa
```

Install dependencies

```bash
  npm install
```

build Android or IOS

```bash
  npx react-native run-android
```
```bash
  npx react-native run-ios
```
It starts the developement server and install rasodaa app in your connected adb enabled physical device.

If developement server not started after app installation. 
Run This command to start developement server again.
```bash
  npx react-native start
```

Note:- Your Laptop and Mobile must connect to same WIFI to access developement server by mobile device.
when you change in code file and save the code developement server perform a Hot reload and the latest code changes reflate in your app.
if you press "r" in your developement server console, developement server perform a rebuild process and launch the app again.

## Environment Variables

Google cloud and firebase support

`LoginScreen/index.tsx`
```typescript
GoogleSignin.configure({});
```
Razerpay support

`UserCheckoutScreen.tsx (line 16:35)`

```typescript
options = {}

RazorpayCheckout.open(options);

```
## Libraries to Know

`Font:- "@expo-google-fonts/poppins"`

`Local-storage:-  "@react-native-async-storage/async-storage"`

`Network-detect:- "@react-native-community/netinfo"`

`OTP Verification:- "@react-native-firebase/app" and "@react-native-firebase/auth"`

`Google Auth:- 
    "@react-native-google-signin/google-signin"`

`Razerpay:- 
    "react-native-razorpay"
`

`App data Management:-
    "@reduxjs/toolkit" and "react-redux"
`

`fingerprint:- 
    "expo-local-authentication"
`

`geolocation:-
    "react-native-geolocation-service"
`

## FAQ

### How to Change colors in Dark and Light Mode.

In Order to change colors in color Modes. You need to change the default colors in Color Mode.

First Open `src/Management/App/appApperienceManager.tsx` File.

here you can find DarkMode and LightMode Variables. You can Change the Values in the Object and It Reflect in the App.

if you confuse which Variables is changing which part of the UI then checkout [Google Matarial Design](https://material.io/design/material-theming/implementing-your-theme.html#color)

Rasodaa Matarial Design [File](https://www.figma.com/file/MdyICLxAJaJE5gGoHgBNDR/Rasodaa?node-id=0%3A1)

### Define the Rasodaa App Life-cycle.

User Registration :- `Login screen` -> `Registration screen` -> `OTP Verification screen` -> `Registeraton Success Screen` -> `Login Screen`

User Forgot Password :- `Login Screen` -> `Forgot Password screen` -> `OTP Verification screen` -> `Login Screen`

Guest Mode :- `Login Screen` -> `City Selection screen` -> `Guest-Mode Hometab` -> `Cheff Profile` -> `Login Screen`

User Mode :- `Login Screen` -> `Pinlock Creation screen` -> `Hometab` -> `Settingtab` -> `Logout` -> `Login screen`




## Connect to previous developers

- [@DeepakTiwari](https://github.com/deepaktiwari09)

